import { DELETE_CUSTOMERS_ERROR, DELETE_CUSTOMERS_PENDING, 
  DELETE_CUSTOMERS_SUCCESS, FETCH_CUSTOMERS_ERROR, FETCH_CUSTOMERS_PENDING, 
  FETCH_CUSTOMERS_SUCCESS, FETCH_CUSTOMER_DETAIL_ERROR, 
  FETCH_CUSTOMER_DETAIL_PENDING, FETCH_CUSTOMER_DETAIL_SUCCESS, 
  FETCH_EXPORT_CUSTOMER_ERROR, 
  FETCH_EXPORT_CUSTOMER_PENDING, 
  FETCH_EXPORT_CUSTOMER_SUCCESS, 
  ON_CHANGE_CUSTOMER_INFO, 
  ON_CHANGE_PAGINATION_CUSTOMER, 
  RESET_CUSTOMER,
  RESET_TO_FIRST_PAGE
} from "../constants/customer.constant";

const adminLoginInfo = JSON.parse(localStorage.getItem('adminLoginInfo'));
var accessToken = "";
if(adminLoginInfo){
  accessToken = adminLoginInfo[0].accessToken;
}
//lấy toàn bộ khách hàng trong db
export const fetchCustomersAction = (limit, currentPage, searchContent) =>{
  return async (dispatch) => {
    try {
        var requestOptions = {
            method: 'GET',
            redirect: 'follow',
            headers:{
              "x-access-token": accessToken
            }
        };

        await dispatch({
            type: FETCH_CUSTOMERS_PENDING
        })
        let  url = "http://localhost:8080/customers";
        //lay tong toan bo san pham ko tinh phan trang
        const responseCustomers = await fetch(url+"?searchContent="+searchContent, requestOptions);
        const data = await responseCustomers.json();
        //lay cac san pham theo phan trang
        const urlCustomersWithSearch = url + "?limit="+limit
        +"&page="+currentPage+"&searchContent="+searchContent;
        const responseCustomerWithSearch = await fetch(urlCustomersWithSearch, requestOptions);
        const dataCustomerWithSearch = await responseCustomerWithSearch.json();
        return dispatch({
            type: FETCH_CUSTOMERS_SUCCESS,
            customers: dataCustomerWithSearch.data,
            totalCustomers: data.data.length
        })
    } catch (error) {
        return dispatch({
            type: FETCH_CUSTOMERS_ERROR,
            error: error
        })
    }   
  }
}

//xóa customers
export const deleteCustomerAction = (customerId) =>{
  return async (dispatch) => {
    try {
        var requestOptions = {
          method: 'DELETE', 
          headers: {
            'Content-Type': 'application/json', // Xác định kiểu dữ liệu của payload là JSON
            "x-access-token": accessToken
          },
          redirect: 'follow',
        };

        await dispatch({
            type: DELETE_CUSTOMERS_PENDING
        })
        let  url = "http://localhost:8080/customers/"+customerId;
        const responseCustomer = await fetch(url, requestOptions);
        const data = await responseCustomer.json();
        return dispatch({
            type: DELETE_CUSTOMERS_SUCCESS,
            message: data.message
        })
    } catch (error) {
        return dispatch({
            type: DELETE_CUSTOMERS_ERROR,
            error: error
        })
    }   
  }
}
//lấy khách hàng theo id trong db
export const fetchCustomerByIdAction = (customerId) =>{
  return async (dispatch) => {
    try {
        var requestOptions = {
            method: 'GET',
            redirect: 'follow',
            headers:{
              "x-access-token": accessToken
            }
        };

        await dispatch({
            type: FETCH_CUSTOMER_DETAIL_PENDING
        })
        let  url = "http://localhost:8080/customers/"+customerId;
        const responseCustomers = await fetch(url, requestOptions);
        const data = await responseCustomers.json();
        return dispatch({
            type: FETCH_CUSTOMER_DETAIL_SUCCESS,
            data: data.data
        })
    } catch (error) {
        return dispatch({
            type: FETCH_CUSTOMER_DETAIL_ERROR,
            error: error
        })
    }   
  }
}
//reset thông báo sau khi xóa khách hàng
export const resetCustomerDelete = () =>{
  return(
    {
      type: RESET_CUSTOMER
    }
  )
}
//thay doi pagination cua customer table
export const onChangePaginationCustomerAction = (currentPage) =>{
  return(
    {
      type: ON_CHANGE_PAGINATION_CUSTOMER,
      payload: currentPage
    }
  )
}
//lưu thông tin tìm kiếm khách hàng trong ô input
export const onChangeCustomerInfoAction = (searchContent) =>{
  return(
    {
      type: ON_CHANGE_CUSTOMER_INFO,
      payload: searchContent
    }
  )
}
//chuyển về trang đầu khi lọc khách hàng
export const resetToFirstPageAction = () =>{
  return(
    {
      type: RESET_TO_FIRST_PAGE,
    }
  )
}

//hàm xuất file excel
export const exportExcelCustomerFile = () => {
  return async (dispatch) => {
    try {
      var requestOptions = {
        method: 'GET', 
        redirect: 'follow',
        headers : {
          "x-access-token": accessToken
        }
      };

      dispatch({
        type: FETCH_EXPORT_CUSTOMER_PENDING
      });

      const response = await fetch("http://localhost:8080/customers/export", requestOptions);

      if (!response.ok) {
        throw new Error('Network response was not ok');
      }

      const blob = await response.blob();
      const url = window.URL.createObjectURL(blob);
      const a = document.createElement('a');
      a.href = url;
      a.download = 'customers.xlsx'; // Tên file tải về
      document.body.appendChild(a);
      a.click();
      a.remove();

      // Cleanup the URL object after download
      window.URL.revokeObjectURL(url);

      dispatch({
        type: FETCH_EXPORT_CUSTOMER_SUCCESS,
      });
    } catch (error) {
      dispatch({
        type: FETCH_EXPORT_CUSTOMER_ERROR,
        error: error.toString()
      });
    }   
  }
}