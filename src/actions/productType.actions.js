import {
  ADD_PRODUCT_TYPE_ERROR,
  ADD_PRODUCT_TYPE_PENDING,
  ADD_PRODUCT_TYPE_SUCCESS,
  FETCH_PRODUCTS_TYPE_ERROR,
  FETCH_PRODUCTS_TYPE_PENDING,
  FETCH_PRODUCTS_TYPE_SUCCESS,
  ON_CHANGE_PRODUCT_TYPE_ADD,
  RESET_TYPE_PRODUCT_MESSAGE,
  RESET_TYPE_PRODUCT_FORM,
  UPLOAD_IMAGE_TYPE_ERROR,
  UPLOAD_IMAGE_TYPE_PENDING,
  UPLOAD_IMAGE_TYPE_SUCCESS,
  FETCH_TYPE_DETAIL_PENDING,
  FETCH_TYPE_DETAIL_SUCCESS,
  FETCH_TYPE_DETAIL_ERROR,
  ON_CHANGE_PRODUCT_TYPE_EDIT,
  RESET_TYPE_PRODUCT_EDIT_FORM,
  EDIT_PRODUCT_TYPE_PENDING,
  EDIT_PRODUCT_TYPE_SUCCESS,
  EDIT_PRODUCT_TYPE_ERROR,
  ON_CHANGE_SEARCH_TYPE_NAME,
  ON_CHANGE_PAGINATION,
  SOFT_DELETE_PRODUCT_TYPE_PENDING,
  SOFT_DELETE_PRODUCT_TYPE_SUCCESS,
  SOFT_DELETE_PRODUCT_TYPE_ERROR,
  RESTORE_PRODUCT_TYPE_PENDING,
  RESTORE_PRODUCT_TYPE_SUCCESS,
  RESTORE_PRODUCT_TYPE_ERROR,
  FETCH_TYPES_DELETED_PENDING,
  FETCH_TYPES_DELETED_ERROR,
  FETCH_TYPES_DELETED_SUCCESS,
} from '../constants/productType.constant'

const adminLoginInfo = JSON.parse(localStorage.getItem('adminLoginInfo'))
var accessToken = ''
if (adminLoginInfo) {
  accessToken = adminLoginInfo[0].accessToken
}

//lấy toàn bộ loại sp
export const fetchProductTypesTableAction = (limit, currentPage, typeName) => {
  return async (dispatch) => {
    try {
      var requestOptions = {
        method: 'GET',
        redirect: 'follow',
      }

      await dispatch({
        type: FETCH_PRODUCTS_TYPE_PENDING,
      })
      let  url = "http://localhost:8080/productTypes";
      //lay tong toan bo san pham ko tinh phan trang
      const responseTypes = await fetch(url+"?searchContent="+typeName, requestOptions);
      const data = await responseTypes.json();
      //lay cac san pham theo phan trang
      const urlTypesWithPagination = url +"?limit="+limit
      +"&page="+currentPage+"&searchContent="+typeName;
      const responseTypesWithPagination= await fetch(urlTypesWithPagination, requestOptions);
      const dataTypesWithPagination= await responseTypesWithPagination.json();
      return dispatch({
        type: FETCH_PRODUCTS_TYPE_SUCCESS,
        types: dataTypesWithPagination.data,
        totalTypes: data.data.length
      })
    } catch (error) {
      return dispatch({
        type: FETCH_PRODUCTS_TYPE_ERROR,
        error: error,
      })
    }
  }
}
//lấy thông tin phân trang thay đổi
export const onChangePaginationTypeTable = (currentPage) => {
    return {
      type: ON_CHANGE_PAGINATION,
      payload: currentPage,
    }
  }
//lấy thông tin thay đổi trong ô input
export const onChangeProductTypeInfo = (infos) => {
  return {
    type: ON_CHANGE_PRODUCT_TYPE_ADD,
    payload: infos,
  }
}
//xử lý upload hình ảnh
export const uploadTypeImageAction = (formDataImg) => {
  return async (dispatch) => {
    try {
      var requestOptions = {
        method: 'POST',
        body: formDataImg,
      }

      await dispatch({
        type: UPLOAD_IMAGE_TYPE_PENDING,
      })

      const uploadImage = await fetch('http://localhost:8080/uploadType/', requestOptions)
      const data = await uploadImage.json()
      return dispatch({
        type: UPLOAD_IMAGE_TYPE_SUCCESS,
        data: data,
      })
    } catch (error) {
      return dispatch({
        type: UPLOAD_IMAGE_TYPE_ERROR,
        error: 'Error uploading the file ' + error,
      })
    }
  }
}
//thêm type sp mới
export const addProductTypeAction = (productTypeInfos) => {
  return async (dispatch) => {
    const bodyJson = {
      reqName: productTypeInfos.name,
      reqImageUrl: productTypeInfos.imageUrl,
      reqImageUrlBackground: productTypeInfos.imageUrlBackground,
      reqDescription: '',
    }
    try {
      var requestOptions = {
        method: 'POST', // phương thức 'POST'
        headers: {
          'Content-Type': 'application/json', // Xác định kiểu dữ liệu của payload là JSON
          'x-access-token': accessToken,
        },
        redirect: 'follow',
        body: JSON.stringify(bodyJson),
      }

      await dispatch({
        type: ADD_PRODUCT_TYPE_PENDING,
      })
      const createNewProduct = await fetch('http://localhost:8080/productTypes', requestOptions)
      const data = await createNewProduct.json()
      return dispatch({
        type: ADD_PRODUCT_TYPE_SUCCESS,
        data: data.data,
        message: data.message,
      })
    } catch (error) {
      return dispatch({
        type: ADD_PRODUCT_TYPE_ERROR,
        error: error,
      })
    }
  }
}
//reset states after add new successfully
export const resetMessageAction = () => {
  return {
    type: RESET_TYPE_PRODUCT_MESSAGE,
  }
}

//reset states after add new successfully
export const resetFormAddTypeAction = () => {
  return {
    type: RESET_TYPE_PRODUCT_FORM,
  }
}

//fetch product type detail by id
export const fetchProductTypeDetailAction = (typeId) => {
  return async (dispatch) => {
    try {
      var requestOptions = {
        method: 'GET',
        redirect: 'follow',
      }
      await dispatch({
        type: FETCH_TYPE_DETAIL_PENDING,
      })
      const responseTypeDetail = await fetch(
        'http://localhost:8080/productTypes/' + typeId,
        requestOptions,
      )
      const dataTypeDetail = await responseTypeDetail.json()
      return dispatch({
        type: FETCH_TYPE_DETAIL_SUCCESS,
        data: dataTypeDetail.data,
      })
    } catch (error) {
      return dispatch({
        type: FETCH_TYPE_DETAIL_ERROR,
        error: error,
      })
    }
  }
}
//get infos changed of product type in edit input
export const onChangeProductTypeEditInfo = (infos) => {
  return {
    type: ON_CHANGE_PRODUCT_TYPE_EDIT,
    payload: infos,
  }
}
//reset edit form
export const resetEditTypeFormAction = () => {
  return {
    type: RESET_TYPE_PRODUCT_EDIT_FORM,
  }
}
//edit product type detail
export const editProductTypeDetailAction = (typeId, editInfos) => {
  return async (dispatch) => {
    const bodyJson = {
      reqName: editInfos.name,
      reqImageUrl: editInfos.imageUrl,
      reqImageUrlBackground: editInfos.imageUrlBackground,
      reqDescription: '',
    }
    try {
      var requestOptions = {
        method: 'PUT',
        body: JSON.stringify(bodyJson),
        headers: {
          'Content-type': 'application/json; charset=UTF-8', //phải thêm contentYype để edit
          'x-access-token': accessToken,
        },
      }

      await dispatch({
        type: EDIT_PRODUCT_TYPE_PENDING,
      })
      const productTypeDetailEdit = await fetch(
        'http://localhost:8080/productTypes/' + typeId,
        requestOptions,
      )
      const data = await productTypeDetailEdit.json()
      return dispatch({
        type: EDIT_PRODUCT_TYPE_SUCCESS,
        data: data.data,
        message: data.message,
      })
    } catch (error) {
      return dispatch({
        type: EDIT_PRODUCT_TYPE_ERROR,
        error: error,
      })
    }
  }
}
//on change search type name
export const onChangeSearchTypeName = (typeName) => {
  return {
    type: ON_CHANGE_SEARCH_TYPE_NAME,
    payload: typeName,
  }
}
//xóa mềm Type
export const softDeleteTypeAction = (typeId) => {
  return async (dispatch) => {
      try {
          var requestOptions = {
              method: 'DELETE', 
              headers: {
              'Content-Type': 'application/json', // Xác định kiểu dữ liệu của payload là JSON
              'x-access-token': accessToken
            },
              redirect: 'follow',
          };
      
          await dispatch({
              type: SOFT_DELETE_PRODUCT_TYPE_PENDING
          })
          const softDelType = await fetch("http://localhost:8080/productTypes/softDelete/"+typeId, requestOptions);
          const data = await softDelType.json();
          return dispatch({
              type: SOFT_DELETE_PRODUCT_TYPE_SUCCESS,
              data: data
          })
          } catch (error) {
          return dispatch({
              type: SOFT_DELETE_PRODUCT_TYPE_ERROR,
              error: error
          })
      }   
  }
}
//restore type đã xóa mềm
export const restoreTypesDeletedAction = (typeId) =>{
  return async (dispatch) => {
    try {
      var requestOptions = {
        method: 'PUT', 
        headers: {
          'Content-Type': 'application/json', // Xác định kiểu dữ liệu của payload là JSON
          'x-access-token': accessToken
        },
        redirect: 'follow',
      };

      await dispatch({
          type: RESTORE_PRODUCT_TYPE_PENDING
      })

      const typeRestored = await fetch("http://localhost:8080/productTypes/restore/"+typeId, requestOptions);
      const data = await typeRestored.json();
      return dispatch({
          type: RESTORE_PRODUCT_TYPE_SUCCESS,
          data: data
      })
    } catch (error) {
      return dispatch({
          type: RESTORE_PRODUCT_TYPE_ERROR,
          error: error
      })
    }   
  }
}

//lấy toàn bộ orders xóa mềm
export const fetchTypesDeletedAction = () =>{
  return async (dispatch) => {
    try {
      var requestOptions = {
        method: 'GET', 
        redirect: 'follow',
        headers:{
          'x-access-token': accessToken
        }
      };
      await dispatch({
          type: FETCH_TYPES_DELETED_PENDING
      })
      const typesDeleted = await fetch("http://localhost:8080/productTypes/typesDeleted", requestOptions);
      const data = await typesDeleted.json();
      dispatch({
          type: FETCH_TYPES_DELETED_SUCCESS,
          data: data.data
      });

    } catch (error) {
      return dispatch({
          type: FETCH_TYPES_DELETED_ERROR,
          error: error
      })
    }   
  }
}