import { FETCH_PRODUCT_DETAIL_ERROR, FETCH_PRODUCT_DETAIL_PENDING, 
        FETCH_PRODUCT_DETAIL_SUCCESS,
        FETCH_ORDERS_PENDING,
        FETCH_ORDERS_SUCCESS,
        FETCH_ORDERS_ERROR,
        FETCH_ORDER_DETAIL_BY_ID_PENDING, 
        FETCH_ORDER_DETAIL_BY_ID_SUCCESS,
        FETCH_ORDER_DETAIL_BY_ID_ERROR,
        FETCH_ORDER_BY_ID_PENDING,
        FETCH_ORDER_BY_ID_SUCCESS,
        FETCH_ORDER_BY_ID_ERROR,
        ON_CHANGE_STATUS,
        RESET_STATUS,
        EDIT_CHANGE_STATUS_PENDING,
        EDIT_CHANGE_STATUS_SUCCESS,
        EDIT_CHANGE_STATUS_ERROR,
        DELETE_ORDER_PENDING,
        DELETE_ORDER_SUCCESS,
        DELETE_ORDER_ERROR,
        FETCH_ORDERS_IN_DAY_PENDING,
        FETCH_ORDERS_IN_DAY_SUCCESS,
        FETCH_ORDERS_IN_DAY_ERROR,
        SOFT_DELETE_ORDER_PENDING,
        SOFT_DELETE_ORDER_SUCCESS,
        SOFT_DELETE_ORDER_ERROR,
        RESTORE_ORDER_PENDING,
        RESTORE_ORDER_SUCCESS,
        RESTORE_ORDER_ERROR,
        FETCH_ORDERS_DELETED_PENDING,
        FETCH_ORDERS_DELETED_SUCCESS,
        FETCH_ORDERS_DELETED_ERROR,
        RESET_RESTORE,
        RESET_STATUS_MESSAGE,
        ON_CHANGE_PAGINATION_ORDER,
        ON_CHANGE_SEARCH_ORDER_INFO,
        ON_CHANGE_ORDER_DATE,
        ON_CHANGE_ORDER_SHIPPED,
        ON_CHANGE_ORDERED_STATUS,
        ON_CHANGE_PREPARING_STATUS,
        ON_CHANGE_DELIVERED_STATUS,
        ON_CHANGE_RECEIVED_STATUS,
        ON_CHANGE_CANCELED_STATUS,
        CHANGE_TO_FIRST_PAGE,
        FETCH_EXPORT_ORDER_SUCCESS,
        FETCH_EXPORT_ORDER_ERROR,
        SEND_EMAIL_PENDING,
        SEND_EMAIL_SUCCESS,
        SEND_EMAIL_ERROR,
        FETCH_EXPORT_ORDER_PENDING
      } from "../constants/order.constants";

const adminLoginInfo = JSON.parse(localStorage.getItem('adminLoginInfo'));
var accessToken = "";
if(adminLoginInfo){
  accessToken = adminLoginInfo[0].accessToken;
}
//xóa order
export const deleteOrderAction = (orderId) =>{
  return async (dispatch) => {
    try {
        var requestOptions = {
          method: 'DELETE', 
          headers: {
            'Content-Type': 'application/json', // Xác định kiểu dữ liệu của payload là JSON
            'x-access-token': accessToken
          },
          redirect: 'follow',
        };

        await dispatch({
            type: DELETE_ORDER_PENDING
        })
        let  url = "http://localhost:8080/orders/"+orderId;
        const responseOrder = await fetch(url, requestOptions);
        const data = await responseOrder.json();
        return dispatch({
            type: DELETE_ORDER_SUCCESS,
            message: data.message
        })
    } catch (error) {
        return dispatch({
            type: DELETE_ORDER_ERROR,
            error: error
        })
    }   
  }
}
//chỉnh sửa status đơn hàng
export const editStatusOrderAction = (status, orderId) =>{
  return async (dispatch) => {
    try {
        var requestOptions = {
          method: 'PUT',
          body: JSON.stringify({"reqStatus": status}),
          headers: {
              'Content-type': 'application/json; charset=UTF-8',
              'x-access-token': accessToken
          },
        };

        await dispatch({
            type: EDIT_CHANGE_STATUS_PENDING
        })
        let  url = "http://localhost:8080/orders/"+orderId;
        const responseOrder = await fetch(url, requestOptions);
        const data = await responseOrder.json();
        return dispatch({
            type: EDIT_CHANGE_STATUS_SUCCESS,
            message: data.message
        })
    } catch (error) {
        return dispatch({
            type: EDIT_CHANGE_STATUS_ERROR,
            error: error
        })
    }   
  }
}
//thay đổi status state khi thay đổi select option status
export const onChangeStatusOrderAction = (status)=>{
  return({
    type: ON_CHANGE_STATUS,
    data: status
  })
}
//reset status của sản phẩm trước thành rỗng trước khi chuyển xem status sp mới
export const resetStatusAction = ()=>{
  return({
    type: RESET_STATUS,
    data: ""
  })
}
//gửi email khi thay đổi status đơn hàng
export const sendEmailAction = (email, subject, content) =>{
  return async (dispatch) => {
    try {
        var requestOptions = {
          method: 'POST',
          body: JSON.stringify({
              "email":email,
              "subject":subject,
              "content":content
          }),
          headers: {
              'Content-type': 'application/json; charset=UTF-8',
              'x-access-token' : accessToken
          },
        };

        await dispatch({
            type: SEND_EMAIL_PENDING
        })
        let  url = "http://localhost:8080/email/send";
        const responseSendEmail = await fetch(url, requestOptions);
        const data = await responseSendEmail.json();
        return dispatch({
            type: SEND_EMAIL_SUCCESS,
            message: data
        })
    } catch (error) {
        return dispatch({
            type: SEND_EMAIL_ERROR,
            error: error
        })
    }   
  }
}
//lấy order theo id 
export const fetchOrderByIdAction = (orderId) => {
  return async (dispatch) => {
      try {
          var requestOptions = {
              method: 'GET',
              redirect: 'follow',
              headers:{
                'x-access-token': accessToken
              }
          };
  
          await dispatch({
              type: FETCH_ORDER_BY_ID_PENDING
          })
          let  url = "http://localhost:8080/orders/"+orderId;
          const responseOrder = await fetch(url, requestOptions);
          const data = await responseOrder.json();
          return dispatch({
              type: FETCH_ORDER_BY_ID_SUCCESS,
              data: data.data
          })
      } catch (error) {
          return dispatch({
              type: FETCH_ORDER_BY_ID_ERROR,
              error: error
          })
      }   
  }
}
//lấy toàn bộ orders
export const fetchOrdersAction = (limit, currentPage, searchContent,
   orderDate, shippedDate, ordered, preparing, delivered, received, canceled) => {
  return async (dispatch) => {
    try {
      var requestOptions = {
        method: 'GET', 
        redirect: 'follow',
        headers: {
          'x-access-token': accessToken
        },
      };

      await dispatch({
          type: FETCH_ORDERS_PENDING 
      })
      const url = "http://localhost:8080/orders";
      var totalOrders = 0;
      var orders = [];
      // trường hợp lọc theo điều kiện thì sẽ lấy phân trang trên tổng sp của đường link có chứa điều kiện
      if(limit != "null"){
        //link tất cả sp theo điều kiện (tất cả các trang) dùng để tính tổng phân trang
        const urlConditionOfAllItems =  url+"?search="+searchContent+"&orderDate="+orderDate+
        "&shippedDate="+shippedDate+"&ordered="+ordered+"&preparing="
        +preparing+"&delivered="+delivered+"&received="+received
        +"&canceled="+canceled;
        //fetch user theo đk các trang
        const responseItemsConditionAllPages = await fetch(urlConditionOfAllItems, requestOptions);
        const dataItemsConditionAllPage = await responseItemsConditionAllPages.json();
        totalOrders = dataItemsConditionAllPage.data.length;
        
        //link các sp khi truyền điều kiện vào theo mỗi trang
        const urlConditionItemsWithPageNumber = url+"?limit="+limit
        +"&page="+currentPage+"&search="+searchContent
        +"&orderDate="+orderDate+"&shippedDate="+shippedDate
        +"&ordered="+ordered+"&preparing="
        +preparing+"&delivered="+delivered+"&received="+received
        +"&canceled="+canceled;
        //fetch các user có đk theo phân trang từng trang 1
        const responseItemsConditionEachPage = await fetch(urlConditionItemsWithPageNumber, requestOptions);
        const dataItemsConditionEachPage = await responseItemsConditionEachPage.json();
        orders = dataItemsConditionEachPage.data;
      }else{
        //trường hợp ko có điều kiện thì phân trang lấy trên tổng toàn bộ user đang có
        const responseTotalOrders = await fetch(url, requestOptions);
        const dataTotalOrders = await responseTotalOrders.json();
        totalOrders = dataTotalOrders.data.length;        
      }        
      dispatch({
          type: FETCH_ORDERS_SUCCESS,
          totalOrders: totalOrders,
          orders: orders
      });

    } catch (error) {
      return dispatch({
          type: FETCH_ORDERS_ERROR,
          error: error
      })
    }   
  }
}
// lấy thông tin phân trang thay đổi
export const onChangePaginationOrderAction = (currentPage)=>{
  return({
    type: ON_CHANGE_PAGINATION_ORDER,
    payload: currentPage
  })
}
// lấy thông tin trong ô tìm kiếm ở mục filter của bảng
export const onChangeSearchOrderInfoAction = (searchContent)=>{
  return({
    type: ON_CHANGE_SEARCH_ORDER_INFO,
    payload: searchContent
  })
}
// lấy thông tin orderDate ô tìm kiếm
export const onChangeOrderDateAction = (orderDate)=>{
  return({
    type: ON_CHANGE_ORDER_DATE,
    payload: orderDate
  })
}
// lấy thông tin shippedDate ô tìm kiếm
export const onChangeOrderShippedAction = (shippedDate)=>{
  return({
    type: ON_CHANGE_ORDER_SHIPPED,
    payload: shippedDate
  })
}
//lấy thông tin ordered status khi lọc
export const onChangeOrderedStatusAction = (ordered) =>{
  return (
    {
      type: ON_CHANGE_ORDERED_STATUS,
      payload: ordered
    }
  )
}
//lấy thông tin preparing status khi lọc
export const onChangePreparingStatusAction = (preparing) =>{
  return (
    {
      type: ON_CHANGE_PREPARING_STATUS,
      payload: preparing
    }
  )
}
//lấy thông tin delivered status khi lọc
export const onChangeDeliveredStatusAction = (delivered) =>{
  return (
    {
      type: ON_CHANGE_DELIVERED_STATUS,
      payload: delivered
    }
  )
}
//lấy thông tin received status khi lọc
export const onChangeReceivedStatusAction = (received) =>{
  return (
    {
      type: ON_CHANGE_RECEIVED_STATUS,
      payload: received
    }
  )
}
export const changeToFirstPageAction = () => {
  return(
    {
      type:CHANGE_TO_FIRST_PAGE
    }
  )
}
//lấy thông tin canceled status khi lọc
export const onChangeCanceledStatusAction = (canceled) =>{
  return (
    {
      type: ON_CHANGE_CANCELED_STATUS,
      payload: canceled
    }
  )
}
//hàm xuất file excel
export const exportExcelOrderFile = () => {
  return async (dispatch) => {
    try {
      var requestOptions = {
        method: 'GET', 
        redirect: 'follow',
        headers : {
          "x-access-token": accessToken
        }
      };

      dispatch({
        type: FETCH_EXPORT_ORDER_PENDING
      });

      const response = await fetch("http://localhost:8080/orders/export", requestOptions);

      if (!response.ok) {
        throw new Error('Network response was not ok');
      }

      const blob = await response.blob();
      const url = window.URL.createObjectURL(blob);
      const a = document.createElement('a');
      a.href = url;
      a.download = 'orders.xlsx'; // Tên file tải về
      document.body.appendChild(a);
      a.click();
      a.remove();

      // Cleanup the URL object after download
      window.URL.revokeObjectURL(url);

      dispatch({
        type: FETCH_EXPORT_ORDER_SUCCESS,
      });

    } catch (error) {
      dispatch({
        type: FETCH_EXPORT_ORDER_ERROR,
        error: error.toString()
      });
    }   
  }
}
//lấy sp chi tiết để hiển thị khi xem orderDetail các sp trong order
export const fetchProductDetailAction = (productId) => {
  return async (dispatch) => {
    try {
      var requestOptions = {
          method: 'GET', 
          redirect: 'follow',
          
      };

      await dispatch({
          type: FETCH_PRODUCT_DETAIL_PENDING
      })

      const productDetail = await fetch("http://localhost:8080/products/"+productId, requestOptions);
      const data = await productDetail.json();
      return dispatch({
          type: FETCH_PRODUCT_DETAIL_SUCCESS,
          data: data.data
      })
    } catch (error) {
      return dispatch({
          type: FETCH_PRODUCT_DETAIL_ERROR,
          error: error
      })
    }   
  }
}
//lấy các orders trong ngày hiện tại
export const fetchOrdersInDayAction = () =>{
  return async (dispatch) => {
    try {
      var requestOptions = {
        method: 'GET', 
        redirect: 'follow',
        headers: {
          'x-access-token': accessToken
        },
      };

      await dispatch({
          type: FETCH_ORDERS_IN_DAY_PENDING 
      })

      const ordersLatest = await fetch("http://localhost:8080/orders/latest", requestOptions);
      const data = await ordersLatest.json();
      dispatch({
          type: FETCH_ORDERS_IN_DAY_SUCCESS,
          data: data.data
      });

    } catch (error) {
      return dispatch({
          type: FETCH_ORDERS_IN_DAY_ERROR,
          error: error
      })
    }   
  }
}

//lấy các orderDetail trong order
export const fetchOrderDetailByIdAction = (orderIdArray) =>{
  return async (dispatch) => {
    try {
      var requestOptions = {
        method: 'GET', 
        redirect: 'follow',
        headers: {
          'x-access-token': accessToken
        },
      };

      await dispatch({
          type: FETCH_ORDER_DETAIL_BY_ID_PENDING
      })

       // Fetch all order details in parallel
      const fetchPromises = orderIdArray.map(orderId => 
        fetch(`http://localhost:8080/orderDetails/${orderId}`, requestOptions)
          .then(response => response.json())
      );

      const orderDetails = await Promise.all(fetchPromises);
      dispatch({
          type: FETCH_ORDER_DETAIL_BY_ID_SUCCESS,
          data: orderDetails
      });

    } catch (error) {
      return dispatch({
          type: FETCH_ORDER_DETAIL_BY_ID_ERROR,
          error: error
      })
    }   
  }
}
//xóa mềm order
export const softDeleteOrderAction = (orderId) => {
  return async (dispatch) => {
      try {
          var requestOptions = {
              method: 'DELETE', 
              headers: {
              'Content-Type': 'application/json', // Xác định kiểu dữ liệu của payload là JSON
              'x-access-token': accessToken
            },
              redirect: 'follow',
          };
      
          await dispatch({
              type: SOFT_DELETE_ORDER_PENDING
          })
      
          const softDelOrder = await fetch("http://localhost:8080/orders/softDelete/"+orderId, requestOptions);
          const data = await softDelOrder.json();
          return dispatch({
              type: SOFT_DELETE_ORDER_SUCCESS,
              data: data
          })
          } catch (error) {
          return dispatch({
              type: SOFT_DELETE_ORDER_ERROR,
              error: error
          })
      }   
  }
}
//restore order đã xóa mềm
export const restoreOrdersDeletedAction = (orderId) =>{
  return async (dispatch) => {
    try {
      var requestOptions = {
        method: 'PUT', 
        headers: {
          'Content-Type': 'application/json', // Xác định kiểu dữ liệu của payload là JSON
          'x-access-token': accessToken
        },
        redirect: 'follow',
      };

      await dispatch({
          type: RESTORE_ORDER_PENDING
      })

      const orderRestored = await fetch("http://localhost:8080/orders/restore/"+orderId, requestOptions);
      const data = await orderRestored.json();
      return dispatch({
          type: RESTORE_ORDER_SUCCESS,
          data: data
      })
    } catch (error) {
      return dispatch({
          type: RESTORE_ORDER_ERROR,
          error: error
      })
    }   
  }
}
//reset lại message sau khi thực hiện restore những sp xóa mềm
export const resetRestoreOrderAction = ()=>{
  return(
    {
      type: RESET_RESTORE,
    }
  )
}
//lấy toàn bộ orders xóa mềm
export const fetchOrdersDeletedAction = () =>{
  return async (dispatch) => {
    try {
      var requestOptions = {
        method: 'GET', 
        redirect: 'follow',
        headers:{
          'x-access-token': accessToken
        }
      };

      await dispatch({
          type: FETCH_ORDERS_DELETED_PENDING
      })

      const ordersDeleted = await fetch("http://localhost:8080/orders/deleted", requestOptions);
      const data = await ordersDeleted.json();
      dispatch({
          type: FETCH_ORDERS_DELETED_SUCCESS,
          data: data.data
      });

    } catch (error) {
      return dispatch({
          type: FETCH_ORDERS_DELETED_ERROR,
          error: error
      })
    }   
  }
}
//reset message của order sau khi thực hiện 1 action thành công
export const resetStatusMessageAction = () => {
  return(
    {
      type : RESET_STATUS_MESSAGE
    }
  )
}