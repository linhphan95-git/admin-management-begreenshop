import { CHANGE_LOGIN_VALUE_ADMIN, CHANGE_PASSWORD, CHANGE_TO_FIRST_PAGE, CHANGE_USER_INFO_ERROR,
      CHANGE_USER_INFO_PENDING, CHANGE_USER_INFO_SUCCESS, DELETE_USER_ERROR,
      DELETE_USER_PENDING, DELETE_USER_SUCCESS, FETCH_EXPORT_USER_ERROR, FETCH_EXPORT_USER_PENDING, FETCH_EXPORT_USER_SUCCESS, FETCH_USERS_ERROR, 
      FETCH_USERS_PENDING, FETCH_USERS_SUCCESS, FETCH_USER_DELETED_ERROR,
      FETCH_USER_DELETED_PENDING, FETCH_USER_DELETED_SUCCESS,
      FETCH_USER_DETAIL_ERROR, FETCH_USER_DETAIL_PENDING, 
      FETCH_USER_DETAIL_SUCCESS, LOGIN_ADMIN_ERROR, LOGIN_ADMIN_PENDING,
      LOGIN_ADMIN_SUCCESS, LOGOUT, ON_CHANGE_ADMIN_INFO, 
      ON_CHANGE_ADMIN_ROLE, 
      ON_CHANGE_MODERATOR_ROLE, 
      ON_CHANGE_PAGINATION, 
      ON_CHANGE_SEARCH_USER, 
      ON_CHANGE_USER_ROLE, 
      RESET_ADMIN_INFO_EDIT_FORM, RESET_DELETE_USER_MESSAGE, 
      RESET_RESTORE, RESET_USER_EDIT_MESSAGE, RESTORE_USER_DELETED_ERROR, 
      RESTORE_USER_DELETED_PENDING, RESTORE_USER_DELETED_SUCCESS, 
      UPLOAD_ADMIN_INFO_ERROR, UPLOAD_ADMIN_INFO_PENDING, 
      UPLOAD_ADMIN_INFO_SUCCESS, UPLOAD_IMAGE_ADMIN_ERROR,
      UPLOAD_IMAGE_ADMIN_PENDING, UPLOAD_IMAGE_ADMIN_SUCCESS } from "../constants/user.constants";

const adminLoginInfo = JSON.parse(localStorage.getItem('adminLoginInfo'))||[];
var accessToken = "";
if(adminLoginInfo.length>0){
  accessToken = adminLoginInfo[0].accessToken;
}
//lấy toàn bộ user
export const fetchUsersAction = (limit, currentPage, searchContent, isAdmin, isModerator, isUser) => {
  return async (dispatch) => {
      try {
          var requestOptions = {
              method: 'GET',
              redirect: 'follow',
              headers: {
                'x-access-token': accessToken
              },
          };
  
          await dispatch({
              type: FETCH_USERS_PENDING
          })
          const url = "http://localhost:8080/users";
          var totalUsers = 0;
          var users = [];
          //trường hợp lọc theo điều kiện thì sẽ lấy phân trang trên tổng sp của đường link có chứa điều kiện
          if(searchContent !== "" || isAdmin || isModerator || isUser){
            //link tất cả sp theo điều kiện (tất cả các trang) dùng để tính tổng phân trang
            const urlConditionOfAllItems =  url+"?search="+searchContent+"&admin="+isAdmin
            +"&moderator="+isModerator+"&user="+isUser;
            //fetch user theo đk các trang
            const responseItemsConditionAllPages = await fetch(urlConditionOfAllItems, requestOptions);
            const dataItemsConditionAllPage = await responseItemsConditionAllPages.json();
            totalUsers = dataItemsConditionAllPage.data.length;
            
            //link các sp khi truyền điều kiện vào theo mỗi trang
            const urlConditionItemsWithPageNumber = url+"?limit="+limit+"&page="+currentPage+"&search="+searchContent+"&admin="+isAdmin
            +"&moderator="+isModerator+"&user="+isUser ;
            //fetch các user có đk theo phân trang từng trang 1
            const responseItemsConditionEachPage = await fetch(urlConditionItemsWithPageNumber, requestOptions);
            const dataItemsConditionEachPage = await responseItemsConditionEachPage.json();
            users = dataItemsConditionEachPage.data;
          }else{
            //trường hợp ko có điều kiện thì phân trang lấy trên tổng toàn bộ user đang có
            const responseTotalUsers = await fetch(url, requestOptions);
            const dataTotalUsers = await responseTotalUsers.json();
            totalUsers = dataTotalUsers.data.length;
            const responseUsersByPage = await fetch(url+"?limit="+limit+"&page="+currentPage, requestOptions);
            const dataUsersByPage = await responseUsersByPage.json();
            users = dataUsersByPage.data; 
          }        
          return dispatch({
              type: FETCH_USERS_SUCCESS,
              data: users,
              totalUsers: totalUsers
          })
      } catch (error) {
          return dispatch({
              type: FETCH_USERS_ERROR,
              error: error
          })
      }   
  }
}
//lưu phân trang thay đổi vào state
export const onChangePaginationUserAction = (currentPage) =>{
  return (
    {
      type: ON_CHANGE_PAGINATION,
      payload: currentPage
    }
  )
}
//lưu admin role thay đổi vào state
export const onChangeAdminRoleAction = (isAdmin) =>{
  return (
    {
      type: ON_CHANGE_ADMIN_ROLE,
      payload: isAdmin
    }
  )
}
//lưu user role thay đổi vào state
export const onChangeUserRoleAction = (isUser) =>{
  return (
    {
      type: ON_CHANGE_USER_ROLE,
      payload: isUser
    }
  )
}   
//lưu Moderator role thay đổi vào state
export const onChangeModeratorRoleAction = (isModerator) =>{
  return (
    {
      type: ON_CHANGE_MODERATOR_ROLE,
      payload: isModerator
    }
  )
}
//lưu Moderator role thay đổi vào state
export const onChangeFirstPage = () =>{
  return (
    {
      type: CHANGE_TO_FIRST_PAGE,
    }
  )
}
//hàm xuất file excel
export const exportExcelUserFile = () => {
  return async (dispatch) => {
    try {
      var requestOptions = {
        method: 'GET', 
        redirect: 'follow',
        headers : {
          "x-access-token": accessToken
        }
      };

      dispatch({
        type: FETCH_EXPORT_USER_PENDING
      });

      const response = await fetch("http://localhost:8080/users/export", requestOptions);

      if (!response.ok) {
        throw new Error('Network response was not ok');
      }

      const blob = await response.blob();
      const url = window.URL.createObjectURL(blob);
      const a = document.createElement('a');
      a.href = url;
      a.download = 'users.xlsx'; // Tên file tải về
      document.body.appendChild(a);
      a.click();
      a.remove();

      // Cleanup the URL object after download
      window.URL.revokeObjectURL(url);

      dispatch({
        type: FETCH_EXPORT_USER_SUCCESS,
      });

    } catch (error) {
      dispatch({
        type: FETCH_EXPORT_USER_ERROR,
        error: error.toString()
      });
    }   
  }
}
//lấy user theo username
export const fetchUserDetailAction = (username) => {
    return async (dispatch) => {
        try {
            var requestOptions = {
                method: 'GET',
                redirect: 'follow',
                headers: {
                  'x-access-token': accessToken
                },
            };
            await dispatch({
                type: FETCH_USER_DETAIL_PENDING
            })
            const responseUser = await fetch("http://localhost:8080/users/username/"+username, requestOptions);
            const data = await responseUser.json();
            return dispatch({
                type: FETCH_USER_DETAIL_SUCCESS,
                data: data
            })
        } catch (error) {
            return dispatch({
                type: FETCH_USER_DETAIL_ERROR,
                error: error
            })
        }   
    }
}
//chỉnh sửa role của user trong trang users
export const changeUserInfoAction = (userId, roleId) => {
    return async (dispatch) => {
        try {
            var requestOptions = {
              method: 'PUT',
              body: JSON.stringify(
                {
                  "roleId": roleId,
                }
              ),
              headers: {
                  'Content-type': 'application/json; charset=UTF-8',
                  'x-access-token': accessToken
              },
            };
    
            await dispatch({
                type: CHANGE_USER_INFO_PENDING
            })
            let  url = "http://localhost:8080/users/roles/"+userId;
            const responseUser= await fetch(url, requestOptions);
            const data = await responseUser.json();
            return dispatch({
                type: CHANGE_USER_INFO_SUCCESS,
                data: data
            })
        } catch (error) {
            return dispatch({
                type: CHANGE_USER_INFO_ERROR,
                error: error
            })
        }   
    }
}
//lưu thông tin password thay đổi vào state
export const changePasswordAction = (password) =>{
    return({
        type:CHANGE_PASSWORD,
        payload: password
    })
}
//reset state của message sau khi thực hiện edit
export const resetEditUserMessage = () =>{
  return({
      type: RESET_USER_EDIT_MESSAGE
  })
}
//xử lý xóa mềm 1 người dùng
export const deleteUserAction = (userId) => {
    return async (dispatch) => {
        try {
            var requestOptions = {
                method: 'DELETE', 
                headers: {
                'Content-Type': 'application/json', // Xác định kiểu dữ liệu của payload là JSON
                'x-access-token': accessToken
                },
                redirect: 'follow',
            };
        
            await dispatch({
                type: DELETE_USER_PENDING
            })
        
            const softDelUser = await fetch("http://localhost:8080/users/softDelete/"+userId, requestOptions);
            const data = await softDelUser.json();
            return dispatch({
                type: DELETE_USER_SUCCESS,
                data: data
            })
            } catch (error) {
            return dispatch({
                type: DELETE_USER_ERROR,
                error: error
            })
        }   
    }
}
//reset thông báo trong state sau khi xóa user
export const resetDeleteUserMessage = () =>{
    return({
        type: RESET_DELETE_USER_MESSAGE
    })
}
//lấy các user bị xóa mềm
export const fetchUsersDeletedAction = () => {
    return async (dispatch) => {
      try {
        var requestOptions = {
          method: 'GET', 
          redirect: 'follow',
          headers: {
            'x-access-token': accessToken
          },
        };
  
        await dispatch({
            type: FETCH_USER_DELETED_PENDING
        })
  
        const usersDeletedResponse = await fetch("http://localhost:8080/users/deleted/", requestOptions);
        const data = await usersDeletedResponse.json();
        return dispatch({
            type: FETCH_USER_DELETED_SUCCESS,
            data: data.data
        })
      } catch (error) {
        return dispatch({
            type: FETCH_USER_DELETED_ERROR,
            error: error
        })
      }   
    }
  }
//restore 1 user xóa mềm
  export const restoreUsersDeletedAction = (userId) =>{
    return async (dispatch) => {
      try {
        var requestOptions = {
          method: 'PUT', 
          headers: {
            'Content-Type': 'application/json', // Xác định kiểu dữ liệu của payload là JSON
            'x-access-token': accessToken 
          },
          redirect: 'follow',
        };
  
        await dispatch({
            type: RESTORE_USER_DELETED_PENDING
        })
  
        const userRestored = await fetch("http://localhost:8080/users/restore/"+userId, requestOptions);
        const data = await userRestored.json();
        return dispatch({
            type: RESTORE_USER_DELETED_SUCCESS,
            data: data
        })
      } catch (error) {
        return dispatch({
            type: RESTORE_USER_DELETED_ERROR,
            error: error
        })
      }   
    }
  }
  //xóa state message sau khi thực hiện restore 1 user xóa mềm
  export const resetRestoreAction = ()=>{
    return(
      {
        type: RESET_RESTORE,
      }
    )
  }
  //lưu thông tin vào state khi nhập vào ô Login
  export const changeLoginValueFormAdminAction = (loginInfo) =>{
    return({
      type: CHANGE_LOGIN_VALUE_ADMIN,
      data: loginInfo
    })
  }
  //thực hiện login
  export const loginAdminAction = (loginInfoInput) =>{
    return async (dispatch) => {
      try {
        var requestOptions = {
            method: 'POST', // phương thức 'POST'
            headers: {
              'Content-Type': 'application/json',
              // 'x-access-token': accessToken 
            },
            
            redirect: 'follow',
            body: JSON.stringify(loginInfoInput)
        };
  
        await dispatch({
            type: LOGIN_ADMIN_PENDING
        })
  
        const login = await fetch("http://localhost:8080/api/auth/login", requestOptions);
        const data = await login.json();
        return dispatch({
            type: LOGIN_ADMIN_SUCCESS,
            data: data,
            loginInfoInput: loginInfoInput
        })
      } catch (error) {
        return dispatch({
            type: LOGIN_ADMIN_ERROR,
            error: error
        })
      }   
    }
  }
  //xử lý logout
  export const logoutAction = () => {
    return({
      type: LOGOUT
    })
  }
//lưu thay đổi thông tin admin vào state
export const onChangeAdminInfoAction = (adminInfoEdited) =>{
  return({
    type: ON_CHANGE_ADMIN_INFO,
    payload: adminInfoEdited
  })
}
//xử lý upload avatar của user
export const uploadImageAction = (formDataImg) => {
  return async (dispatch) => {
    try {
      var requestOptions = {
        method: 'POST',
        body: formDataImg,
        headers: {
          'Content-type': 'application/json; charset=UTF-8',
          'x-access-token': accessToken
        },
      };

      await dispatch({
          type: UPLOAD_IMAGE_ADMIN_PENDING
      })

      const uploadImage = await fetch("http://localhost:8080/upload/", requestOptions);
      const data = await uploadImage.json();
      return dispatch({
          type: UPLOAD_IMAGE_ADMIN_SUCCESS,
          data: data
      })
    } catch (error) {
      return dispatch({
          type: UPLOAD_IMAGE_ADMIN_ERROR,
          error: "Error uploading the file " + error
      })
    }   
  }
}
//chỉnh sửa password và avatar của user trong trang admin profile
export const uploadAdminInfoAction = (userInfoEdited, userId) => {
  return async (dispatch) => {
    try {
      var requestOptions = {
        method: 'PUT',
        body: JSON.stringify(userInfoEdited),
        headers: {
          'Content-type': 'application/json; charset=UTF-8',
          'x-access-token': accessToken
        },
      };

      await dispatch({
          type: UPLOAD_ADMIN_INFO_PENDING
      })

      const uploadUser = await fetch("http://localhost:8080/users/"+userId, requestOptions);
      const data = await uploadUser.json();
      return dispatch({
          type: UPLOAD_ADMIN_INFO_SUCCESS,
          data: data
      })
    } catch (error) {
      return dispatch({
          type: UPLOAD_ADMIN_INFO_ERROR,
          error: "Error uploading " + error
      })
    }   
  }
}
//reset thông tin admin trong form sau khi chỉnh sửa xong
export const resetAdminInfoEditFormAction = () =>{
  return (
    {
      type:RESET_ADMIN_INFO_EDIT_FORM,
    }
  )
}
//lưu thông tin tìm kiếm user trong input vào state
export const onChangeSearchUserAction = (searchContent) =>{
  return (
    {
      type: ON_CHANGE_SEARCH_USER,
      payload: searchContent
    }
  )
}

