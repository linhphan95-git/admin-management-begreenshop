import { ADD_NEW_PRODUCT_ERROR, ADD_NEW_PRODUCT_PENDING, ADD_NEW_PRODUCT_SUCCESS, ALLSTOCK_CHECKED, CHANGE_MAX_PRICE, CHANGE_MIN_PRICE,
        CHANGE_PAGINATION, CHANGE_TO_FIRST_PAGE, CHECK_PRODUCT_IN_ORDER_DETAIL_ERROR, CHECK_PRODUCT_IN_ORDER_DETAIL_PENDING, CHECK_PRODUCT_IN_ORDER_DETAIL_SUCCESS, EDIT_NEW_PRODUCT_ERROR, EDIT_NEW_PRODUCT_PENDING, EDIT_NEW_PRODUCT_SUCCESS, FETCH_FEATURE_PRODUCT_ERROR, FETCH_FEATURE_PRODUCT_PENDING, FETCH_FEATURE_PRODUCT_SUCCESS, FETCH_PRODUCTS_ERROR, FETCH_PRODUCTS_PENDING,
        FETCH_PRODUCTS_SUCCESS, FETCH_PRODUCT_DELETED_ERROR, FETCH_PRODUCT_DELETED_PENDING, FETCH_PRODUCT_DELETED_SUCCESS, FETCH_PRODUCT_DETAIL_ERROR, FETCH_PRODUCT_DETAIL_PENDING, FETCH_PRODUCT_DETAIL_SUCCESS, FETCH_PRODUCT_TYPES_ERROR,
        FETCH_PRODUCT_TYPES_PENDING, FETCH_PRODUCT_TYPES_SUCCESS, 
        HARD_DELETE_ERROR, 
        HARD_DELETE_PENDING, 
        HARD_DELETE_SUCCESS, 
        INSTOCK_CHECKED, ON_CHANGE_INPUT_EDIT_FORM, ON_CHANGE_INPUT_FORM, OUTSTOCK_CHECKED, RESET_AFTER_CREATE, RESET_DELETE, RESET_EDIT_FORM, RESET_EDIT_MESSAGE, RESET_PRODUCT_IN_ORDER_DETAIL, RESET_RESTORE, RESTORE_PRODUCT_DELETED_ERROR, RESTORE_PRODUCT_DELETED_PENDING, RESTORE_PRODUCT_DELETED_SUCCESS, SEARCH_PRODUCT_NAME,
        SOFT_DELETE_ERROR,
        SOFT_DELETE_PENDING,
        SOFT_DELETE_SUCCESS,
        TYPE_PRODUCT_CHECKED, 
        UPLOAD_IMAGE_ERROR, 
        UPLOAD_IMAGE_PENDING,
        UPLOAD_IMAGE_SUCCESS,
        FETCH_EXPORT_PRODUCT_PENDING, FETCH_EXPORT_PRODUCT_SUCCESS, FETCH_EXPORT_PRODUCT_ERROR
      } from "../constants/admin.constants";

const adminLoginInfo = JSON.parse(localStorage.getItem('adminLoginInfo'));
var accessToken = "";
if(adminLoginInfo){
  accessToken = adminLoginInfo[0].accessToken;
}
//lấy tất cả loại sp đang có
export const fetchProductTypesAction = () => {
  return async (dispatch) => {
      try {
          var requestOptions = {
              method: 'GET',
              redirect: 'follow',
              
          };
  
          await dispatch({
              type: FETCH_PRODUCT_TYPES_PENDING
          })

          const responseTotalTypes = await fetch("http://localhost:8080/productTypes", requestOptions);
          const dataTotalTypes = await responseTotalTypes.json();
          return dispatch({
              type: FETCH_PRODUCT_TYPES_SUCCESS,
              data: dataTotalTypes.data
          })
      } catch (error) {
          return dispatch({
              type: FETCH_PRODUCT_TYPES_ERROR,
              error: error
          })
      }   
  }
}
//lấy tất cả sp      
export const fetchProductsAdminAction = (limit, currentPage, minPrice, maxPrice, productName,inStock, outStock, allStock, typeId) => {
  return async (dispatch) => {
      try {
          var requestOptions = {
              method: 'GET',
              redirect: 'follow',
              headers: {
                'x-access-token': accessToken
              },
          };
  
          await dispatch({
              type: FETCH_PRODUCTS_PENDING
          })
          var totalProduct = 0;
          var products = "";
          var page = currentPage;
          let  url = "http://localhost:8080/products";
          let urlCondition = "";
          let urlConditionPage = "";
          //trường hợp lọc theo điều kiện thì sẽ lấy phân trang trên tổng sp của đường link có chứa điều kiện
          if(minPrice || maxPrice || productName || inStock || outStock || allStock || (Array.isArray(typeId))){
            const productTypeIdString = typeId.join(",");
            //tổng sp lấy theo toàn bộ sp có điều kiện 
            if(productTypeIdString){
              //nếu có tìm theo type product
              urlCondition =  url+"?min="+minPrice +"&max="+maxPrice+"&name="+productName+"&inStock="+inStock+"&outStock="+outStock+"&allStock="+allStock+"&typeId="+productTypeIdString;
              urlConditionPage = url+"?limit="+limit+"&page="+page+"&min="+minPrice +"&max="+maxPrice+"&name="+productName+"&inStock="+inStock+"&outStock="+outStock+"&allStock="+allStock+"&typeId="+productTypeIdString;
            }else{
              //ko có type product
              urlConditionPage = url+"?limit="+limit+"&page="+page+"&min="+minPrice +"&max="+maxPrice+"&name="+productName+"&inStock="+inStock+"&outStock="+outStock+"&allStock="+allStock;
              urlCondition =  url+"?min="+minPrice +"&max="+maxPrice+"&name="+productName+"&inStock="+inStock+"&outStock="+outStock+"&allStock="+allStock;
            }
            const responseTotalProductsByCondition = await fetch(urlCondition, requestOptions);
            const dataTotalProductsByCondition = await responseTotalProductsByCondition.json();
            //lấy tổng số toàn bộ sp để tạo phân trang
            totalProduct = dataTotalProductsByCondition.data.length;
            //sau khi lọc nếu chỉ tìm được 1 trang thì sẽ hiển thị ngay trang 1 
            if(totalProduct<= limit){
              page = 1;
            }
            //lấy ra các sp có đk theo phân trang từng trang 1
            const responseProductsByConditionByPage = await fetch(urlConditionPage, requestOptions);
            const dataProductsByConditionByPage = await responseProductsByConditionByPage.json();
            products = dataProductsByConditionByPage.data;
          }else{
            //trường hợp ko có điều kiện thì phân trang lấy trên tổng toàn bộ sp đang có
            const responseTotalProducts = await fetch(url, requestOptions);
            const dataTotalProducts = await responseTotalProducts.json();
            totalProduct = dataTotalProducts.data.length;
            const responseProductsByPage = await fetch(url+"?limit="+limit+"&page="+currentPage, requestOptions);
            const dataProductsByPage = await responseProductsByPage.json();
            products = dataProductsByPage.data; 
          }          
          return dispatch({
              type: FETCH_PRODUCTS_SUCCESS,
              productsByPage: products,
              totalProducts: totalProduct
          })
      } catch (error) {
          return dispatch({
              type: FETCH_PRODUCTS_ERROR,
              error: error
          })
      }   
  }
}
//chuyển sp về trang 1 khi filter sp
export const changeToFirstPageAction = () => {
  return(
    {
      type:CHANGE_TO_FIRST_PAGE
    }
  )
}
//lưu phân trang thay đổi vào state
export const onChangePaginationAction = (currentPage) =>{
  return({
    type: CHANGE_PAGINATION,
    currentPage: currentPage
  })
}
//lưu giá lọc nhỏ nhất thay đổi vào state
export const changeMinPriceAction = (priceMin) => {
  return ({
    type: CHANGE_MIN_PRICE,
    payload: priceMin
  })
}
//lưu giá lọc lớn nhất thay đổi vào state
export const changeMaxPriceAction = (priceMax) => {
  return ({
    type: CHANGE_MAX_PRICE,
    payload: priceMax
  })
}
//lưu tên sp thay đổi vào state

export const searchProductNameAction = (productName) => {
  return ({
    type: SEARCH_PRODUCT_NAME,
    payload: productName
  })
}
//lưu checkbox value instock thay đổi vào state

export const checkInStockAction = (inStock) => {
  return ({
    type: INSTOCK_CHECKED,
    payload: inStock
  })
}
//lưu checkbox value outstock thay đổi vào state

export const checkOutStockAction = (outStock) => {
  return ({
    type: OUTSTOCK_CHECKED,
    payload: outStock
  })
}
//lưu checkbox value all product thay đổi vào state

export const checkAllStockAction = (allStock) => {
  return ({
    type: ALLSTOCK_CHECKED,
    payload: allStock
  })
}
//lưu checkbox value loại sp thay đổi vào state

export const checkTypeProductAction = (typeId) => {
  return ({
    type: TYPE_PRODUCT_CHECKED,
    payload: typeId
  })
}
//lưu giá trị thay đôi của ô input khi tạo mới
export const onChangeInputAction = (infos) => {
  return ({
    type: ON_CHANGE_INPUT_FORM,
    payload: infos
  })
}
////lưu giá trị thay đôi của ô input khi edit
export const onChangeInputEditAction = (infos) => {
  return ({
    type: ON_CHANGE_INPUT_EDIT_FORM,
    payload: infos
  })
}
//hàm xuất file excel
export const exportExcelProductFile = () => {
  return async (dispatch) => {
    try {
      var requestOptions = {
        method: 'GET', 
        redirect: 'follow',
        headers : {
          "x-access-token": accessToken
        }
      };

      dispatch({
        type: FETCH_EXPORT_PRODUCT_PENDING
      });

      const response = await fetch("http://localhost:8080/products/export", requestOptions);

      if (!response.ok) {
        throw new Error('Network response was not ok');
      }

      const blob = await response.blob();
      const url = window.URL.createObjectURL(blob);
      const a = document.createElement('a');
      a.href = url;
      a.download = 'products.xlsx'; // Tên file tải về
      document.body.appendChild(a);
      a.click();
      a.remove();

      // Cleanup the URL object after download
      window.URL.revokeObjectURL(url);

      dispatch({
        type: FETCH_EXPORT_PRODUCT_SUCCESS,
      });

    } catch (error) {
      dispatch({
        type: FETCH_EXPORT_PRODUCT_ERROR,
        error: error.toString()
      });
    }   
  }
}
//xử lý thêm sp mới
export const addNewProductAction = (productInfos) => {
  return async (dispatch) => {
    var promotion = "";
    var price = "";
    //neu gia ban nhap vao lon hon promo thi doi nguoc vi tri
    if(parseFloat(productInfos["promotionPrice"])>0 && parseFloat(productInfos["buyPrice"])>parseFloat(productInfos["promotionPrice"])){
      price = productInfos["promotionPrice"];
      promotion = productInfos["buyPrice"];
    }else{
      price = productInfos["buyPrice"];
      promotion = productInfos["promotionPrice"];
    }
    const bodyJson = {
      reqName:productInfos.name,
      reqDescription:productInfos.description,
      reqType: productInfos.type,
      reqImageUrl: productInfos.imageUrl,
      reqBuyPrice: price,
      reqPromotionPrice: promotion, 
      reqAmount: productInfos.amount,
      reqOrigin: productInfos.origin, 
      reqWeight: productInfos.weight, 
    }
      try {
            var requestOptions = {
              method: 'POST', // phương thức 'POST'
              headers: {
                'Content-Type': 'application/json', // Xác định kiểu dữ liệu của payload là JSON
                'x-access-token': accessToken
              },
              redirect: 'follow',
              body: JSON.stringify(bodyJson)
          };
  
          await dispatch({
              type: ADD_NEW_PRODUCT_PENDING
          })

          const createNewProduct = await fetch("http://localhost:8080/products", requestOptions);
          const data = await createNewProduct.json();
          return dispatch({
              type: ADD_NEW_PRODUCT_SUCCESS,
              data: data.data,
              message: data.message
          })
      } catch (error) {
          return dispatch({
              type: ADD_NEW_PRODUCT_ERROR,
              error: error
          })
      }   
  }
}
//reset form sau khi tạo mới
export const resetAfterCreateAction = () => {
  return(
    {
      type: RESET_AFTER_CREATE
    }
  )
}
//lấy sp theo id
export const fetchProductDetailAction = (productId) => {
  return async (dispatch) => {
    try {
      var requestOptions = {
          method: 'GET', 
          redirect: 'follow',
      };

      await dispatch({
          type: FETCH_PRODUCT_DETAIL_PENDING
      })

      const productDetail = await fetch("http://localhost:8080/products/"+productId, requestOptions);
      const data = await productDetail.json();
      return dispatch({
          type: FETCH_PRODUCT_DETAIL_SUCCESS,
          data: data.data
      })
    } catch (error) {
      return dispatch({
          type: FETCH_PRODUCT_DETAIL_ERROR,
          error: error
      })
    }   
  }
}
//chỉnh sửa chi tiết sp
export const editProductDetailAction = (productId, editInfos) => {
  return async (dispatch) => {
    try {
      var requestOptions = {
        method: 'PUT',
        body: JSON.stringify(editInfos),
        headers: {
            'Content-type': 'application/json; charset=UTF-8', //phải thêm contentYype để edit
            'x-access-token': accessToken
        },
      };

      await dispatch({
          type: EDIT_NEW_PRODUCT_PENDING
      })

      const productDetailEdit = await fetch("http://localhost:8080/products/"+productId, requestOptions);
      const data = await productDetailEdit.json();
      return dispatch({
          type: EDIT_NEW_PRODUCT_SUCCESS,
          data: data
      })
    } catch (error) {
      return dispatch({
          type: EDIT_NEW_PRODUCT_ERROR,
          error: error
      })
    }   
  }
}
//reset form sau khi edit
export const resetEditFormAction = () => {
  return(
    {
      type: RESET_EDIT_FORM
    }
  )
}
//reset state của message thông báo sau khi tạo thành công
export const resetEditMessageAction = () => {
  return(
    {
      type: RESET_EDIT_MESSAGE
    }
  )
}
//xử lý upload ảnh sp
export const uploadImageAction = (formDataImg ) => {
  return async (dispatch) => {
    try {
      var requestOptions = {
        method: 'POST',
        body: formDataImg
      };

      await dispatch({
          type: UPLOAD_IMAGE_PENDING
      })

      const uploadImage = await fetch("http://localhost:8080/upload/", requestOptions);
      const data = await uploadImage.json();
      return dispatch({
          type: UPLOAD_IMAGE_SUCCESS,
          data: data
      })
    } catch (error) {
      return dispatch({
          type: UPLOAD_IMAGE_ERROR,
          error: "Error uploading the file " + error
      })
    }   
  }
}
//xử lý reset lại giá trị của state chứa thông tin sp chi tiết khi click vào edit
export const resetProductInOrderDetail = () => {
  return({
    type: RESET_PRODUCT_IN_ORDER_DETAIL,
    data: ""
  })
}
//lấy các sp trong orderDetail để xem chi tiết đặt hàng
export const checkProductInOrderDetailAction = (productId) =>{
  return async (dispatch) => {
    try {
      var requestOptions = {
        method: 'GET', 
        redirect: 'follow',
      };

      await dispatch({
          type: CHECK_PRODUCT_IN_ORDER_DETAIL_PENDING
      })

      const productsInOrderDetail = await fetch("http://localhost:8080/orderDetails/product/"+productId, requestOptions);
      const data = await productsInOrderDetail.json();
      return dispatch({
          type: CHECK_PRODUCT_IN_ORDER_DETAIL_SUCCESS,
          data: data.data
      })
    } catch (error) {
      return dispatch({
          type: CHECK_PRODUCT_IN_ORDER_DETAIL_ERROR,
          error: error
      })
    }   
  }
}
//xử lý xóa mềm sp
export const softDeleteAction = (productId) =>{
  return async (dispatch) => {
    try {
      var requestOptions = {
        method: 'DELETE', 
        headers: {
          'x-access-token': accessToken
        },
        redirect: 'follow',
      };

      await dispatch({
          type: SOFT_DELETE_PENDING
      })

      const softDelProduct = await fetch("http://localhost:8080/products/softDelete/"+productId, requestOptions);
      const data = await softDelProduct.json();
      return dispatch({
          type: SOFT_DELETE_SUCCESS,
          data: data.message
      })
    } catch (error) {
      return dispatch({
          type: SOFT_DELETE_ERROR,
          error: error
      })
    }   
  }
}
//lấy danh sách những product đã bị xóa mềm
export const fetchProductsDeletedAction = () => {
  return async (dispatch) => {
    try {
      var requestOptions = {
        method: 'GET', 
        redirect: 'follow',
        headers: {
          'x-access-token': accessToken
        },
      };

      await dispatch({
          type: FETCH_PRODUCT_DELETED_PENDING
      })

      const productDeletedResponse = await fetch("http://localhost:8080/products/deleted/", requestOptions);
      const data = await productDeletedResponse.json();
      return dispatch({
          type: FETCH_PRODUCT_DELETED_SUCCESS,
          data: data.data
      })
    } catch (error) {
      return dispatch({
          type: FETCH_PRODUCT_DELETED_ERROR,
          error: error
      })
    }   
  }
}
//reset message sau khi xóa thành công
export const resetDeleteAction = ()=>{
  return(
    {
      type: RESET_DELETE,
    }
  )
}
//xử lý restore product đã xóa mềm
export const restoreProductDeletedAction = (productId) =>{
  return async (dispatch) => {
    try {
      var requestOptions = {
        method: 'PUT', 
        headers: {
          'x-access-token': accessToken
        },
        redirect: 'follow',
      };

      await dispatch({
          type: RESTORE_PRODUCT_DELETED_PENDING
      })

      const productRestored = await fetch("http://localhost:8080/products/restore/"+productId, requestOptions);
      const data = await productRestored.json();
      return dispatch({
          type: RESTORE_PRODUCT_DELETED_SUCCESS,
          data: data
      })
    } catch (error) {
      return dispatch({
          type: RESTORE_PRODUCT_DELETED_ERROR,
          error: error
      })
    }   
  }
}
//xóa state message sau khi thực hiện thông báo xóa mềm
export const resetRestoreAction = ()=>{
  return(
    {
      type: RESET_RESTORE,
    }
  )
}
//xử lý xóa cứng (hiện chưa dùng đến, chỉ dùng xóa mềm)
export const hardDeleteAction = (productId) =>{
  return async (dispatch) => {
    try {
      var requestOptions = {
        method: 'DELETE', 
        headers: {
          'x-access-token': accessToken
        },
        redirect: 'follow',
      };

      await dispatch({
          type: HARD_DELETE_PENDING
      })

      const hardDelProduct = await fetch("http://localhost:8080/products/"+productId, requestOptions);
      const data = await hardDelProduct.json();
      return dispatch({
          type: HARD_DELETE_SUCCESS,
          data: data
      })
    } catch (error) {
      return dispatch({
          type: HARD_DELETE_ERROR,
          error: error
      })
    }   
  }
}
//lấy những sp bán chạy đổ ra dashboard
export const fetchFeatureProductAction = () => {
  return async (dispatch) => {
    try {
      var requestOptions = {
          method: 'GET', 
          redirect: 'follow',
      };

      await dispatch({
          type: FETCH_FEATURE_PRODUCT_PENDING
      })

      const productDetail = await fetch("http://localhost:8080/products/featured", requestOptions);
      const data = await productDetail.json();
      return dispatch({
          type: FETCH_FEATURE_PRODUCT_SUCCESS,
          data: data.data
      })
    } catch (error) {
      return dispatch({
          type: FETCH_FEATURE_PRODUCT_ERROR,
          error: error
      })
    }   
  }
}