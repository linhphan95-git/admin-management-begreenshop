import 'bootstrap/dist/css/bootstrap.min.css'
import React, { Suspense, useEffect } from 'react'
import { BrowserRouter, Navigate, Route, Routes } from 'react-router-dom'
import { useSelector } from 'react-redux'

import { CSpinner, useColorModes } from '@coreui/react'
import './scss/style.scss'

// Containers
const DefaultLayout = React.lazy(() => import('./layout/DefaultLayout'))

// Pages
const Login = React.lazy(() => import('./views/pages/login/Login'))
const Register = React.lazy(() => import('./views/pages/register/Register'))
const Page404 = React.lazy(() => import('./views/pages/page404/Page404'))
const Page403 = React.lazy(() => import('./views/pages/page403/Page403'))
const Page500 = React.lazy(() => import('./views/pages/page500/Page500'))

// Higher-Order Component for auth
import withAuth from './components/withAuth'

const App = () => {

  const { isColorModeSet, setColorMode } = useColorModes('coreui-free-react-admin-template-theme')
  const storedTheme = useSelector((state) => state.theme)

  useEffect(() => {
    const urlParams = new URLSearchParams(window.location.href.split('?')[1])
    const theme = urlParams.get('theme') && urlParams.get('theme').match(/^[A-Za-z0-9\s]+/)[0]
    if (theme) {
      setColorMode(theme)
    }

    if (isColorModeSet()) {
      return
    }

    setColorMode(storedTheme)
  }, []) // eslint-disable-line react-hooks/exhaustive-deps

  //thiết lập role cho toàn bộ các trang trong admin dùng hoc withAuth (nếu role user ko vào được)
  const AdminLayout = withAuth(DefaultLayout, ['admin', 'moderator'])
  const userInfos = JSON.parse(localStorage.getItem('adminLoginInfo')) || [] // Lấy mảng roles từ localStorage và parse nó thành object
  return (
    <BrowserRouter>
      <Suspense
        fallback={
          <div className="pt-3 text-center">
            <CSpinner color="primary" variant="grow" />
          </div>
        }
      >
        <Routes>
          {/* nếu đã login thì đi đến trang login sẽ chuyển hướng về trang chủ */}
          <Route
            exact
            path="/login"
            name="Login Page"
            element={userInfos.length>0 ? <Navigate to="/" replace /> : <Login />}
          />
          {/* <Route exact path="/login" name="Login Page" element={<Login />} /> */}
          <Route exact path="/register" name="Register Page" element={<Register />} />
          <Route exact path="/404" name="Page 404" element={<Page404 />} />
          <Route exact path="/403" name="Page 403" element={<Page403 />} />
          <Route exact path="/500" name="Page 500" element={<Page500 />} />
          {/* nếu ko có 2 role như khi báo ở Admin Layout thì ko truy cập đc */}
          <Route path="*" name="Home" element={<AdminLayout />} />
        </Routes>
      </Suspense>
    </BrowserRouter>
  )
}

export default App
