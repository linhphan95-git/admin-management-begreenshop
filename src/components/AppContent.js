import React, { Suspense } from 'react'
import { Navigate, Route, Routes } from 'react-router-dom'
import { CContainer, CSpinner } from '@coreui/react'
import routes from '../routes'
import Page404 from '../views/pages/page404/Page404'
import Page403 from '../views/pages/page403/Page403'
import Login from '../views/pages/login/Login'

// eslint-disable-next-line react/prop-types
const AppContent = ({ userRoles }) => {
  if (!userRoles) {
    return <div>Loading...</div>; // hoặc bạn có thể trả về một component tạm thời khác hoặc thông báo lỗi
  }

  return (
    <CContainer className="px-4" lg>
      <Suspense fallback={<CSpinner color="primary" />}>
        <Routes>
          {/* kiểm tra bên routes xem cho những role nào vào và so sánh với
            userRoles (roles) của admin lấy từ local
          */}
          {routes.map((route, idx) => {
            //Phân quyền theo từng route riêng và role riêng 
            // eslint-disable-next-line react/prop-types
            if (!route.roles || route.roles.some(role => userRoles.includes(role))) {
              return (
                route.element && (
                  <Route
                    key={idx}
                    path={route.path}
                    exact={route.exact}
                    name={route.name}
                    element={<route.element />}
                  />
                )
              )
            }else {
              // Nếu không có quyền truy cập, chuyển hướng đến trang 403
              return <Route key={idx} path={route.path} element={<Navigate to="/403" replace />} />;
            }
          })}
           <Route path="/" element={<Navigate to="/dashboard" replace />} />
           {/* nếu vào những trang ko có roles phù hợp thì nó sẽ hiện 403 */}
           {/* <Route path="/403" element={<Navigate to="/403" replace />} /> */}
           <Route path="*" element={<Navigate to="/404" replace />} />

        </Routes>
      </Suspense>
    </CContainer>
  )
}

export default React.memo(AppContent)


