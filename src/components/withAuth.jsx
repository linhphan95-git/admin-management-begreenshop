import React from 'react'
import { Navigate } from 'react-router-dom'

const withAuth = (Component, allowedRoles) => {
  // eslint-disable-next-line react/display-name
  return (props) => {
    // nếu roles !== admin thì ko vào được các trang admin
    const userInfos = JSON.parse(localStorage.getItem('adminLoginInfo'))||[]; // Lấy mảng roles từ localStorage và parse nó thành object
    if (userInfos.length === 0 ) {
      // Nếu chua login hoặc không chuyển hướng đến trang login
      return <Navigate to="/login" />
    }else if ( !userInfos[0].roles.some(role => allowedRoles.includes(role.name))){
      //nếu login rôi nhưng mà role ko hợp lệ thì di chuyển đến 403
      return <Navigate to="/403" />
    }
    
    return <Component {...props} />
  }
}

export default withAuth
