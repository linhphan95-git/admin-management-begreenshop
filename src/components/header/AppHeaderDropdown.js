import React, { useEffect, useState } from 'react'
import {
  CAvatar,
  CBadge,
  CDropdown,
  CDropdownDivider,
  CDropdownHeader,
  CDropdownItem,
  CDropdownMenu,
  CDropdownToggle,
} from '@coreui/react'
import {
  cilBell,
  cilCreditCard,
  cilCommentSquare,
  cilEnvelopeOpen,
  cilFile,
  cilLockLocked,
  cilSettings,
  cilTask,
  cilUser,
} from '@coreui/icons'
import CIcon from '@coreui/icons-react'

import avatar8 from './../../assets/images/avatars/8.jpg'
import { useDispatch, useSelector } from 'react-redux'
import { fetchUserDetailAction, logoutAction } from '../../actions/user.actions'
import { Navigate, useNavigate } from 'react-router-dom'
import { ToastContainer, toast } from 'react-toastify'

const AppHeaderDropdown = () => {
  const { userInfos, user, fetchUserMessage, pendingUser } = useSelector((reduxData) => reduxData.userReducers);
  const navigate = useNavigate();
  const [redirectToLogin, setRedirectToLogin] = useState(false);
  const dispatch = useDispatch();
  //hàm xử lý khi click vào logout
  const handleLogout = () => {
    toast.info("Logout processing...");
    
    setTimeout(() => {
      // Thực hiện logout
      dispatch(logoutAction());
      // Redirect về trang login
      setRedirectToLogin(true);
    }, 3000); // Trì hoãn 2 giây
  };
  //lấy thông tin user bằng username
  useEffect(() => {
    if (userInfos && userInfos.length > 0) {
      dispatch(fetchUserDetailAction(userInfos[0].username));
    }
  }, [dispatch, userInfos]);
  //nếu token hết hạn thì login lại
  useEffect(() => {
    if (!pendingUser && fetchUserMessage === "TokenExpiredError") {
      // toast.error("Token has expired");
      dispatch(logoutAction());
      setRedirectToLogin(true);
    }
  }, [dispatch, pendingUser, fetchUserMessage]);

  //hàm xử lý redirect về trang login
  useEffect(() => {
    if (redirectToLogin) {
      navigate('/login', { replace: true });
      window.location.reload();
    }
  }, [redirectToLogin, navigate]);

  if (!user || user.length === 0) {
    return null; // or some fallback UI
  }
  return (
    <CDropdown variant="nav-item">
      <ToastContainer/>
      {user ?
        <>
          {/* <CDropdownToggle placement="bottom-end" className="py-0 pe-0" caret={false}> */}
            {/* <CAvatar src={user[0]?.avatar ? "http://localhost:8080/images/"+ user[0].avatar : "https://www.pngall.com/wp-content/uploads/5/Profile-PNG-File.png"} size="md" /> */}
            <a style={{cursor:"pointer"}} onClick={handleLogout} ><CIcon icon={cilSettings} style={{marginTop:"10px"}} className="me-2" /> LogOut</a>
            
          {/* </CDropdownToggle> */}
          {/* <CDropdownMenu className="pt-0" placement="bottom-end">
            <CDropdownHeader className="bg-body-secondary fw-semibold mb-2">Account</CDropdownHeader>
              <CDropdownItem href="/profile">
                <CIcon icon={cilUser} className="me-2" />
                  Profile
              </CDropdownItem>
              <CDropdownItem style={{cursor:"pointer"}} onClick={handleLogout} >
                <CIcon icon={cilSettings} className="me-2" />
                  LogOut
              </CDropdownItem>
          </CDropdownMenu> */}
        </>
      
        :
        null
        }
      
    </CDropdown>
  )
}

export default AppHeaderDropdown
