export const inputLabels =
  {
    name:"Product Name",
    buyPrice:"Product Price",
    promotionPrice:"Product Promotion",
    description:"Product Description",
    amount:"Product Stock",
    weight:"Product Weight (kg) For One Times Sale",
    imageUrl:"Product Image",
    origin:"Product Origin",
    type: "Product Type"
  }
export const feedbackInvalids =
  {
    name:"Product name is required",
    buyPrice:"Price must be a infinity number and greater than 0",
    // discountPercent:"Discount Percent must be a number from 0 to 100", //[1-9] đảm bảo rằng giá trị bắt đầu bằng một số khác 0. [0-9]* cho phép nhập bất kỳ số nào từ 0 đến 9 sau đó (không giới hạn số lần lặp lại).
    promotionPrice:"Product promotion is required",
    description:"Description is required and minimum 500 words",
    amount:"Stock amount must be a infinity number and greater than 0",
    weight:"Weight must greater than 0 and can be a decimal number. ex: 0.1, 5,...", //số lớn hơn hoặc bằng 0 và có thể là số thập phân
    imageUrl:"Required 3 images",
    origin:"origin is required",
    type:"Product Type is required"
  }
  export const patternProducts =
  {
    name:"",
    buyPrice: String.raw`^(?!0*(\.0+)?$)(\d+(\.\d+)?|(\.\d+))$`, //số lớn hơn 0 và có thể là số thập phân
    promotionPrice: String.raw`^\d+(\.\d+)?|\.\d+$`, //số lớn hơn hoặc bằng 0 và có thể là số thập phân
    // discountPercent:"^100$|^([1-9]?[0-9])$", //số từ 0 đến 100
    description:"",
    amount:"[0-9][0-9]*", // số nguyên từ 0
    weight:String.raw`^(?!0*(\.0+)?$)(\d+(\.\d+)?|(\.\d+))$`, //số lớn hơn 0 và có thể là số thập phân
    imageUrl:"",
    origin:"",
    type:""
  }
  export const inputGroupTexts =
  {
    name:"",
    buyPrice:"$", 
    promotionPrice:"$", 
    // discountPercent:"%", 
    amount:"", 
    weight:"kg", 
    imageUrl:"",
    origin:"",
    type:""
  }
  export const selectStatusOrder = [
    {
      value:"ordered",
      title: "Ordered", 
    },
    {
      value:"preparing",
      title: "Seller is preparing order", 
    },
    {
      value:"delivering",
      title: "Order is being delivered", 
    },
    {
      value:"received",
      title: "Customer is received the order", 
    },
    {
      value:"canceled",
      title: "Order canceled", 
    },
  ]

  export const userRoles = [
    {
      value:"66287181084ab470365007df",
      title:"User"
    },
    {
      value:"66287181084ab470365007e4",
      title:"Admin"
    },
    {
      value:"66287181084ab470365007e7",
      title:"Moderator"
    },
  ]