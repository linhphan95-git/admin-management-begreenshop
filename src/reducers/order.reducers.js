import { 
        FETCH_ORDER_DETAIL_BY_ID_PENDING,
        FETCH_ORDER_DETAIL_BY_ID_SUCCESS,
        FETCH_ORDER_DETAIL_BY_ID_ERROR,
        FETCH_ORDERS_PENDING,
        FETCH_ORDERS_SUCCESS,
        FETCH_ORDER_BY_ID_PENDING,
        FETCH_ORDER_BY_ID_SUCCESS,
        FETCH_ORDER_BY_ID_ERROR,
        ON_CHANGE_STATUS,
        RESET_STATUS,
        EDIT_CHANGE_STATUS_PENDING,
        EDIT_CHANGE_STATUS_SUCCESS,
        EDIT_CHANGE_STATUS_ERROR,
        DELETE_ORDER_PENDING,
        DELETE_ORDER_SUCCESS,
        DELETE_ORDER_ERROR,
        FETCH_ORDERS_IN_DAY_PENDING,
        FETCH_ORDERS_IN_DAY_SUCCESS,
        FETCH_ORDERS_IN_DAY_ERROR,
        SOFT_DELETE_ORDER_ERROR,
        SOFT_DELETE_ORDER_SUCCESS,
        SOFT_DELETE_ORDER_PENDING,
        RESTORE_ORDER_PENDING,
        RESTORE_ORDER_SUCCESS,
        RESTORE_ORDER_ERROR,
        FETCH_ORDERS_DELETED_PENDING,
        FETCH_ORDERS_DELETED_SUCCESS,
        FETCH_ORDERS_DELETED_ERROR,
        RESET_RESTORE,
        RESET_STATUS_MESSAGE,
        ON_CHANGE_PAGINATION_ORDER,
        ON_CHANGE_SEARCH_ORDER_INFO,
        ON_CHANGE_ORDER_DATE,
        ON_CHANGE_ORDER_SHIPPED,
        ON_CHANGE_ORDERED_STATUS,
        ON_CHANGE_PREPARING_STATUS,
        ON_CHANGE_DELIVERED_STATUS,
        ON_CHANGE_RECEIVED_STATUS,
        ON_CHANGE_CANCELED_STATUS,
        CHANGE_TO_FIRST_PAGE,
        SEND_EMAIL_PENDING,
        SEND_EMAIL_SUCCESS,
        SEND_EMAIL_ERROR,
        FETCH_EXPORT_ORDER_PENDING,
        FETCH_EXPORT_ORDER_ERROR,
        FETCH_EXPORT_ORDER_SUCCESS
   } from "../constants/order.constants.js";

const initialState = {
  orders: [],
  pendingFetchOrders: false,
  pendingFetchOrderDetail: false,
  orderDetail: [],
  order:[],
  pendingFetchOrder: false,
  status:"",
  pendingEditStatus: false,
  editStatusMessage:"",
  pendingOrderDeleted : false,
  deleteStatusMessage:"",
  ordersLatest:[],
  pendingOrderLatest: false,
  pendingOrderSoftDeleted : false,
  softDeleteStatusMessage :"",
  pendingRestoreDeletedOrder : false,
  ordersRestoredMessage :"",
  ordersDeleted : [],
  //filter table
  currentPage: 1,
  limit: 10,
  totalPage: 0,
  searchContent: "",
  orderDate:null,
  shippedDate:null,
  ordered: false,
  preparing: false,
  delivered: false,
  received: false,
  canceled: false,
  //export excel file
  loading: false,
  error: null,
  sendEmailPending: false,
  sendEmailMessage:""
}

const orderReducers = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_ORDERS_PENDING:
      state.pendingFetchOrders = true;
      break;
    case FETCH_ORDERS_SUCCESS:
        state.pendingFetchOrders = false;
        state.totalPage = Math.ceil(action.totalOrders / state.limit);
        state.orders = action.orders;
        break;
    // case FETCH_ORDERS_ERROR:
    //     break;
    case FETCH_ORDER_BY_ID_PENDING:
      state.pendingFetchOrder = true;
      break;
    case FETCH_ORDER_BY_ID_SUCCESS:
      state.pendingFetchOrder = false;
      state.order = action.data;
      break;
    case FETCH_ORDER_BY_ID_ERROR:
      break;
    case FETCH_ORDER_DETAIL_BY_ID_PENDING:
      state.pendingFetchOrderDetail = true;
      break;
    case FETCH_ORDER_DETAIL_BY_ID_SUCCESS:
      state.pendingFetchOrderDetail = false;
      state.orderDetail = action.data;
      break;
    case FETCH_ORDER_DETAIL_BY_ID_ERROR:
      break;
    case ON_CHANGE_STATUS:
      state.status = action.data;
      break;
      //reset old status when open other edit order Status
    case RESET_STATUS:
      state.status = action.data;
      break;
    case EDIT_CHANGE_STATUS_PENDING:
      state.pendingEditStatus = true;
      break;
    case EDIT_CHANGE_STATUS_SUCCESS:
      state.pendingEditStatus = false;
      state.editStatusMessage = action.message;
      break;
    case EDIT_CHANGE_STATUS_ERROR:
      break;
      //hard delete đã ko sử dụng nữa
    case DELETE_ORDER_PENDING:
      state.pendingOrderDeleted = true;
      break;
    case DELETE_ORDER_SUCCESS:
      state.pendingOrderDeleted = false;
      state.deleteStatusMessage = action.message;
      break;
    case DELETE_ORDER_ERROR:
      break;
    case FETCH_ORDERS_IN_DAY_PENDING:
      state.pendingOrderLatest= true;
      break;
    case FETCH_ORDERS_IN_DAY_SUCCESS:
      state.pendingOrderLatest = false;
      state.ordersLatest = action.data;
      break;
    case FETCH_ORDERS_IN_DAY_ERROR:
      break;
    case SOFT_DELETE_ORDER_PENDING:
      state.pendingOrderSoftDeleted = true;
      break;
    case SOFT_DELETE_ORDER_SUCCESS:
      state.pendingOrderSoftDeleted = false;
      state.softDeleteStatusMessage = action.data.message;
      break;
    case SOFT_DELETE_ORDER_ERROR:
      break;
    case RESET_STATUS_MESSAGE:
      state.softDeleteStatusMessage = "";
      state.editStatusMessage = "";
      break
    case RESTORE_ORDER_PENDING:
      state.pendingRestoreDeletedOrder = true;
      break;
    case RESTORE_ORDER_SUCCESS:
      state.pendingRestoreDeletedOrder = false;
      state.ordersRestoredMessage = action.data.message;
      break;
    case RESTORE_ORDER_ERROR:
      break;
    case RESET_RESTORE:
      state.ordersRestoredMessage = "";
      break;
    case FETCH_ORDERS_DELETED_PENDING:
      state.pendingOrderDeleted= true;
      break;
    case FETCH_ORDERS_DELETED_SUCCESS:
      state.pendingOrderDeleted = false;
      state.ordersDeleted = action.data;
      break;
    case FETCH_ORDERS_DELETED_ERROR:
      break;
    case ON_CHANGE_PAGINATION_ORDER:
      state.currentPage = action.payload;
      break;
    case ON_CHANGE_SEARCH_ORDER_INFO:
      state.searchContent = action.payload;
      break;
    case ON_CHANGE_ORDER_DATE:
      state.orderDate = action.payload;
      break;
    case ON_CHANGE_ORDER_SHIPPED:
      state.shippedDate = action.payload;
      break;
    case ON_CHANGE_ORDERED_STATUS:
      state.ordered = action.payload;
      break;
    case ON_CHANGE_PREPARING_STATUS:
      state.preparing = action.payload;
      break;
    case ON_CHANGE_DELIVERED_STATUS:
      state.delivered = action.payload;
      break;
    case ON_CHANGE_RECEIVED_STATUS:
      state.received = action.payload;
      break;
    case ON_CHANGE_CANCELED_STATUS:
      state.canceled = action.payload;
      break;
    case CHANGE_TO_FIRST_PAGE:
      state.currentPage = 1;
      break;
    case FETCH_EXPORT_ORDER_PENDING:
      return {
        ...state,
        loading: true,
        error: null
      };
    case FETCH_EXPORT_ORDER_SUCCESS:
      return {
        ...state,
        loading: false,
        error: null
      };
    case FETCH_EXPORT_ORDER_ERROR:
      return {
        ...state,
        loading: false,
        error: action.error
      };
    case SEND_EMAIL_PENDING:
      state.sendEmailPending = true;
      break;
    case SEND_EMAIL_SUCCESS:
      state.sendEmailPending = false;
      state.sendEmailMessage = action.data;
      break;
    case SEND_EMAIL_ERROR:
      break;
    default:
      break
    
  }

  return { ...state }
}

export default orderReducers;
