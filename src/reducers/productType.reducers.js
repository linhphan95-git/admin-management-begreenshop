import { ADD_PRODUCT_TYPE_ERROR, ADD_PRODUCT_TYPE_PENDING,
   ADD_PRODUCT_TYPE_SUCCESS, FETCH_PRODUCTS_TYPE_ERROR, 
   FETCH_PRODUCTS_TYPE_PENDING, FETCH_PRODUCTS_TYPE_SUCCESS, 
   ON_CHANGE_PRODUCT_TYPE_ADD, RESET_TYPE_PRODUCT_FORM, 
   UPLOAD_IMAGE_TYPE_ERROR, UPLOAD_IMAGE_TYPE_PENDING, 
   UPLOAD_IMAGE_TYPE_SUCCESS, RESET_TYPE_PRODUCT_MESSAGE, 
   FETCH_TYPE_DETAIL_PENDING,
   FETCH_TYPE_DETAIL_SUCCESS,
   ON_CHANGE_PRODUCT_TYPE_EDIT,
   RESET_TYPE_PRODUCT_EDIT_FORM,
   EDIT_PRODUCT_TYPE_PENDING,
   EDIT_PRODUCT_TYPE_SUCCESS,
   EDIT_PRODUCT_TYPE_ERROR,
   ON_CHANGE_SEARCH_TYPE_NAME,
   ON_CHANGE_PAGINATION,
   SOFT_DELETE_PRODUCT_TYPE_ERROR,
   SOFT_DELETE_PRODUCT_TYPE_SUCCESS,
   SOFT_DELETE_PRODUCT_TYPE_PENDING,
   RESTORE_PRODUCT_TYPE_PENDING,
   RESTORE_PRODUCT_TYPE_SUCCESS,
   RESTORE_PRODUCT_TYPE_ERROR,
   FETCH_TYPES_DELETED_PENDING,
   FETCH_TYPES_DELETED_SUCCESS,
   FETCH_TYPES_DELETED_ERROR} from "../constants/productType.constant";
const initialState = {
  limit: 3,
  currentPage: 1,
  totalPage: 0,
  pendingType:false,
  types : [],
  productTypeInfos: {
    name:"",
    imageUrl:"",
    imageUrlBackground:""
  },
  pendingUpload : false,
  imagesUploaded:"",
  pendingAddType: false,
  newProductType: {},
  newProductTypeMessage:"",
  pendingTypeDetail : false,
  typeDetail : "",
  typeEditInfos:{},
  pendingEditType :false,
  editProductTypeMessage:"",
  typeName:"",
  softDeletePending: false,
  softDeleteMessage:"",
  restorePending: false,
  restoreMessage:"",
  typesDeleted:[],
  typeDeletedPending: false
}

const productTypeReducers = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_PRODUCTS_TYPE_PENDING:
      state.pendingType = true;
      break;
    case FETCH_PRODUCTS_TYPE_SUCCESS:
      state.pendingType = false;
      state.types = action.types;
      state.totalPage = Math.ceil(action.totalTypes / state.limit);
     
      break;
    case FETCH_PRODUCTS_TYPE_ERROR:
      break;
    case ON_CHANGE_PAGINATION:
      state.currentPage =  action.payload;
      break;
    case ON_CHANGE_PRODUCT_TYPE_ADD:
      state.productTypeInfos =  action.payload;
      break;
    case UPLOAD_IMAGE_TYPE_PENDING:
      state.pendingUpload = true;
      break;
    case UPLOAD_IMAGE_TYPE_SUCCESS:
      state.pendingUpload = false;
      state.imagesUploaded = action.data.data;
      break;
    case UPLOAD_IMAGE_TYPE_ERROR:
      break;
    case ADD_PRODUCT_TYPE_PENDING:
      state.pendingAddType = true;
      break;
    case ADD_PRODUCT_TYPE_SUCCESS:
      state.pendingAddType = false;
      state.newProductType = action.data;
      state.newProductTypeMessage = action.message;
      break;
    case ADD_PRODUCT_TYPE_ERROR:
      break;
    case RESET_TYPE_PRODUCT_FORM:
      return {
        ...state,
        productTypeInfos:{
          name:"",
          imageUrl:"",
          imageUrlBackground:""
        }
      }
    case RESET_TYPE_PRODUCT_MESSAGE:
      return {
        ...state,
        newProductTypeMessage:"",
        editProductTypeMessage:"",
        softDeleteMessage:"",
        restoreMessage:""
      }
    case FETCH_TYPE_DETAIL_PENDING:
      state.pendingTypeDetail = true;
      break;
    case FETCH_TYPE_DETAIL_SUCCESS:
      state.pendingTypeDetail = false;
      state.typeDetail = action.data;
      break;
    case FETCH_TYPE_DETAIL_PENDING:
      break;
    case ON_CHANGE_PRODUCT_TYPE_EDIT:
      state.typeEditInfos =  action.payload;
      break;
    case RESET_TYPE_PRODUCT_EDIT_FORM:
      return {
        ...state,
        typeDetail: ""
      }
    case EDIT_PRODUCT_TYPE_PENDING:
      state.pendingEditType = true;
      break;
    case EDIT_PRODUCT_TYPE_SUCCESS:
      state.pendingEditType = false;
      state.editProductTypeMessage = action.message;
      break;
    case EDIT_PRODUCT_TYPE_ERROR:
      break;
    case ON_CHANGE_SEARCH_TYPE_NAME:
      state.typeName =  action.payload;
      break;
    case SOFT_DELETE_PRODUCT_TYPE_PENDING:
      state.softDeletePending = true;
      break;
    case SOFT_DELETE_PRODUCT_TYPE_SUCCESS:
      state.softDeletePending = false;
      state.softDeleteMessage = action.data.message;
      break;
    case SOFT_DELETE_PRODUCT_TYPE_ERROR:
      break;
    case RESTORE_PRODUCT_TYPE_PENDING:
      state.restorePending = true;
      break;
    case RESTORE_PRODUCT_TYPE_SUCCESS:
      state.restorePending = false;
      state.restoreMessage = action.data.message;
      break;
    case RESTORE_PRODUCT_TYPE_ERROR:
      break;
    case FETCH_TYPES_DELETED_PENDING:
      state.typeDeletedPending = true;
      break;
    case FETCH_TYPES_DELETED_SUCCESS:
      state.typeDeletedPending = false;
      state.typesDeleted = action.data;
      break;
    case FETCH_TYPES_DELETED_ERROR:
      break;
  default:
  break
}
  return { ...state }
}

export default productTypeReducers;