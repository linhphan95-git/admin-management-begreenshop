import { FETCH_PRODUCTS_ERROR,
        FETCH_PRODUCTS_PENDING,
        FETCH_PRODUCTS_SUCCESS,
        CHANGE_PAGINATION, CHANGE_MIN_PRICE, CHANGE_MAX_PRICE,
        SEARCH_PRODUCT_NAME,
        INSTOCK_CHECKED,
        OUTSTOCK_CHECKED,
        ALLSTOCK_CHECKED,
        TYPE_PRODUCT_CHECKED, 
        FETCH_PRODUCT_TYPES_PENDING, FETCH_PRODUCT_TYPES_SUCCESS,
        FETCH_PRODUCT_TYPES_ERROR,
        ON_CHANGE_INPUT_FORM,
        ADD_NEW_PRODUCT_PENDING,
        ADD_NEW_PRODUCT_SUCCESS,
        ADD_NEW_PRODUCT_ERROR,
        RESET_AFTER_CREATE,
        FETCH_PRODUCT_DETAIL_ERROR,
        FETCH_PRODUCT_DETAIL_PENDING,
        FETCH_PRODUCT_DETAIL_SUCCESS,
        ON_CHANGE_INPUT_EDIT_FORM,
        EDIT_NEW_PRODUCT_PENDING,
        EDIT_NEW_PRODUCT_SUCCESS,
        EDIT_NEW_PRODUCT_ERROR,
        RESET_EDIT_FORM,
        UPLOAD_IMAGE_PENDING,
        UPLOAD_IMAGE_SUCCESS,
        UPLOAD_IMAGE_ERROR,
        CHECK_PRODUCT_IN_ORDER_DETAIL_PENDING,
        CHECK_PRODUCT_IN_ORDER_DETAIL_SUCCESS,
        CHECK_PRODUCT_IN_ORDER_DETAIL_ERROR,
        SOFT_DELETE_PENDING,
        SOFT_DELETE_SUCCESS,
        SOFT_DELETE_ERROR,
        HARD_DELETE_PENDING,
        HARD_DELETE_SUCCESS,
        HARD_DELETE_ERROR,
        RESET_PRODUCT_IN_ORDER_DETAIL,
        CHANGE_TO_FIRST_PAGE,
        FETCH_PRODUCT_DELETED_PENDING,
        FETCH_PRODUCT_DELETED_SUCCESS,
        FETCH_PRODUCT_DELETED_ERROR,
        RESTORE_PRODUCT_DELETED_PENDING,
        RESTORE_PRODUCT_DELETED_SUCCESS,
        RESTORE_PRODUCT_DELETED_ERROR,
        RESET_RESTORE,
        RESET_DELETE,
        RESET_EDIT_MESSAGE,
        FETCH_FEATURE_PRODUCT_PENDING,
        FETCH_FEATURE_PRODUCT_SUCCESS,
        FETCH_FEATURE_PRODUCT_ERROR,
        FETCH_EXPORT_PRODUCT_PENDING, FETCH_EXPORT_PRODUCT_SUCCESS, FETCH_EXPORT_PRODUCT_ERROR
   } from "../constants/admin.constants";

const initialState = {
  products: [],
  limitProductsPage: 10,
  pending: false,
  totalPage: 0,
  currentPage: 1,
  //filter
  minPrice:"",
  maxPrice:"",
  productName:"", 
  allStock: true,
  inStock: false,
  outStock: false,
  typeId: [],
  // Types Products
  types: [],
  pendingType: false,
  productInfos: {
    name: "",
    buyPrice: "",
    promotionPrice: "",
    amount: "",
    weight: "",
    imageUrl: "",
    origin:"",
    type:""
  },
  newProduct:"",
  pendingNewProduct:false,
  message: "",
  pendingProductDetail:false,
  productDetail:[],
  productDetailEdit:{
    // name: "",
    // buyPrice: "",
    // discountPercent: 0,
    // amount: "",
    // weight: "",
    // imageUrl: "",
    // origin:"",
    // type:""
  },
  //edit 
  pendingEdit: false,
  productEdited:"",
  editMessage:"",
  pendingUpload : false,
  imagesUploaded : "",
  pendingCheckProductInOrderDetail: false,
  productInOrderDetail:"",
  //Delete
  softDeletePending: false,
  softDeleteMessage: "",
  hardDeletePending: false,
  hardDeleteProduct: "",
  hardDeleteMessage:"",
  pendingFetchProductDeleted : false,
  productsDeleted:[],
  pendingRestore: false,
  restoreMessage: "",
  pendingFeatureProducts: false,
  featureProducts: [],
  //export excel file
  loading: false,
  error: null
}

const adminReducers = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_PRODUCTS_PENDING:
      state.pending = true;
      break;
    case FETCH_PRODUCTS_SUCCESS:
        state.pending = false;
        state.totalPage = Math.ceil(action.totalProducts / state.limitProductsPage);
        state.products = action.productsByPage;
        break;
    case FETCH_PRODUCTS_ERROR:
        break;
    case CHANGE_TO_FIRST_PAGE:
      state.currentPage = 1;
      break;
    case CHANGE_PAGINATION:
      state.currentPage = action.currentPage;
      break;
    case CHANGE_MIN_PRICE:
        state.minPrice = action.payload;
        break;
    case CHANGE_MAX_PRICE:
      state.maxPrice = action.payload;
      break;
    case SEARCH_PRODUCT_NAME:
      state.productName = action.payload;
      break;
    case INSTOCK_CHECKED:
      state.inStock = action.payload;
      break;
    case OUTSTOCK_CHECKED:
      state.outStock = action.payload;
      break;
    case ALLSTOCK_CHECKED:
      state.allStock = action.payload;
    break;
    case ON_CHANGE_INPUT_FORM:
      state.productInfos = action.payload;
    break;
    case TYPE_PRODUCT_CHECKED:
      let updatedTypeIds = [...state.typeId]; // Tạo một bản sao của mảng typeId
      //kiểm tra typeid được truyền vào có trùng với typeid đang có trong mảng state ko
      let findIndexDuplicated = updatedTypeIds.findIndex(type => type === action.payload);
      if (findIndexDuplicated === -1) {
        //nếu ko có thì push type id nầy vào mảng (tương đương với checkbox)
        updatedTypeIds.push(action.payload);
      } else {
        //nếu có thì xóa đi (tương đương với uncheck box)
        updatedTypeIds.splice(findIndexDuplicated, 1);
      }
      return {
        ...state,
        typeId: updatedTypeIds // Trả về một state mới là 1 mảng với typeId đã được checked
      };
    case FETCH_PRODUCT_TYPES_PENDING:
      state.pendingType = true;
      break;
    case FETCH_PRODUCT_TYPES_SUCCESS:
      state.pendingType = false;
      state.types = action.data;
      break;
    case FETCH_PRODUCT_TYPES_ERROR:
      break;
      case ADD_NEW_PRODUCT_PENDING:
        state.pendingNewProduct = true;
        break;
      case ADD_NEW_PRODUCT_SUCCESS:
        state.pendingNewProduct = false;
        state.newProduct = action.data;
        state.message = action.message;
        break;
      case ADD_NEW_PRODUCT_ERROR:
        break;
      case RESET_AFTER_CREATE:
        state.productInfos = {
          name: "",
          buyPrice: "",
          promotionPrice: "",
          description: "",
          amount: "",
          weight: "",
          imageUrl: "",
          origin:"",
          type:""
        };
        state.message = "";
        break;
    case FETCH_PRODUCT_DETAIL_ERROR:
        break;
    case FETCH_PRODUCT_DETAIL_PENDING:
        state.pendingProductDetail = true;
        break;
    case FETCH_PRODUCT_DETAIL_SUCCESS:
      state.pendingProductDetail = false;
      state.productDetail = action.data;
      break;
    case ON_CHANGE_INPUT_EDIT_FORM:
      // console.log(action.type);
      // state.productDetailEdit = action.payload; // Đây chỉ là một dòng comment, nên không ảnh hưởng
      return {
        ...state,
        productDetailEdit: action.payload // Thực hiện cập nhật state một cách đúng đắn
      };
    case EDIT_NEW_PRODUCT_PENDING:
      state.pendingEdit = true;
      break;
    case EDIT_NEW_PRODUCT_SUCCESS:
      state.pendingEdit = false;
      state.productEdited = action.data.data;
      state.editMessage = action.data.message;
      break;
    case EDIT_NEW_PRODUCT_ERROR:
      break;
    case RESET_EDIT_FORM:
      state.productDetailEdit = {};
      state.productDetail = {};
      break;
    case RESET_EDIT_MESSAGE:
      state.editMessage = "";
      state.message = "";
      break;
    case UPLOAD_IMAGE_PENDING:
      state.pendingUpload = true;
      break;
    case UPLOAD_IMAGE_SUCCESS:
      state.pendingUpload = false;
      state.imagesUploaded = action.data.data;
      break;
    case UPLOAD_IMAGE_ERROR:
      break;
    case RESET_PRODUCT_IN_ORDER_DETAIL:
        state.productInOrderDetail = action.data;
      break;
    case CHECK_PRODUCT_IN_ORDER_DETAIL_PENDING:
      state.pendingCheckProductInOrderDetail = true;
      break;
    case CHECK_PRODUCT_IN_ORDER_DETAIL_SUCCESS:
      state.pendingCheckProductInOrderDetail = false;
      state.productInOrderDetail = action.data;
      break;
    case CHECK_PRODUCT_IN_ORDER_DETAIL_ERROR:
      break;
    case SOFT_DELETE_PENDING:
      state.softDeletePending = true;
      break;
    case SOFT_DELETE_SUCCESS:
      state.softDeletePending = false;
      state.softDeleteMessage = action.data;
      break;
    case SOFT_DELETE_ERROR:
      break;
    case HARD_DELETE_PENDING:
      state.hardDeletePending = true;
      break;
    case HARD_DELETE_SUCCESS:
      state.hardDeletePending = false;
      state.hardDeleteProduct = action.data.data;
      state.hardDeleteMessage = action.data.message;
      break;
    case HARD_DELETE_ERROR:
      break;
    case RESET_DELETE:
      state.softDeleteMessage = "";
      state.hardDeleteMessage = "";
      break;
    case FETCH_PRODUCT_DELETED_PENDING:
      state.pendingFetchProductDeleted = true;
      break;
    case FETCH_PRODUCT_DELETED_SUCCESS:
        state.pendingFetchProductDeleted = false;
        state.productsDeleted = action.data;
        break;
    case FETCH_PRODUCT_DELETED_ERROR:
        break;
    case RESTORE_PRODUCT_DELETED_PENDING:
      state.pendingRestore = true;
      break;
    case RESTORE_PRODUCT_DELETED_SUCCESS:
        state.pendingRestore = false;
        state.restoreMessage = action.data.message;
        break;
    case RESTORE_PRODUCT_DELETED_ERROR:
        break;
    case RESET_RESTORE:
        state.restoreMessage = "";
      break;
    case FETCH_FEATURE_PRODUCT_PENDING:
      state.pendingFeatureProducts = true;
      break;
    case FETCH_FEATURE_PRODUCT_SUCCESS:
        state.pendingFeatureProducts = false;
        state.featureProducts = action.data;
        break;
    case FETCH_FEATURE_PRODUCT_ERROR:
        break;
    case FETCH_EXPORT_PRODUCT_PENDING:
      return {
        ...state,
        loading: true,
        error: null
      };
    case FETCH_EXPORT_PRODUCT_SUCCESS:
      return {
        ...state,
        loading: false,
        error: null
      };
    case FETCH_EXPORT_PRODUCT_ERROR:
      return {
        ...state,
        loading: false,
        error: action.error
      };
    default:
      break
  }

  return { ...state }
}

export default adminReducers;
