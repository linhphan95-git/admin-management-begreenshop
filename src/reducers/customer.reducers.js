import { DELETE_CUSTOMERS_ERROR, DELETE_CUSTOMERS_PENDING,
   DELETE_CUSTOMERS_SUCCESS, FETCH_CUSTOMERS_ERROR,
    FETCH_CUSTOMERS_PENDING, FETCH_CUSTOMERS_SUCCESS, 
    FETCH_CUSTOMER_DETAIL_ERROR, FETCH_CUSTOMER_DETAIL_PENDING, 
    FETCH_CUSTOMER_DETAIL_SUCCESS, 
    ON_CHANGE_CUSTOMER_INFO, 
    ON_CHANGE_PAGINATION_CUSTOMER, 
    RESET_CUSTOMER,
    RESET_TO_FIRST_PAGE} from "../constants/customer.constant";

const initialState = {
  limit: 3,
  currentPage: 1,
  totalPage: 0,
  searchContent:"",
  pendingCustomers:false,
  customers : [],
  pendingCustomerDeleted : false,
  deleteStatusCustomerMessage:"",
  pendingCustomerDetail : false,
  customer:[]
}

const customerReducers = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_CUSTOMERS_PENDING:
      state.pendingCustomers = true;
      break;
    case FETCH_CUSTOMERS_SUCCESS:
      state.pendingCustomers = false;
      state.customers = action.customers;
      state.totalPage = Math.ceil(action.totalCustomers / state.limit);
      break;
    case FETCH_CUSTOMERS_ERROR:
      break;
    case DELETE_CUSTOMERS_PENDING:
      state.pendingCustomerDeleted = true;
      break;
    case DELETE_CUSTOMERS_SUCCESS:
      state.pendingCustomerDeleted = false;
      state.deleteStatusCustomerMessage = action.message;
      break;
    case DELETE_CUSTOMERS_ERROR:
      break;
    case FETCH_CUSTOMER_DETAIL_PENDING:
      state.pendingCustomerDetail = true;
      break;
    case FETCH_CUSTOMER_DETAIL_SUCCESS:
      state.pendingCustomerDetail = false;
      state.customer = action.data;
      break;
    case FETCH_CUSTOMER_DETAIL_ERROR:
      break;
    case RESET_CUSTOMER:
      state.deleteStatusCustomerMessage = "";
      break;
    case ON_CHANGE_CUSTOMER_INFO:
      state.searchContent = action.payload;
      break;
    case RESET_TO_FIRST_PAGE:
      state.currentPage = 1;
      break;
    case ON_CHANGE_PAGINATION_CUSTOMER:
      state.currentPage = action.payload;
      break;
  default:
  break
}
  return { ...state }
}

export default customerReducers;
