import { combineReducers } from 'redux';
import adminReducers from './admin.reducers';
import userReducers from './user.reducers';
import orderReducers from './order.reducers';
import customerReducers from './customer.reducers';
import changeState from '../store';
import productTypeReducers from './productType.reducers';
const rootReducer = combineReducers ({
  adminReducers,
  userReducers,
  orderReducers,
  customerReducers,
  changeState,
  productTypeReducers
});
export default rootReducer;