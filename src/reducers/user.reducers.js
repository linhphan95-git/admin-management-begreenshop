import {  CHANGE_LOGIN_VALUE_ADMIN, CHANGE_PASSWORD, CHANGE_USER_INFO_ERROR,
   CHANGE_USER_INFO_PENDING, CHANGE_USER_INFO_SUCCESS, DELETE_USER_ERROR,
    DELETE_USER_PENDING, DELETE_USER_SUCCESS, FETCH_EXPORT_USER_ERROR, FETCH_EXPORT_USER_PENDING, FETCH_EXPORT_USER_SUCCESS, FETCH_USERS_ERROR, 
    FETCH_USERS_PENDING, FETCH_USERS_SUCCESS, FETCH_USER_DELETED_ERROR, 
    FETCH_USER_DELETED_PENDING, FETCH_USER_DELETED_SUCCESS, 
    FETCH_USER_DETAIL_ERROR, FETCH_USER_DETAIL_PENDING, 
    FETCH_USER_DETAIL_SUCCESS, LOGIN_ADMIN_ERROR, LOGIN_ADMIN_PENDING, LOGIN_ADMIN_SUCCESS, LOGOUT, ON_CHANGE_ADMIN_INFO, ON_CHANGE_ADMIN_ROLE, ON_CHANGE_MODERATOR_ROLE, ON_CHANGE_PAGINATION, ON_CHANGE_SEARCH_USER, ON_CHANGE_USER_ROLE, RESET_ADMIN_INFO_EDIT_FORM, RESET_DELETE_USER_MESSAGE, RESET_RESTORE, RESET_USER_EDIT_MESSAGE, RESTORE_USER_DELETED_ERROR, RESTORE_USER_DELETED_PENDING, RESTORE_USER_DELETED_SUCCESS, 
    UPLOAD_ADMIN_INFO_ERROR, 
    UPLOAD_ADMIN_INFO_PENDING, 
    UPLOAD_ADMIN_INFO_SUCCESS, 
    UPLOAD_IMAGE_ADMIN_ERROR, 
    UPLOAD_IMAGE_ADMIN_PENDING,
    UPLOAD_IMAGE_ADMIN_SUCCESS,
    CHANGE_TO_FIRST_PAGE} from "../constants/user.constants";


const initialState = {
  limit: 5,
  currentPage: 1,
  totalPage: 1,
  users: [],
  pendingUsers: false,
  adminProfile: [],
  user: [],
  fetchUserMessage :"",
  pendingUser: false,
  pendingUserInfo: false,
  userInfoMessage: "",
  password:"",
  pendingDeleteUser: false,
  userDeleted:[],
  userDeletedMessage:"",
  pendingFetchDeletedUser : false,
  usersDeleted:[],
  pendingRestoreDeletedUser: false,
  usersRestoredMessage:"",
  loginInfoInput: {
    username:"",
    password:""
  },
  pendingLogin: false,
  resultLoginInfo:[],
  userInfos: JSON.parse(localStorage.getItem('adminLoginInfo')) || [],
  adminInfoEdited:[],
  pendingUpload : false,
  imagesUploaded: "",
  pendingUploadUserInfo: false,
  uploadUserMessage:"",
  //filter table
  searchContent:"",
  isAdmin: false,
  isModerator: false,
  isUser: false,
  //export excel file
  loading: false,
  error: null
}

const userReducers = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_USERS_PENDING:
      state.pendingUsers = true;
      break;
    case FETCH_USERS_SUCCESS:
      state.pendingUsers = false;
      state.users = action.data;
      state.totalPage = Math.ceil(action.totalUsers / state.limit);
      break;
    case FETCH_USERS_ERROR:
      break;
    case FETCH_USER_DETAIL_PENDING:
      state.pendingUser = true;
      break;
    case FETCH_USER_DETAIL_SUCCESS:
      state.pendingUser = false;
      state.user = action.data.data;
      state.fetchUserMessage = action.data.message;
      break;
    case FETCH_USER_DETAIL_ERROR:
      break;
    case CHANGE_USER_INFO_PENDING:
      state.pendingUserInfo = true;
      break;
    case CHANGE_USER_INFO_SUCCESS:
      state.pendingUserInfo = false;
      // state.userInfo = action.data;
      state.userInfoMessage = action.data.message;
      break;
    case CHANGE_USER_INFO_ERROR:
      break;
    case CHANGE_PASSWORD:
      state.password = action.payload;
      break;
    case RESET_USER_EDIT_MESSAGE:
      state.userInfoMessage = "";
      break;
    case DELETE_USER_PENDING:
      state.pendingDeleteUser = true;
      break;
    case DELETE_USER_SUCCESS:
      state.pendingDeleteUser = false;
      state.userDeleted = action.data;
      state.userDeletedMessage = action.data.message;
      break;
    case DELETE_USER_ERROR:
      break;
    case RESET_DELETE_USER_MESSAGE:
      state.userDeletedMessage = "";
      break;
    case FETCH_USER_DELETED_PENDING:
      state.pendingFetchDeletedUser = true;
      break;
    case FETCH_USER_DELETED_SUCCESS:
      state.pendingFetchDeletedUser = false;
      state.usersDeleted = action.data;
      break;
    case FETCH_USER_DELETED_ERROR:
      break;
    case RESTORE_USER_DELETED_PENDING:
      state.pendingRestoreDeletedUser = true;
      break;
    case RESTORE_USER_DELETED_SUCCESS:
      state.pendingRestoreDeletedUser = false;
      state.usersRestoredMessage = action.data.message;
      break;
    case RESTORE_USER_DELETED_ERROR:
      break;
    case RESET_RESTORE:
      state.usersRestoredMessage = "";
      break;
    case CHANGE_LOGIN_VALUE_ADMIN:
      state.loginInfoInput = action.data;
    case LOGIN_ADMIN_PENDING:
      state.pendingLogin = true;
      break;
    case LOGIN_ADMIN_SUCCESS:
      state.pendingLogin = false;
      state.resultLoginInfo = {...action.data}; //nếu thành công gồm access và refresh Token
    if(!action.data.message){
      //nếu login thành công mới lưu vào local storage
      const loginInfoArray = [];
      loginInfoArray.push({...action.data, username: action.loginInfoInput.username})
      localStorage.setItem('adminLoginInfo', JSON.stringify(loginInfoArray));
    }
    break;
    case LOGIN_ADMIN_ERROR:
        break;
    case LOGOUT:
      localStorage.removeItem("adminLoginInfo");
      break;
    case ON_CHANGE_ADMIN_INFO:
      state.adminInfoEdited = action.payload;
      break;
    case UPLOAD_IMAGE_ADMIN_PENDING:
      state.pendingUpload = true;
      break;
    case UPLOAD_IMAGE_ADMIN_SUCCESS:
      state.pendingUpload = false;
      state.imagesUploaded = action.data.data;
      break;
    case UPLOAD_IMAGE_ADMIN_ERROR:
      break;
    case UPLOAD_ADMIN_INFO_PENDING:
      state.pendingUploadUserInfo = true;
      break;
    case UPLOAD_ADMIN_INFO_SUCCESS:
      state.pendingUploadUserInfo = false;
      state.uploadUserMessage = action.data.message;
      break;
    case UPLOAD_ADMIN_INFO_ERROR:
      break;
    case RESET_ADMIN_INFO_EDIT_FORM:
      state.adminInfoEdited = [];
      break;
    case ON_CHANGE_SEARCH_USER:
      state.searchContent = action.payload;
      break;
    case ON_CHANGE_ADMIN_ROLE:
      state.isAdmin = action.payload;
      break;
    case ON_CHANGE_USER_ROLE:
      state.isUser = action.payload;
      break;
    case ON_CHANGE_MODERATOR_ROLE:
      state.isModerator = action.payload;
      break;
    case ON_CHANGE_PAGINATION:
      state.currentPage = action.payload;
      break;
    case CHANGE_TO_FIRST_PAGE:
      state.currentPage = 1;
      break;
    case FETCH_EXPORT_USER_PENDING:
      return {
        ...state,
        loading: true,
        error: null
      };
    case FETCH_EXPORT_USER_SUCCESS:
      return {
        ...state,
        loading: false,
        error: null
      };
    case FETCH_EXPORT_USER_ERROR:
      return {
        ...state,
        loading: false,
        error: action.error
      };
    default:
      break
  }

  return { ...state }
}

export default userReducers;
