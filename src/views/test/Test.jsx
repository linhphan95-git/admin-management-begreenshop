import React, { useState } from "react";

function UploadImage() {
  const [files, setFiles] = useState([]);
  // const [photos, setPhotos] = useState([]);

  function handleChange(e) {
    const selectedFiles = Array.from(e.target.files).slice(0, 3); // Limit to 3 files
    setFiles(selectedFiles);
    const formData = new FormData();
    selectedFiles.forEach(file => {
      formData.append('files', file);
    });

    fetch('http://localhost:8080/upload', {
      method: 'POST',
      body: formData,
    })
      .then(response => response.json())
      .then(data => {
        setPhotos([...data, ...photos]);
      })
      .catch(error => {
        console.error('Error uploading the file:', error);
      });
  }
  console.log(files)
  return (
    <div>
      <div>
        <input type="file" name="file" onChange={handleChange} multiple />
      </div>
      <div>
        {files.map((file, index) => (
          <img
            key={index}
            src={URL.createObjectURL(file)}
            alt={`preview ${index}`}
            style={{ width: "100px", height: "100px", margin: "5px" }}
          />
        ))}
      </div>
      {/* <div>
        {photos.map((photo, index) => (
          <img
            key={index}
            src={`http://localhost:8080/${photo.filename}`}
            alt="Uploaded"
            style={{ width: "100px", height: "100px", margin: "5px" }}
          />
        ))}
      </div> */}
    </div>
  );
}

export default UploadImage;
