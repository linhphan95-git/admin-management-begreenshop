import React, { useState } from "react";

function UploadImage() {
  const [file, setFile] = useState(null);
  const [photos, setPhotos] = useState([]);

  function handleChange(e) {
    const selectedFile = e.target.files[0];
    setFile(URL.createObjectURL(selectedFile));

    const formData = new FormData();
    formData.append('file', selectedFile);

    fetch('http://localhost:8080/upload', {
      method: 'POST',
      body: formData,
    })
      .then(response => response.json())
      .then(data => {
        setPhotos([data, ...photos]);
      })
      .catch(error => {
        console.error('Error uploading the file:', error);
      });
  }

  return (
    <div>
      <div>
        <input type="file" name="file" onChange={handleChange} multiple/>
      </div>
      {photos.map(photo => (
        <img key={photo.filename} style={{width:"100px", height:"100px"}} src={`http://localhost:8080/${photo.filename}`} alt="Uploaded" />
      ))}
    </div>
  );
}

export default UploadImage;
