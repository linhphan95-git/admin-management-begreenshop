import React from 'react'
import { Grid } from "@mui/material"
import {
  CButton,
  CCol,
  CRow,
} from '@coreui/react'
import CustomerTable from './CustomerTable'
import LongMenuFilter from '../customers/LongMenuFilter';
import DownloadIcon from '@mui/icons-material/Download';
import { exportExcelCustomerFile } from '../../actions/customer.actions';
import { useDispatch } from 'react-redux';

const Customer = () => {
  const dispatch = useDispatch();
  const handleOnClickExportExcelButton =() =>{
    dispatch(exportExcelCustomerFile());
  }
  return (
    <>
      <CRow>
        <CCol xs>
          <h1 style={{textAlign:"center", marginBottom:"30px"}}>Customers Management</h1>
          <div style={{display:"flex", marginBottom:"20px", marginTop:"10px"}}>
            {/* filter bar menu */}
            <LongMenuFilter/>
            <CButton 
              onClick={handleOnClickExportExcelButton} 
              color="success" shape="rounded-pill"
              style={{color:"white", marginRight:"10px"}}
            >
              <DownloadIcon/>EXCEL FILE
            </CButton>
          </div>
          {/* table Customers */}
          <Grid item lg={12} md={12} sm={12} xs={12}>
            <CustomerTable></CustomerTable>
          </Grid>
        </CCol>
      </CRow>
      
    </>
  )
}

export default Customer
