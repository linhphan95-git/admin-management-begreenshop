import React, { useEffect } from "react";
import { CButton, CModalTitle, CModalBody, CModalFooter , CModal, CModalHeader } from "@coreui/react"
import { useDispatch, useSelector } from "react-redux";
import { ToastContainer, toast } from 'react-toastify';
import { deleteCustomerAction, fetchCustomersAction, resetCustomerDelete } from "../../../actions/customer.actions";

const ModalDelete= (data) => {
  
  const dispatch = useDispatch();
  const { pendingCustomerDeleted, deleteStatusCustomerMessage
  } = useSelector((reduxData) => reduxData.customerReducers);
  //xử lý xóa customer
  const handleDeleteItemSubmit = async () => {
    // Ensure this check happens after the async operation completes
    dispatch(deleteCustomerAction(data.customerId))
  };
  //thông báo sau khi xóa
  useEffect(() => {
    if(!pendingCustomerDeleted && deleteStatusCustomerMessage == "Customer Deleted Successfully"){
      toast.success("Delete Customer Successfully!",{containerId:"CustomerDelSuccess"});
      data.setVisible(false);
      dispatch(fetchCustomersAction());
      dispatch(resetCustomerDelete())
    }
    if(!pendingCustomerDeleted && deleteStatusCustomerMessage == "Have an error"){
      toast.error("Delete Customer Failed!",{containerId:"CustomerDelFailed"});
      data.setVisible(false);
      dispatch(resetCustomerDelete())
    }
  }, [dispatch,pendingCustomerDeleted,deleteStatusCustomerMessage])

  return (
    <>
      <ToastContainer autoClose={1500} hideProgressBar={true} containerId="CustomerDelSuccess" position="top-center" />
      <ToastContainer autoClose={1500} hideProgressBar={true} containerId="CustomerDelFailed" position="top-right" />

      <CModal
        scrollable
        visible={data.visible}
        onClose={() => data.setVisible(false)}
        aria-labelledby="ScrollingLongContentExampleLabel2"
      >
        <CModalHeader>
          <CModalTitle style={{color:"#2196F3", marginLeft:"35%"}} id="ScrollingLongContentExampleLabel2">Delete Item</CModalTitle>
        </CModalHeader>
        <CModalBody>
          Are you sure deleting this item?
        </CModalBody>
        <CModalFooter>
          <CButton color="secondary" onClick={() => data.setVisible(false)}>
            Close
          </CButton>
          <CButton color="primary" onClick={() => handleDeleteItemSubmit()}>Yes</CButton>
        </CModalFooter>
      </CModal>
    </>
  )
}
export default ModalDelete;