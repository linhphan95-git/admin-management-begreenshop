import React, { useEffect } from 'react'
import { Box, Checkbox, FormControlLabel } from "@mui/material"
import SearchIcon from '@mui/icons-material/Search';
import { useDispatch, useSelector } from 'react-redux';
import { onChangeCustomerInfoAction, resetToFirstPageAction } from '../../actions/customer.actions';

const FilterBarCustomer = () => {
  const dispatch = useDispatch();
  const {searchContent
  } = useSelector((reduxData)=> reduxData.customerReducers);
  //get search info customer
  const handleOnChangeSearchCustomerTable = (event) =>{
    dispatch(resetToFirstPageAction());
    dispatch(onChangeCustomerInfoAction(event.target.value));
  }
  return(
    <>
      {/* by name */}
      <Box style={{position:"relative", margin:"10px 0px"}}>
        <SearchIcon style={{color:"#7AB730", position:"absolute", top:"7px", left:"220px"}}/>
        <input value={searchContent} onChange={handleOnChangeSearchCustomerTable} style={{backgroundColor:"transparent",color:"#7AB730", 
        border:"2px solid #7AB730", borderRadius:"5px", padding:"5px", width:"250px" }} type="text" placeholder='Search Customer Info...' />
      </Box>
    </>
  )
}

export default FilterBarCustomer;