
import React, { useEffect, useState } from 'react';
import { Box, Grid, Pagination, Paper, Table, TableBody, TableCell, TableContainer, TableHead, TablePagination, TableRow, Tooltip } from "@mui/material"
import { useDispatch, useSelector } from 'react-redux';
import { fetchCustomerByIdAction, fetchCustomersAction, onChangePaginationCustomerAction } from '../../actions/customer.actions';

const CustomerTable = () => {
  const dispatch = useDispatch();
  const {customers, customer, limit, currentPage, searchContent, totalPage
  } = useSelector((reduxData) => reduxData.customerReducers);

  // Gọi API để lấy dữ liệu customers đổ ra bảng
  useEffect( function fetchCustomer() {
    dispatch(fetchCustomersAction(limit, currentPage, searchContent));
}, [dispatch, limit, currentPage, searchContent]);  
  // //delete customers
  // const [visibleDeleteModal, setVisibleDeleteModal] = useState(false);
  // const handleDeleteIconClick = (customerId) => {
  //   setVisibleDeleteModal(true);
  //   dispatch(fetchCustomerByIdAction(customerId));
  // }
  const handleChangePaginationCustomer = (event, value) =>{
    dispatch(onChangePaginationCustomerAction(value))
  }
  return(
    <>
      {/* <OrderDetailModal orderDetail={orderDetail} visible={visible} setVisible={setVisible} /> */}
      <TableContainer component={Paper}>
        <Table sx={{ minWidth: 650 }} aria-label="simple table">
            <TableHead>
            <TableRow>
                <TableCell sx={{ fontWeight:"bolder" }}>#</TableCell>
                <TableCell sx={{ fontWeight:"bolder", width:"70px" }}>Customer ID</TableCell>
                <TableCell sx={{ fontWeight:"bolder" }}>Full Name</TableCell>
                <TableCell sx={{ fontWeight:"bolder" }}>Phone</TableCell>
                <TableCell sx={{ fontWeight:"bolder"}}>Email</TableCell>
                <TableCell sx={{ fontWeight:"bolder" }}>Address</TableCell>
                <TableCell sx={{ fontWeight:"bolder" }}>City</TableCell>
                <TableCell sx={{ fontWeight:"bolder" }}>Country</TableCell>
            </TableRow>
            </TableHead>
            <TableBody>
            {
              customers.length > 0 &&
              customers.map((element, index) => {
                return (
                  <TableRow 
                    sx={{
                      '&:last-child td, &:last-child th': { border: 0 },
                      '&:hover': {
                        backgroundColor: '#f5f5f5',
                      },
                      backgroundColor: index % 2 === 0 ? '#f9f9f9' : '#ffffff',
                    }}
                    key={index}>
                      <TableCell>{(currentPage-1)*limit + (index+1)}</TableCell>
                      <TableCell >{element._id}</TableCell>
                      <TableCell>{element.fullName}</TableCell>
                      <TableCell>{element.phone}</TableCell>
                      <TableCell>{element.email} </TableCell>
                      <TableCell>{element.address} </TableCell>
                      <TableCell>{element.city} </TableCell>
                      <TableCell>{element.country} </TableCell>
                  </TableRow>
                ) 
              })
            }
            </TableBody>
        </Table>
      </TableContainer>
      <Grid item lg={12} md={12} sm={12} xs={12} mt={5} mb={2}>
          <Pagination count={totalPage} page={currentPage}
           variant="outlined" color="secondary"
           onChange={handleChangePaginationCustomer}
           />
      </Grid>
    {/* <ModalDelete visible={visibleDeleteModal} setVisible={setVisibleDeleteModal} customerId={customer._id}></ModalDelete> */}
    {/* <ModalEditForm visible={visibleEditModal} setVisible={setVisibleEditModal} order={order}/> */}
    </>
  )
}

export default CustomerTable;