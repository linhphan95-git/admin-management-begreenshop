
import React, { useEffect, useState } from 'react';
import EditIcon from '@mui/icons-material/Edit';
import VisibilityIcon from '@mui/icons-material/Visibility';
import DeleteIcon from '@mui/icons-material/Delete';
import { useDispatch, useSelector } from 'react-redux';
import { fetchOrderByIdAction, fetchOrderDetailByIdAction, 
  fetchOrdersAction, onChangePaginationOrderAction, resetStatusAction, 
  resetStatusMessageAction } from '../../actions/order.actions';
import OrderDetailModal from './modal/OrderDetailModal';
import ModalEditForm from './modal/ModalEditForm';
import ModalDelete from './modal/ModalDelete';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { Grid, Pagination, Paper, Table, TableBody, TableCell, TableContainer, TableHead, TableRow, Tooltip, Typography } from "@mui/material"

const TableOrder = (data) => {
  const dispatch = useDispatch();
  const {orders, order, currentPage, limit, totalPage,
    searchContent, orderDate, shippedDate, ordered, preparing ,
    delivered, received, canceled
   } = useSelector((reduxData) => reduxData.orderReducers);
  useEffect( function fetchUser() {
    // Gọi API để lấy dữ liệu order đổ ra bảng
    dispatch(fetchOrdersAction(limit, currentPage, searchContent, orderDate, shippedDate, 
      ordered, preparing , delivered, received, canceled));
}, [dispatch, limit, currentPage, searchContent, orderDate, shippedDate
    ,ordered, preparing, delivered, received, canceled
  ]);

  // Function to convert ISO date-time to 'ngày tháng năm giờ' format
const formatDateTime = (isoString) => {
  const date = new Date(isoString);
  const options = { 
    day: '2-digit', 
    month: '2-digit', 
    year: 'numeric', 
    hour: '2-digit', 
    minute: '2-digit', 
    second: '2-digit',
    hour12: false // 24-hour format
  };
    return date.toLocaleDateString('vi-VN', options).replace(',', ' lúc');
  };
  //change pagination
  const handleOnChangePaginationOrderTable = (event, value) =>{
     dispatch(onChangePaginationOrderAction(value))
  }
  
  //view detail order
  const [visible, setVisible] = useState(false)
  const [orderNote, setOrderNote] = useState([])
  const handleViewDetail = async (orderDetails, infoArray) => {
    setOrderNote([]);
    setVisible(true);
    setOrderNote(infoArray);
   await dispatch(fetchOrderDetailByIdAction(orderDetails));
  
  }
  //edit status order
  const [visibleEditModal, setVisibleEditModal] = useState(false);
  const handleEditIconClick = (orderId) => {
    setVisibleEditModal(true);
    dispatch(fetchOrderByIdAction(orderId));
    //reset old Message order before edit
    dispatch(resetStatusAction());
  }
  //delete order
  const resultLoginInfo = JSON.parse(localStorage.getItem('adminLoginInfo')) || [] // Lấy mảng roles từ localStorage và parse nó thành object
  const hasAdmin = resultLoginInfo[0]?.roles?.some(role=> role.name ==="admin");

  const [visibleDeleteModal, setVisibleDeleteModal] = useState(false);
  const handleDeleteIconClick = (orderId) => {
    if(hasAdmin){
      setVisibleDeleteModal(true);
      dispatch(fetchOrderByIdAction(orderId));
    }else{
      toast.error("You don't have permission to delete!", {
        position: "top-center",
      });
    }    
     //reset old Message order before delete
     dispatch(resetStatusMessageAction());
  }
  
  return(
    <>
      <ToastContainer/>
      <OrderDetailModal orderNote={orderNote} visible={visible} setVisible={setVisible} />
      <TableContainer component={Paper}>
        <Table sx={{ minWidth: 650 }} aria-label="simple table">
            <TableHead>
            <TableRow>
                <TableCell sx={{ fontWeight:"bolder" }}>#</TableCell>
                <TableCell sx={{ fontWeight:"bolder", width:"70px" }}>Order ID</TableCell>
                <TableCell sx={{ fontWeight:"bolder" }}>Order Date</TableCell>
                <TableCell sx={{ fontWeight:"bolder" }}>Shipped Date</TableCell>
                <TableCell sx={{ fontWeight:"bolder"}}>Customer Email</TableCell>
                <TableCell sx={{ fontWeight:"bolder" }}>Order Details</TableCell>
                <TableCell sx={{ fontWeight:"bolder" }}>Cost</TableCell>
                <TableCell sx={{ fontWeight:"bolder" }}>Status</TableCell>
                <TableCell sx={{ fontWeight:"bolder" }}>Action</TableCell>
            </TableRow>
            </TableHead>
            <TableBody>
            {
              orders.length > 0 &&
              orders.map((element, index) => {
                return (
                  <TableRow 
                    sx={{
                      '&:last-child td, &:last-child th': { border: 0 },
                      '&:hover': {
                        backgroundColor: '#f5f5f5',
                      },
                      backgroundColor: index % 2 === 0 ? '#f9f9f9' : '#ffffff',
                    }}
                    key={index}>
                      <TableCell>{(currentPage-1)*limit + (index+1)}</TableCell>
                      <TableCell >{element._id}</TableCell>
                      <TableCell>{formatDateTime(element.orderDate)}</TableCell>
                      <TableCell>{formatDateTime(element.shippedDate)}</TableCell>
                      <TableCell>{element.customer.email} </TableCell>
                      <TableCell>
                        <Tooltip title="View Detail">
                          <VisibilityIcon
                            onClick={()=> handleViewDetail(element.orderDetails, [element.note, element.addressDelivery])}
                            style={{ color: 'blue', cursor: 'pointer' }}
                          />
                        </Tooltip>
                      </TableCell>
                      <TableCell>
                       {element.cost.toFixed(2)}
                      </TableCell>
                      <TableCell>
                       {element.status}
                      </TableCell>
                      <TableCell>
                      <Tooltip title="Edit">
                        <EditIcon
                          onClick={() => handleEditIconClick(element._id)}
                          style={{ color: 'blue', cursor: 'pointer', fontSize:"20px" }}
                        />
                      </Tooltip>
                      <Tooltip title="Delete">
                        <DeleteIcon 
                          onClick={()=> handleDeleteIconClick(element._id)}
                          style={{color:"red", cursor:"pointer", fontSize:"20px"}}
                        />
                      </Tooltip>
                      </TableCell>
                  </TableRow>
                ) 
              })
            }
            </TableBody>
        </Table>
      </TableContainer>
      <Grid item lg={12} md={12} sm={12} xs={12} mt={5} mb={2}>
          <Pagination count={totalPage} page={currentPage}
           variant="outlined" color="secondary"
           onChange={handleOnChangePaginationOrderTable}
           />
      </Grid>
      <ModalDelete visible={visibleDeleteModal} setVisible={setVisibleDeleteModal} orderId={order._id}></ModalDelete>
      <ModalEditForm visible={visibleEditModal} setVisible={setVisibleEditModal}/>
    </>
  )
}

export default TableOrder;