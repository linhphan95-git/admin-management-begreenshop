import * as React from 'react';
import IconButton from '@mui/material/IconButton';
import Menu from '@mui/material/Menu';
import MoreVertIcon from '@mui/icons-material/MoreVert';
import { useDispatch } from 'react-redux';
import FilterBarOrder from './FilterBarOrder';
import { CButton } from '@coreui/react';
// const ITEM_HEIGHT = 55;

export default function LongMenuFilter() {
  const dispatch = useDispatch();
  const [anchorEl, setAnchorEl] = React.useState(null);
  const open = Boolean(anchorEl);
  // khi click thì mở filter bar
  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };
  return (
    <div>
      <CButton 
        onClick={handleClick} 
        color="primary" shape="rounded-pill"
        style={{color:"white", marginRight:"10px"}}
      >
        <MoreVertIcon />Menu Filter
      </CButton>
      <Menu
        id="long-menu"
        MenuListProps={{
          'aria-labelledby': 'long-button',
        }}
        anchorEl={anchorEl}
        open={open}
        onClose={handleClose}
        PaperProps={{
          style: {
            height: "350px",
            width: '300px',
            paddingLeft:"20px",
            background:"rgb(33 38 49)",
            marginTop:"10px",
            
          },
        }}
      >
        <FilterBarOrder/>
      </Menu>
    </div>
  );
}