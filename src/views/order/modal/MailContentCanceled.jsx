import React from "react";
const MailContentCanceled = (data) =>{
  return(
    <>
      <div className="container">
        <table border={0} cellPadding={0} cellSpacing={0} className="body">
          <tbody>
            <tr>
              <td>&nbsp;</td>
              <td className="container">
                <div className="content">
                  <table className="main">
                    {/* START MAIN CONTENT AREA */}
                    <tbody>
                      <tr>
                        <td className="wrapper">
                          <table border={0} cellPadding={0} cellSpacing={0}>
                            <tbody>
                              <tr>
                                <td>
                                  <h1>Notification For Your Order</h1>
                                  <h2>Your Order: {data.order._id}</h2>
                                  <table
                                    border={0}
                                    cellPadding={0}
                                    cellSpacing={0}
                                    className="btn btn-primary"
                                  >
                                    <tbody>
                                      <tr>
                                        <td align="left">
                                          <table
                                            border={0}
                                            cellPadding={0}
                                            cellSpacing={0}
                                          >
                                            <tbody>
                                              <tr>
                                                <td>
                                                  {" "}
                                                  <p>Dear Mr/Mrs, {data.order.customer.fullName}</p>
                                                </td>
                                              </tr>
                                            </tbody>
                                          </table>
                                        </td>
                                      </tr>
                                    </tbody>
                                  </table>
                                  <p>
                                      We regret to inform you that your order has been canceled.
                                      We apologize for any inconvenience this may have caused. 
                                      If you have any questions or need further assistance, 
                                      please do not hesitate to contact our customer service team at 
                                      <a href="">customer.service@beGreenShop.com</a>.
                                      Thank you for your understanding!
                                  </p>
                                </td>
                              </tr>
                            </tbody>
                          </table>
                        </td>
                      </tr>
                      {/* END MAIN CONTENT AREA */}
                    </tbody>
                  </table>
                  {/* START FOOTER */}
                  <div className="footer">
                    <table border={0} cellPadding={0} cellSpacing={0}>
                      <tbody>
                        <tr>
                          <td className="content-block">
                            <br /> Best regards,
                            BeGreen Customer Service Team
                          </td>
                        </tr>
                        <tr>
                          <td className="content-block powered-by">
                            Powered by <a href="#">BeGreenShop</a>.
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                  {/* END FOOTER */}
                  {/* END CENTERED WHITE CONTAINER */}
                </div>
              </td>
              <td>&nbsp;</td>
            </tr>
          </tbody>
        </table>
      </div>
    </>
  )
}
export default MailContentCanceled;