import React from "react";
const MailContent = (data) =>{
  //format time 
  const formatDateTime = (isoString) => {
    const date = new Date(isoString);
    const options = { 
      day: '2-digit', 
      month: '2-digit', 
      year: 'numeric', 
    };
      return date.toLocaleDateString('vi-VN', options);
    };
  return(
    <>
      <div className="container">
        <table border={0} cellPadding={0} cellSpacing={0} className="body">
          <tbody>
            <tr>
              <td>&nbsp;</td>
              <td className="container">
                <div className="content">
                  <table className="main">
                    {/* START MAIN CONTENT AREA */}
                    <tbody>
                      <tr>
                        <td className="wrapper">
                          <table border={0} cellPadding={0} cellSpacing={0}>
                            <tbody>
                              <tr>
                                <td>
                                  <h1>Notification For Your Order</h1>
                                  <h2>Your Order: {data.order._id}</h2>
                                  <table
                                    border={0}
                                    cellPadding={0}
                                    cellSpacing={0}
                                    className="btn btn-primary"
                                  >
                                    <tbody>
                                      <tr>
                                        <td align="left">
                                          <table
                                            border={0}
                                            cellPadding={0}
                                            cellSpacing={0}
                                          >
                                            <tbody>
                                              <tr>
                                                <td>
                                                  {" "}
                                                  <p>Dear Mr/Mrs, {data.order.customer.fullName}</p>
                                                </td>
                                              </tr>
                                            </tbody>
                                          </table>
                                        </td>
                                      </tr>
                                    </tbody>
                                  </table>
                                  <p>
                                      We are pleased to inform you that your status order is now changed to 
                                      <span style={{color:"red"}}> {data.status}</span>. Your order will be 
                                      shipped at: {formatDateTime(data.order.shippedDate)}. You can track your
                                      order on our website. If you have any questions or need further assistance, 
                                      please do not hesitate to contact our customer service team at 
                                      <a href="">customer.service@beGreenShop.com</a>.
                                      Thank you for shopping with us!
                                  </p>
                                </td>
                              </tr>
                            </tbody>
                          </table>
                        </td>
                      </tr>
                      {/* END MAIN CONTENT AREA */}
                    </tbody>
                  </table>
                  {/* START FOOTER */}
                  <div className="footer">
                    <table border={0} cellPadding={0} cellSpacing={0}>
                      <tbody>
                        <tr>
                          <td className="content-block">
                            <br /> Best regards,
                            BeGreen Customer Service Team
                          </td>
                        </tr>
                        <tr>
                          <td className="content-block powered-by">
                            Powered by <a href="#">BeGreenShop</a>.
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                  {/* END FOOTER */}
                  {/* END CENTERED WHITE CONTAINER */}
                </div>
              </td>
              <td>&nbsp;</td>
            </tr>
          </tbody>
        </table>
      </div>
    </>
  )
}
export default MailContent;