import React, { useEffect, useState }  from "react"
import { CButton, CFormSelect, CModal, CModalBody, CModalFooter, CModalHeader, CModalTitle, CTable, CTableBody, CTableDataCell, CTableHead, CTableHeaderCell, CTableRow } from "@coreui/react"
import {selectStatusOrder} from "../../../data"
import { useDispatch, useSelector } from "react-redux"
import { editStatusOrderAction, fetchOrderByIdAction, fetchOrdersAction, onChangeStatusOrderAction,
   resetStatusMessageAction, sendEmailAction } from "../../../actions/order.actions"
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import MailContent from "./MailContent"
import ReactDOMServer from 'react-dom/server';
import MailContentCanceled from "./MailContentCanceled"

const ModalEditForm = (data) => {
  const dispatch = useDispatch();
  const {status, pendingEditStatus, editStatusMessage,
    limit, currentPage, searchContent, orderDate, shippedDate
    ,ordered, preparing, delivered, received, canceled, order, orderDetail
  } = useSelector((reduxData) => reduxData.orderReducers);
  //xử lý lưu thay đổi trạng thái đơn hàng
  const handleChangeStatus = (event) =>{
    dispatch(onChangeStatusOrderAction(event.target.value));
  }
  //xử lý khi click thay đổi trạng thái đơn hàng
  const handleEditStatusClick = () => {
    if(status == ""){
      toast.error('Need to choose new status', {containerId:"C"});
    }else{
      dispatch(editStatusOrderAction(status, order._id));
    }
  }
  //thông báo khi edit xong
  useEffect(()=>{
    if(!pendingEditStatus && editStatusMessage == "Order Updated Successfully"){
      toast.success('Edit Status Successfully', {containerId:"A"});
      // tải lại bảng order sau khi cập nhật status
      dispatch(fetchOrdersAction(limit, currentPage, searchContent, 
        orderDate, shippedDate, ordered, preparing, delivered, received, canceled));
      //đóng modal edit
      data.setVisible(false);
      //reset message status
      dispatch(resetStatusMessageAction());
      // dispatch(fetchOrderByIdAction(order._id));
      // gửi email sau khi thay đổi trạng thái đơn hàng cho client
      var mailContentHtml = "";
      if(status !== "canceled") {
        mailContentHtml = ReactDOMServer.renderToStaticMarkup(
          <MailContent order={order} status={status}/>);
      }else{
        mailContentHtml = ReactDOMServer.renderToStaticMarkup(
          <MailContentCanceled order={order} status={status}/>);
      }
        dispatch(sendEmailAction(
          order.customer.email,
          'Order Notification Email Of BeGreenShop',
          mailContentHtml
        ));
    }
    if(!pendingEditStatus && editStatusMessage == "Have an error"){
      toast.error('Edit Status Failed!', {containerId:"B"});
      //đóng modal edit
      data.setVisible(false)
    }
  },[dispatch, pendingEditStatus,editStatusMessage, limit, currentPage, 
    searchContent, orderDate, shippedDate,ordered, preparing, delivered, received, canceled])
  return (
    <>
      <ToastContainer containerId="A" autoClose={2000} hideProgressBar={true} position="top-center" />
      <ToastContainer containerId="B" autoClose={2000} hideProgressBar={true}  position="top-right" />
      <ToastContainer containerId="C" autoClose={2000} hideProgressBar={true}  position="top-right" />
      <CModal
        visible={data.visible}
        onClose={() => data.setVisible(false)}
        aria-labelledby="LiveDemoExampleLabel"
      >
        <CModalHeader>
          <CModalTitle id="LiveDemoExampleLabel">Edit Order Status</CModalTitle>
        </CModalHeader>
        <CModalBody>
          <CFormSelect 
            value={status? status : order.status} 
            aria-label="Default select example"
            onChange={handleChangeStatus}
          >
            <option>Open this select menu</option>
            {selectStatusOrder.map((status, index)=>{
              return (
                <option 
                  key={index} 
                  value={status.value}
                  >
                  {status.title}
                </option>
              )
            })}
          </CFormSelect>
        </CModalBody>
        <CModalFooter>
          <CButton color="secondary" onClick={() => data.setVisible(false)}>
            Close
          </CButton>
          <CButton onClick={handleEditStatusClick} color="primary">Save changes</CButton>
        </CModalFooter>
      </CModal>
    </>
  )
}
export default ModalEditForm;