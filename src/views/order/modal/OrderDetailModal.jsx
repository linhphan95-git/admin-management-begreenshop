import React  from "react"
import { CButton, CModal, CModalBody, CModalFooter, CModalHeader, CModalTitle, CTable, CTableBody, CTableDataCell, CTableHead, CTableHeaderCell, CTableRow } from "@coreui/react"
import { Typography } from "@mui/material"
import { useSelector } from "react-redux";

const OrderDetailModal = (data) => {
  const {orderDetail
   } = useSelector((reduxData) => reduxData.orderReducers);

  return(
    <CModal
      size="lg"
      visible={data.visible}
      onClose={() => data.setVisible(false)}
      aria-labelledby="LiveDemoExampleLabel"
    >
      <CModalHeader onClose={() => data.setVisible(false)}>
        <CModalTitle id="LiveDemoExampleLabel">Order Detail Information </CModalTitle>
      </CModalHeader>
      <CModalBody>
      <CTable>
          <CTableHead>
            <CTableRow>
              <CTableHeaderCell scope="col">Name</CTableHeaderCell>
              <CTableHeaderCell scope="col">Image</CTableHeaderCell>
              <CTableHeaderCell scope="col">Buy Price</CTableHeaderCell>
              <CTableHeaderCell scope="col">Promotion</CTableHeaderCell>
              <CTableHeaderCell scope="col">Order Quantity</CTableHeaderCell>
              <CTableHeaderCell scope="col">Weight</CTableHeaderCell>
              <CTableHeaderCell scope="col">Origin</CTableHeaderCell>
            </CTableRow>
          </CTableHead>
          <CTableBody>
            {orderDetail?.length > 0 && orderDetail.map((value, index) => {
              return (
                <CTableRow key={index}>
                  <CTableDataCell key={`name-${index}`}>{value.data.product.name}</CTableDataCell>
                  <CTableDataCell key={`image-${index}`}>
                    <img
                      style={{ width: "50px", height: "50px" }}
                      src={`http://localhost:8080/images/${value.data.product.imageUrl[0]}`}
                      alt={value.data.product.name}
                    />
                  </CTableDataCell>
                  <CTableDataCell key={`buyPrice-${index}`}>${value.data.product.buyPrice}</CTableDataCell>
                  <CTableDataCell key={`promotionPrice-${index}`}>${value.data.product.promotionPrice}</CTableDataCell>
                  <CTableDataCell key={`quantity-${index}`}>{value.data.quantity}</CTableDataCell>
                  <CTableDataCell key={`weight-${index}`}>{value.data.product.weight} kg</CTableDataCell>
                  <CTableDataCell key={`origin-${index}`}>{value.data.product.origin}</CTableDataCell>
                </CTableRow>
              );
            })}
          </CTableBody>
        </CTable>
        <div>
            <p><strong>Client Note:</strong> {data.orderNote[0]}</p>
            <p><strong>Delivery Info:</strong> {data.orderNote[1]}</p>

        </div>
      </CModalBody>
    </CModal>
  )
}
export default OrderDetailModal;