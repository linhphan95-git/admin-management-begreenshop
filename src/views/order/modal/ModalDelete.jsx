import React, { useEffect } from "react";
import { CButton, CModalTitle, CModalBody, CModalFooter , CModal, CModalHeader } from "@coreui/react"
import { useDispatch, useSelector } from "react-redux";
import { ToastContainer, toast } from 'react-toastify';
import { fetchOrdersAction, resetStatusMessageAction, softDeleteOrderAction } from "../../../actions/order.actions";

const ModalDelete= (data) => {
  
  const dispatch = useDispatch();
  const { pendingOrderSoftDeleted, softDeleteStatusMessage,
    limit, currentPage, searchContent, orderDate, shippedDate
    ,ordered, preparing, delivered, received, canceled
  } = useSelector((reduxData) => reduxData.orderReducers);
  const handleDeleteItemSubmit = async () => {
    // Ensure this check happens after the async operation completes
    dispatch(softDeleteOrderAction(data.orderId))
  };

  useEffect(() => {
    if(!pendingOrderSoftDeleted && softDeleteStatusMessage == "Order Deleted Successfully"){
      toast.success("Delete Order Successfully!",{containerId:"DeleteSuccess"});
      data.setVisible(false);
      dispatch(fetchOrdersAction(limit, currentPage, searchContent,
         orderDate, shippedDate,ordered, preparing, delivered, received, canceled));
      dispatch(resetStatusMessageAction());
    }
    if(!pendingOrderSoftDeleted && softDeleteStatusMessage == "Can not delete active orders"){
      toast.error("Can not delete active orders!",{containerId:"DeleteOrderActive"});
      data.setVisible(false);
      dispatch(fetchOrdersAction(limit, currentPage, searchContent, orderDate, 
        shippedDate, ordered, preparing, delivered, received, canceled));
      dispatch(resetStatusMessageAction());
    }
    
    if(!pendingOrderSoftDeleted && softDeleteStatusMessage == "Have an error"){
      toast.error("Delete Order Failed!",{containerId:"Failed"});
      data.setVisible(false);
      dispatch(resetStatusMessageAction());
    };
  }, [dispatch,pendingOrderSoftDeleted, softDeleteStatusMessage, limit, 
    currentPage, searchContent, orderDate, shippedDate,ordered, preparing, 
    delivered, received, canceled])

  return (
    <>
      <ToastContainer autoClose={1500} hideProgressBar={true} containerId="DeleteSuccess" position="top-center" />
      <ToastContainer autoClose={1500} hideProgressBar={true} containerId="Failed" position="top-right" />
      <ToastContainer autoClose={1500} hideProgressBar={true} containerId="DeleteOrderActive" position="top-right" />

      <CModal
        scrollable
        visible={data.visible}
        onClose={() => data.setVisible(false)}
        aria-labelledby="ScrollingLongContentExampleLabel2"
      >
        <CModalHeader>
          <CModalTitle style={{color:"#2196F3", marginLeft:"35%"}} id="ScrollingLongContentExampleLabel2">Delete Item</CModalTitle>
        </CModalHeader>
        <CModalBody>
          Are you sure deleting this item?
        </CModalBody>
        <CModalFooter>
          <CButton color="secondary" onClick={() => data.setVisible(false)}>
            Close
          </CButton>
          <CButton color="primary" onClick={() => handleDeleteItemSubmit()}>Yes</CButton>
        </CModalFooter>
      </CModal>
    </>
  )
}
export default ModalDelete;