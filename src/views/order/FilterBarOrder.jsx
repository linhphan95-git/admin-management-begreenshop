import React, { useEffect } from 'react'
import { Box, Checkbox, FormControlLabel } from "@mui/material"
import SearchIcon from '@mui/icons-material/Search';
import { useDispatch, useSelector } from 'react-redux';
import { changeToFirstPageAction, onChangeCanceledStatusAction, onChangeDeliveredStatusAction, 
  onChangeOrderedStatusAction, onChangePreparingStatusAction, onChangeReceivedStatusAction, onChangeSearchOrderInfoAction } from '../../actions/order.actions';

const FilterBarOrder = () => {
  const dispatch = useDispatch();
  const {searchContent, ordered, preparing, delivered, received, canceled
  } = useSelector((reduxData)=> reduxData.orderReducers);
  //lưu thay đổi trạng thái sang ordered
  const handleOrderedStatusChange = () => {
    dispatch(changeToFirstPageAction())
    dispatch(onChangeOrderedStatusAction(!ordered))
  }
  //lưu thay đổi trạng thái sang preparing
  const handlePreparingStatusChange = () => {
    dispatch(changeToFirstPageAction())
    dispatch(onChangePreparingStatusAction(!preparing))
  }
  //lưu thay đổi trạng thái sang delivered
  const handleDeliveredStatusChange = () => {
    dispatch(changeToFirstPageAction())
    dispatch(onChangeDeliveredStatusAction(!delivered))
  }
  //lưu thay đổi trạng thái sang received
  const handleReceivedStatusChange = () => {
    dispatch(changeToFirstPageAction())
    dispatch(onChangeReceivedStatusAction(!received))
  }
  //lưu thay đổi trạng thái sang canceled
  const handleCanceledStatusChange = () => {
    dispatch(changeToFirstPageAction())
    dispatch(onChangeCanceledStatusAction(!canceled))
  }
  //get search info order
  const handleOnChangeSearchOrderTable = (event) =>{
    dispatch(changeToFirstPageAction())
    dispatch(onChangeSearchOrderInfoAction(event.target.value))
  }
  return(
    <>
      {/* by name */}
      <Box style={{position:"relative", margin:"10px 0px"}}>
        <SearchIcon style={{color:"#7AB730", position:"absolute", top:"7px", left:"220px"}}/>
        <input value={searchContent} onChange={handleOnChangeSearchOrderTable} style={{backgroundColor:"transparent",color:"#7AB730", 
        border:"2px solid #7AB730", borderRadius:"5px", padding:"5px", width:"250px" }} type="text" placeholder='Search By Cost/Id/Email' />
      </Box>
      {/* by Status */}
      <Box  style={{marginTop:"10px"}}>
        <h5 style={{color:"#F65005"}}>Filter By Order Status</h5>
        <FormControlLabel style={{color:"#7AB730", width:"100%"}} 
          onChange={handleOrderedStatusChange}
          checked={ordered}
          control={<Checkbox color="success"  
          sx={{
              color: "#7AB730",
              '&.Mui-checked': {
                color: "#7AB730",
              },
            }}
          />} 
          label="Ordered" />
        <FormControlLabel style={{color:"#7AB730", width:"100%"}} 
          onChange={handlePreparingStatusChange}
          control={<Checkbox color="success"  
          checked={preparing}
          sx={{
              color: "#7AB730",
              '&.Mui-checked': {
                color: "#7AB730",
              },
            }}
          />} 
          label="Seller is preparing order" />
        <FormControlLabel style={{color:"#7AB730", width:"100%"}} 
        onChange={handleDeliveredStatusChange}
        control={<Checkbox color="success"  
        checked={delivered}
        sx={{
            color: "#7AB730",
            '&.Mui-checked': {
              color: "#7AB730",
            },
          }}
        />} 
        label="Order is being delivered" />
        <FormControlLabel style={{color:"#7AB730", width:"100%"}} 
        onChange={handleReceivedStatusChange}
        control={<Checkbox color="success"  
        checked={received}
        sx={{
            color: "#7AB730",
            '&.Mui-checked': {
              color: "#7AB730",
            },
          }}
        />} 
        label="Customer is received the order" />
        <FormControlLabel style={{color:"#7AB730", width:"100%"}} 
        onChange={handleCanceledStatusChange}
        control={<Checkbox color="success"  
        checked={canceled}
        sx={{
            color: "#7AB730",
            '&.Mui-checked': {
              color: "#7AB730",
            },
          }}
        />} 
        label="Order canceled" />
      </Box>
    </>
  )
}

export default FilterBarOrder;