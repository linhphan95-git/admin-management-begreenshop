import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from 'react-redux';
import { fetchOrderDetailByIdAction } from "../../actions/order.actions";
import {CircularProgress, Box} from '@mui/material';

const ProductOrderDetail = (data) => {
  const dispatch = useDispatch();
  const [combinedOrderDetails, setCombinedOrderDetails] = useState([]);
  const {orderDetail, pendingFetchOrderDetail} = useSelector((reduxData) => reduxData.orderReducers);
  useEffect( function fetchOrderDetail() {
    // const fetchDetails = async () => {
    // Gọi API để lấy dữ liệu những orderDetail trong Order
      dispatch(fetchOrderDetailByIdAction(data.orderDetails));
      // setCombinedOrderDetails(prevDetails => [...prevDetails, ...orderDetail]);
    // };
      // fetchDetails();

  }, [dispatch, data.orderDetails ]); 
  return (
    <div>
      { pendingFetchOrderDetail ? 
        <Box sx={{ display: 'flex', width: '30px', height: '30px' }}>
          <CircularProgress />
        </Box>
        : 
        orderDetail.length > 0 ? 
          orderDetail.map((value, index) => (
            <p key={index}>
              product: {value.data.product ? value.data.product.name : 'N/A'} - 
              quantity: {value.data.quantity}
            </p>
          )) 
          : <p>No details available</p>
      }
    </div>
  );
};
export default ProductOrderDetail;