
import React, { useEffect, useState } from 'react';
import { Box, Tooltip, Typography } from "@mui/material"
import VisibilityIcon from '@mui/icons-material/Visibility';
import { useDispatch, useSelector } from 'react-redux';
import { DataGrid } from '@mui/x-data-grid';
import { fetchOrderByIdAction, fetchOrderDetailByIdAction, fetchOrdersDeletedAction } from '../../../actions/order.actions';
import SettingsBackupRestore from '@mui/icons-material/SettingsBackupRestore';
import OrderDetailModal from '../modal/OrderDetailModal';
import ModalRestoreOrder from './RestoreModal';

const OrderDeleted = (data) => {
  // const [visibleDeleteModal, setVisibleDeleteModal]  = useState(false);
  const dispatch = useDispatch();
  const {ordersDeleted, orderDetail, order  } = useSelector((reduxData) => reduxData.orderReducers);
  useEffect( function fetchUser() {
    // Gọi API để lấy dữ liệu order đổ ra bảng
    dispatch(fetchOrdersDeletedAction());
}, [dispatch]);

  // Function to convert ISO date-time to 'ngày tháng năm giờ' format
const formatDateTime = (isoString) => {
  const date = new Date(isoString);
  const options = { 
    day: '2-digit', 
    month: '2-digit', 
    year: 'numeric', 
    hour: '2-digit', 
    minute: '2-digit', 
    second: '2-digit',
    hour12: false // 24-hour format
  };
    return date.toLocaleDateString('vi-VN', options).replace(',', ' lúc');
  };
  const columns = [
    { 
      field: 'orderNumeric',
      headerName: '#', 
      width: 30 
    },
    { 
      field: '_id',
      headerName: 'Order ID', 
      width: 100 
    },
    { 
      field: 'orderDate', 
      headerName: 'Order Date', 
      width: 160,
      valueGetter: (value, row) => formatDateTime(row.orderDate),
    },
    { 
      field: 'shippedDate',
      headerName: 'Shipped Date',
      width: 160,
      valueGetter: (value, row) => formatDateTime(row.shippedDate),
    },
    {
      field: 'customer',
      headerName: 'Customer',
      width: 150,
      renderCell: (params) => (
        <Box>
          <p>{params.row.customer ? params.row.customer.email : 'No Customer Info'}</p> 
        </Box>
      ),
    },
    {
      field: 'orderDetails',
      headerName: 'Order Details',
      description: 'This column is not sortable.',
      sortable: false,
      width: 100,
      renderCell: (params) =>
         <Box style={{textAlign:"center"}}>
          <Tooltip title="View Detail">
            <VisibilityIcon
              onClick={()=> handleViewDetail(params.row.orderDetails, [params.row.note, params.row.addressDelivery])}
              style={{ color: 'blue', cursor: 'pointer' }}
            />
          </Tooltip>
         </Box>
      ,
    },
    {
      field: 'cost',
      headerName: 'Cost',
      type: 'number',
      width: 70,
    },
    {
      field: 'status',
      headerName: 'Status',
      width: 90,
    },
    {
      field: 'action',
      headerName: 'Action',
      type: 'number',
      width: 60,
      sortable: false,
      filterable: false,
      renderCell: (params) => (
        <Box>
          <Tooltip title="Restore Product">
            <SettingsBackupRestore
              onClick={() => handleRestoreIconClick(params.row._id)}
              style={{ color: 'purple', cursor: 'pointer' }}
            />
          </Tooltip>
        </Box>
        
      ),
    },
  ];
  const rows = ordersDeleted.length>0 ? ordersDeleted.map((order, index) => ({ ...order, orderNumeric: index + 1 })) : ordersDeleted;
  //view detail order
  const [visible, setVisible] = useState(false)
  const [orderNote, setOrderNote] = useState([])
  //xử lý khi click vào xem chi tiết đơn hàng
  const handleViewDetail = async (orderDetails, infoArray) => {
    setOrderNote([]);
    setVisible(true);
    setOrderNote(infoArray);
   await dispatch(fetchOrderDetailByIdAction(orderDetails));
  }
  const [visibleRestoreModal, setVisibleRestoreModal] = useState(false);
  //xử lý khi click icon restore đơn hàng đã xóa
  const handleRestoreIconClick = (orderId) => {
    setVisibleRestoreModal(true);
    dispatch(fetchOrderByIdAction(orderId));
  }
  return(
    <>
     <ModalRestoreOrder 
        visible={visibleRestoreModal}
        setVisible={setVisibleRestoreModal}
        orderId = {order._id}
      />
     <OrderDetailModal orderNote={orderNote} orderDetail={orderDetail} visible={visible} setVisible={setVisible} />
      <div style={{ height: 600, width: '100%' }}>
        <h1 style={{ textAlign:"center" }}>Orders Deleted Management</h1>
        <DataGrid
          className='grid-pagination'
          rows={rows}
          columns={columns}
          getRowHeight={(params) => {
            return 70; // set chiều cao cho các hàng khác
          }}
          initialState={{
            pagination: {
              paginationModel: { page: 0, pageSize: 10 },
            },
          }}
          pageSizeOptions={[5, 10]}
          // checkboxSelection
          getRowId={(row) => row._id}
        />
      </div>
    </>
  )
}

export default OrderDeleted;