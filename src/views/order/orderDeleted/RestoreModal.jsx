import React, { useEffect } from "react";
import { CButton, CModalTitle, CModalBody, CModalFooter , CModal, CModalHeader } from "@coreui/react"
import { ToastContainer, toast } from 'react-toastify';
import { useDispatch, useSelector } from 'react-redux';
import { fetchProductsDeletedAction, resetRestoreAction, restoreProductDeletedAction } from "../../../actions/admin.actions";
import { fetchOrdersDeletedAction, resetRestoreOrderAction, restoreOrdersDeletedAction } from "../../../actions/order.actions";
const ModalRestoreOrder = (data) =>{
  const dispatch = useDispatch();
  const { pendingRestoreDeletedOrder, ordersRestoredMessage } = useSelector((reduxData) => reduxData.orderReducers);
  //thực hiện restore order
  const handleRestoreItemSubmit = () => {
    dispatch(restoreOrdersDeletedAction(data.orderId));
  }
  //thông báo sau khi restore
  useEffect( function restore() {
    if(!pendingRestoreDeletedOrder && ordersRestoredMessage === "Order restored successfully"){
      toast.success("Restore Successfully",{containerId:"RestoreSuccess"});
      dispatch(fetchOrdersDeletedAction());
      //đóng modal
      data.setVisible(false)
      //reset restore message
      dispatch(resetRestoreOrderAction());
    }
    //ban đầu nếu orderMessage === "" thì nó cũng sẽ bị rơi vào ordersRestoredMessage !== "Order restored successfully"
    //nên phải thêm điều kiện ordersRestoredMessage có giá trị
    if(!pendingRestoreDeletedOrder && ordersRestoredMessage && ordersRestoredMessage !== "Order restored successfully"){
      toast.error("Restore Failed",{containerId:"RestoreFailed"})
      dispatch(resetRestoreOrderAction());
    }
  }, [dispatch,pendingRestoreDeletedOrder,ordersRestoredMessage]);

  return(
    <>
      <ToastContainer autoClose={1500} hideProgressBar={true} containerId="RestoreSuccess" position="top-center" />
      <ToastContainer autoClose={1500} hideProgressBar={true} containerId="RestoreFailed" position="top-right" />
      <CModal
        scrollable
        visible={data.visible}
        onClose={() => data.setVisible(false)}
        aria-labelledby="ScrollingLongContentExampleLabel2"
      >
        <CModalHeader>
          <CModalTitle style={{color:"#2196F3", marginLeft:"35%"}} id="ScrollingLongContentExampleLabel2">Restore Item</CModalTitle>
        </CModalHeader>
        <CModalBody>
          Are you sure restoring this item?
        </CModalBody>
        <CModalFooter>
          <CButton color="secondary" onClick={() => data.setVisible(false)}>
            Close
          </CButton>
          <CButton color="primary" onClick={() => handleRestoreItemSubmit()}>Yes</CButton>
        </CModalFooter>
      </CModal>
    </>
  )
}

export default ModalRestoreOrder