import React from 'react'
import { Grid } from "@mui/material"
import {
  CButton,
  CCol,
  CRow,
} from '@coreui/react'
import TableOrder from './TableOrder';
import LongMenuFilter from './LongMenuFilter';
import dayjs from 'dayjs';
import { DemoContainer } from '@mui/x-date-pickers/internals/demo';
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs';
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
import { DateTimePicker } from '@mui/x-date-pickers/DateTimePicker';
import { useDispatch, useSelector } from 'react-redux';
import { onChangeOrderDateAction, onChangeOrderShippedAction, changeToFirstPageAction, exportExcelOrderFile } from '../../actions/order.actions';
import DownloadIcon from '@mui/icons-material/Download';

const Order = () => {
  const dispatch = useDispatch();
  const {orderDate, shippedDate} = useSelector((reduxData)=> reduxData.orderReducers);
  //lưu order date filter thay đổi vào state 
  const handleOrderDateChange = (date) =>{
    dispatch(changeToFirstPageAction())
    dispatch(onChangeOrderDateAction(dayjs(date)));
  }
  //lưu shipped date filter thay đổi vào state 
  const handleShippedDateChange = (date) =>{
    dispatch(changeToFirstPageAction())
    dispatch(onChangeOrderShippedAction(dayjs(date)));
  }
  //xử lý sự kiện click nút tải file excel
  const handleOnClickExportExcelButton = () =>{
    dispatch(exportExcelOrderFile())
  }
  return (
    <>
      <CRow>
        <CCol xs>
          <h1 style={{textAlign:"center", marginBottom:"30px"}}>Order Management</h1>
          {/* table orders */}
          
          <CCol lg={4} md={6} sm={6} xs={6}>
            <LocalizationProvider dateAdapter={AdapterDayjs}>
              <DemoContainer components={['DateTimePicker']}>
                <DateTimePicker 
                  label="Search Order Date" 
                  value={orderDate}
                  onChange={(newValue) => handleOrderDateChange(newValue)}
                />
              </DemoContainer>
            </LocalizationProvider>
          </CCol>
          <CCol lg={4} md={6} sm={6} xs={6}>
            <LocalizationProvider dateAdapter={AdapterDayjs}>
              <DemoContainer components={['DateTimePicker']}>
                <DateTimePicker 
                  label="Search Shipped Date" 
                  value={shippedDate}
                  onChange={(newValue) => handleShippedDateChange(newValue)}
                />
              </DemoContainer>
            </LocalizationProvider>
          </CCol>
          <div style={{display:"flex", marginBottom:"20px", marginTop:"10px"}}>
            {/* filter bar menu */}
            <LongMenuFilter/>
            <CButton 
              onClick={handleOnClickExportExcelButton} 
              color="success" shape="rounded-pill"
              style={{color:"white", marginRight:"10px"}}
            >
              <DownloadIcon/>EXCEL FILE
            </CButton>
          </div>
          <Grid item lg={12} md={12} sm={12} xs={12}>
            <TableOrder/>
          </Grid>
        </CCol>
      </CRow>
      
    </>
  )
}

export default Order
