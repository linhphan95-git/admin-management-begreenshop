import React, { useEffect } from 'react'
import classNames from 'classnames'

import {
  CAvatar,
  CCard,
  CCardBody,
  CCardHeader,
  CCol,
  CProgress,
  CRow,
  CTable,
  CTableBody,
  CTableDataCell,
  CTableHead,
  CTableHeaderCell,
  CTableRow,
} from '@coreui/react'
import CIcon from '@coreui/icons-react'
import {
  cibCcAmex,
  cibCcApplePay,
  cibCcMastercard,
  cibCcPaypal,
  cibCcStripe,
  cibCcVisa,
  cifBr,
  cifEs,
  cifFr,
  cifIn,
  cifPl,
  cifUs,
  cilPeople,
  cilUser,
  cilUserFemale,
  cilBasket,
  cilTruck,
  cilHouse,
  cilGift,
  cilXCircle,
} from '@coreui/icons'

import avatar1 from 'src/assets/images/avatars/1.jpg'
import avatar2 from 'src/assets/images/avatars/2.jpg'
import avatar3 from 'src/assets/images/avatars/3.jpg'
import avatar4 from 'src/assets/images/avatars/4.jpg'
import avatar5 from 'src/assets/images/avatars/5.jpg'
import avatar6 from 'src/assets/images/avatars/6.jpg'

import { CChartDoughnut } from '@coreui/react-chartjs'
import { useDispatch, useSelector } from 'react-redux'
import { fetchFeatureProductAction } from '../../actions/admin.actions'
import { fetchOrdersAction, fetchOrdersInDayAction } from '../../actions/order.actions'
import { fetchCustomersAction } from '../../actions/customer.actions'

const Dashboard = () => {
  const tableExample = [
    {
      avatar: { src: avatar1, status: 'success' },
      user: {
        name: 'Yiorgos Avraamu',
        new: true,
        registered: 'Jan 1, 2023',
      },
      country: { name: 'USA', flag: cifUs },
      usage: {
        value: 50,
        period: 'Jun 11, 2023 - Jul 10, 2023',
        color: 'success',
      },
      payment: { name: 'Mastercard', icon: cibCcMastercard },
      activity: '10 sec ago',
    },
    {
      avatar: { src: avatar2, status: 'danger' },
      user: {
        name: 'Avram Tarasios',
        new: false,
        registered: 'Jan 1, 2023',
      },
      country: { name: 'Brazil', flag: cifBr },
      usage: {
        value: 22,
        period: 'Jun 11, 2023 - Jul 10, 2023',
        color: 'info',
      },
      payment: { name: 'Visa', icon: cibCcVisa },
      activity: '5 minutes ago',
    },
    {
      avatar: { src: avatar3, status: 'warning' },
      user: { name: 'Quintin Ed', new: true, registered: 'Jan 1, 2023' },
      country: { name: 'India', flag: cifIn },
      usage: {
        value: 74,
        period: 'Jun 11, 2023 - Jul 10, 2023',
        color: 'warning',
      },
      payment: { name: 'Stripe', icon: cibCcStripe },
      activity: '1 hour ago',
    },
    {
      avatar: { src: avatar4, status: 'secondary' },
      user: { name: 'Enéas Kwadwo', new: true, registered: 'Jan 1, 2023' },
      country: { name: 'France', flag: cifFr },
      usage: {
        value: 98,
        period: 'Jun 11, 2023 - Jul 10, 2023',
        color: 'danger',
      },
      payment: { name: 'PayPal', icon: cibCcPaypal },
      activity: 'Last month',
    },
    {
      avatar: { src: avatar5, status: 'success' },
      user: {
        name: 'Agapetus Tadeáš',
        new: true,
        registered: 'Jan 1, 2023',
      },
      country: { name: 'Spain', flag: cifEs },
      usage: {
        value: 22,
        period: 'Jun 11, 2023 - Jul 10, 2023',
        color: 'primary',
      },
      payment: { name: 'Google Wallet', icon: cibCcApplePay },
      activity: 'Last week',
    },
    {
      avatar: { src: avatar6, status: 'danger' },
      user: {
        name: 'Friderik Dávid',
        new: true,
        registered: 'Jan 1, 2023',
      },
      country: { name: 'Poland', flag: cifPl },
      usage: {
        value: 43,
        period: 'Jun 11, 2023 - Jul 10, 2023',
        color: 'success',
      },
      payment: { name: 'Amex', icon: cibCcAmex },
      activity: 'Last week',
    },
  ]
  const dispatch = useDispatch();
  const {featureProducts} = useSelector((reduxData)=> reduxData.adminReducers);
  const {orders, ordersLatest, limit, currentPage, searchContent, 
    orderDate, shippedDate, ordered, preparing, delivered, received, canceled } = useSelector((reduxData) => reduxData.orderReducers);
  const {customers } = useSelector((reduxData) => reduxData.customerReducers);
  useEffect(()=>{
    //lấy những sp bán chạy
    dispatch(fetchFeatureProductAction());
    //lấy tất cả orders
    dispatch(fetchOrdersAction(null, currentPage, searchContent, orderDate, 
      shippedDate, ordered, preparing, delivered, received, canceled));
      //lấy tất cả customer
    dispatch(fetchCustomersAction("", "", ""));
    //lấy những order trong ngày
    dispatch(fetchOrdersInDayAction());
  },[dispatch, limit, currentPage, searchContent, orderDate, shippedDate,
    ordered, preparing, delivered, received, canceled
  ])
  const productNames = featureProducts?.slice(0,6).map(item => item.productInfo[0].name);
  const totalQuantity = featureProducts?.slice(0,6).map(item => item.totalQuantity);
  const orderedTotal = orders.reduce((total, order) =>{
    if(order.status ==="ordered"){
      return total + 1;
    }
    return total;
  },0);
  const preparingTotal = orders.reduce((total, order) =>{
    if(order.status ==="preparing"){
      return total + 1;
    }
    return total;
  },0);
  const deliveringTotal = orders.reduce((total, order) =>{
    if(order.status ==="delivering"){
      return total + 1;
    }
    return total;
  },0);
  const receivedTotal = orders.reduce((total, order) =>{
    if(order.status ==="received"){
      return total + 1;
    }
    return total;
  },0);
  const canceledTotal = orders.reduce((total, order) =>{
    if(order.status ==="canceled"){
      return total + 1;
    }
    return total;
  },0);

  const revenue = orders.reduce((total, order) =>{
    if(order.status ==="received"){
      return total + order.cost;
    }
    return total;
  },0);
  const progressGroupExample3 = [
    { title: 'Ordered', icon: cilBasket, percent: (orderedTotal * 100)/orders.length, value: orderedTotal},
    { title: 'Preparing', icon: cilGift, percent: (preparingTotal * 100)/orders.length, value: preparingTotal },
    { title: 'Delivering', icon: cilTruck, percent: (deliveringTotal * 100)/orders.length, value: deliveringTotal},
    { title: 'Received', icon: cilHouse, percent: (receivedTotal * 100)/orders.length, value: receivedTotal},
    { title: 'Cancel', icon: cilXCircle, percent: (canceledTotal * 100)/orders.length, value: canceledTotal},
  ]
  return (
    <>
      <CRow>
        <CCol xs={12}>
          <CCard className="mb-4">
            <CCardHeader>Traffic {' & '} Sales</CCardHeader>
            <CCardBody>
              <CRow>
                <CCol xs={12} md={6} xl={6}>
                  <CRow>
                    <CCol xs={6}>
                      <div className="border-start border-start-4 border-start-info py-1 px-3">
                        <div className="text-body-secondary text-truncate small">Total Customers</div>
                        <div className="fs-5 fw-semibold">{customers.length}</div>
                      </div>
                    </CCol>
                    <CCol xs={6}>
                      <div className="border-start border-start-4 border-start-danger py-1 px-3 mb-3">
                        <div className="text-body-secondary text-truncate small">
                         Total Orders
                        </div>
                        <div className="fs-5 fw-semibold">{orders.length}</div>
                      </div>
                    </CCol>
                  </CRow>
                  <hr className="mt-0" />
                  <h4>Orders Status</h4>
                  {progressGroupExample3.map((item, index) => (
                    <div className="progress-group" key={index}>
                      <div className="progress-group-header">
                        <CIcon className="me-2" icon={item.icon} size="lg" />
                        <span>{item.title}</span>
                        <span className="ms-auto fw-semibold">
                          {item.value}{' '}
                          <span className="text-body-secondary small">({item.percent.toFixed(2)}%)</span>
                        </span>
                      </div>
                      <div className="progress-group-bars">
                        <CProgress thin color="success" value={parseFloat(item.percent.toFixed(2))} />
                      </div>
                    </div>
                  ))}
                </CCol>
                <CCol xs={12} md={6} xl={6}>
                  <CRow>
                    <CCol xs={6}>
                      <div className="border-start border-start-4 border-start-warning py-1 px-3 mb-3">
                        <div className="text-body-secondary text-truncate small">Revenues</div>
                        <div className="fs-5 fw-semibold">$ {revenue.toFixed(2)}</div>
                      </div>
                    </CCol>
                    <CCol xs={6}>
                      <div className="border-start border-start-4 border-start-success py-1 px-3 mb-3">
                        <div className="text-body-secondary text-truncate small">Order Today</div>
                        <div className="fs-5 fw-semibold">{ordersLatest.length}</div>
                      </div>
                    </CCol>
                  </CRow>

                  <hr className="mt-0" />
                  <CCard className="mb-4">
                    <CCardHeader>List 5 Best Sellers Products</CCardHeader>
                    <CCardBody>
                      <CChartDoughnut
                        data={{
                          labels: productNames,
                          datasets: [
                            {
                              backgroundColor: ['#41B883', '#E46651', '#00D8FF', '#DD1B16'],
                              data: totalQuantity,
                            },
                          ],
                        }}
                      />
                    </CCardBody>
                  </CCard>
                  <div className="mb-5"></div>
                </CCol>
              </CRow>
              <br />
              {/* <CTable align="middle" className="mb-0 border" hover responsive>
                <CTableHead className="text-nowrap">
                  <CTableRow>
                    <CTableHeaderCell className="bg-body-tertiary text-center">
                      <CIcon icon={cilPeople} />
                    </CTableHeaderCell>
                    <CTableHeaderCell className="bg-body-tertiary">User</CTableHeaderCell>
                    <CTableHeaderCell className="bg-body-tertiary text-center">
                      Country
                    </CTableHeaderCell>
                    <CTableHeaderCell className="bg-body-tertiary">Usage</CTableHeaderCell>
                    <CTableHeaderCell className="bg-body-tertiary text-center">
                      Payment Method
                    </CTableHeaderCell>
                    <CTableHeaderCell className="bg-body-tertiary">Activity</CTableHeaderCell>
                  </CTableRow>
                </CTableHead>
                <CTableBody>
                  {tableExample.map((item, index) => (
                    <CTableRow v-for="item in tableItems" key={index}>
                      <CTableDataCell className="text-center">
                        <CAvatar size="md" src={item.avatar.src} status={item.avatar.status} />
                      </CTableDataCell>
                      <CTableDataCell>
                        <div>{item.user.name}</div>
                        <div className="small text-body-secondary text-nowrap">
                          <span>{item.user.new ? 'New' : 'Recurring'}</span> | Registered:{' '}
                          {item.user.registered}
                        </div>
                      </CTableDataCell>
                      <CTableDataCell className="text-center">
                        <CIcon size="xl" icon={item.country.flag} title={item.country.name} />
                      </CTableDataCell>
                      <CTableDataCell>
                        <div className="d-flex justify-content-between text-nowrap">
                          <div className="fw-semibold">{item.usage.value}%</div>
                          <div className="ms-3">
                            <small className="text-body-secondary">{item.usage.period}</small>
                          </div>
                        </div>
                        <CProgress thin color={item.usage.color} value={item.usage.value} />
                      </CTableDataCell>
                      <CTableDataCell className="text-center">
                        <CIcon size="xl" icon={item.payment.icon} />
                      </CTableDataCell>
                      <CTableDataCell>
                        <div className="small text-body-secondary text-nowrap">Last login</div>
                        <div className="fw-semibold text-nowrap">{item.activity}</div>
                      </CTableDataCell>
                    </CTableRow>
                  ))}
                </CTableBody>
              </CTable> */}
            </CCardBody>
          </CCard>
        </CCol>
      </CRow>
    </>
  )
}

export default Dashboard
