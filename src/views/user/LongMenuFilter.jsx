import * as React from 'react';
import IconButton from '@mui/material/IconButton';
import Menu from '@mui/material/Menu';
import MenuItem from '@mui/material/MenuItem';
import MoreVertIcon from '@mui/icons-material/MoreVert';
import FilterBar from './FilterBar';
import { useDispatch, useSelector } from 'react-redux';
import { fetchProductTypesAction, fetchProductsAdminAction } from '../../actions/admin.actions';
import { CButton } from '@coreui/react';
// const ITEM_HEIGHT = 55;

export default function LongMenuFilter() {
  const dispatch = useDispatch();
  const [anchorEl, setAnchorEl] = React.useState(null);
  const open = Boolean(anchorEl);
  // khi click thì mở filter bar
  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };
  return (
    <div>
      {/* <IconButton
        aria-label="more"
        id="long-button"
        aria-controls={open ? 'long-menu' : undefined}
        aria-expanded={open ? 'true' : undefined}
        aria-haspopup="true"
        onClick={handleClick}
        style={{border:"none", borderRadius:"5px", marginBottom:"10px", width:"105px",
        height:"32px", fontSize:"17px", color:"white", backgroundColor:"#F65005"}}
      >
        <MoreVertIcon />FILTER
      </IconButton> */}
      <CButton 
        onClick={handleClick} 
        color="primary" shape="rounded-pill"
        style={{color:"white", marginRight:"10px"}}
      >
        <MoreVertIcon />Menu Filter
      </CButton>
      <Menu
        id="long-menu"
        MenuListProps={{
          'aria-labelledby': 'long-button',
        }}
        anchorEl={anchorEl}
        open={open}
        onClose={handleClose}
        PaperProps={{
          style: {
            height: "350px",
            width: '250px',
            paddingLeft:"20px",
            background:"rgb(33 38 49)",
            marginTop:"10px",
            
          },
        }}
      >
        <FilterBar/>
      </Menu>
    </div>
  );
}