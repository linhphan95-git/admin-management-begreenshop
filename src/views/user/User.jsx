import React from 'react'
import { Grid } from "@mui/material"
import {
  CCol,
  CRow,
} from '@coreui/react'
import UserTable from './UserTable'
// import TableOrder from './TableOrder';
const Order = () => {
  return (
    <>
      <CRow>
        <CCol xs>
          <h1 style={{textAlign:"center", marginBottom:"30px"}}>User Management</h1>
          {/* table orders */}
          <Grid item lg={12} md={12} sm={12} xs={12}>
            <UserTable/>
          </Grid>
        </CCol>
      </CRow>
      
    </>
  )
}

export default Order
