import React, { useEffect } from "react";
import { CButton, CModalTitle, CModalBody, CModalFooter , CModal, CModalHeader } from "@coreui/react"
import { useDispatch, useSelector } from "react-redux";
import { ToastContainer, toast } from 'react-toastify';
import { deleteUserAction, fetchUsersAction, resetDeleteUserMessage } from "../../../actions/user.actions";

const ModalDelete= (data) => {
  
  const dispatch = useDispatch();
  const { pendingDeleteUser, userDeletedMessage,limit, currentPage, searchContent, totalPage,
    isAdmin, isModerator, isUser } = useSelector((reduxData) => reduxData.userReducers);
  const handleDeleteItemSubmit = async () => {
    // Ensure this check happens after the async operation completes
    dispatch(deleteUserAction(data.user._id))
  };

  useEffect(() => {
    if(!pendingDeleteUser && userDeletedMessage == "user soft deleted successfully"){
      toast.success("Delete User Successfully!",{containerId:"DeleteUserSuccess"});
      data.setVisible(false);
      dispatch(fetchUsersAction(limit, currentPage, searchContent, totalPage,
        isAdmin, isModerator, isUser));
      dispatch(resetDeleteUserMessage());
    }
    if(!pendingDeleteUser && userDeletedMessage == "Cannot delete user with active orders"){
      toast.error("Cannot delete user with active orders!",{containerId:"DeleteUserActive"});
      data.setVisible(false);
      dispatch(fetchUsersAction(limit, currentPage, searchContent, totalPage,
        isAdmin, isModerator, isUser));
      dispatch(resetDeleteUserMessage());
    }
    if(!pendingDeleteUser && userDeletedMessage == "Have an error"){
      toast.error("Delete User Failed!",{containerId:"DelUserFailed"});
      data.setVisible(false);
      dispatch(resetDeleteUserMessage());
    }
  }, [dispatch,pendingDeleteUser,userDeletedMessage, limit, currentPage, searchContent, totalPage,
    isAdmin, isModerator, isUser])
  return (
    <>
      <ToastContainer autoClose={1500} hideProgressBar={true} containerId="DeleteUserSuccess" position="top-center" />
      <ToastContainer autoClose={1500} hideProgressBar={true} containerId="DelUserFailed" position="top-right" />
      <ToastContainer autoClose={1500} hideProgressBar={true} containerId="DeleteUserActive" position="top-center" />

      <CModal
        scrollable
        visible={data.visible}
        onClose={() => data.setVisible(false)}
        aria-labelledby="ScrollingLongContentExampleLabel2"
      >
        <CModalHeader>
          <CModalTitle style={{color:"#2196F3", marginLeft:"35%"}} id="ScrollingLongContentExampleLabel2">Delete Item</CModalTitle>
        </CModalHeader>
        <CModalBody>
          Are you sure deleting this item?
        </CModalBody>
        <CModalFooter>
          <CButton color="secondary" onClick={() => data.setVisible(false)}>
            Close
          </CButton>
          <CButton color="primary" onClick={() => handleDeleteItemSubmit()}>Yes</CButton>
        </CModalFooter>
      </CModal>
    </>
  )
}
export default ModalDelete;