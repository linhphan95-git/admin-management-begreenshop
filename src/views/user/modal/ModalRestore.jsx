import React, { useEffect } from "react";
import { CButton, CModalTitle, CModalBody, CModalFooter , CModal, CModalHeader } from "@coreui/react"
import { useDispatch, useSelector } from "react-redux";
import { ToastContainer, toast } from 'react-toastify';
import { deleteUserAction, fetchUsersAction, fetchUsersDeletedAction, resetDeleteUserMessage, resetRestoreAction, restoreUsersDeletedAction } from "../../../actions/user.actions";

const ModalRestore= (data) => {
  
  const dispatch = useDispatch();
  const { pendingRestoreDeletedUser, usersRestoredMessage } = useSelector((reduxData) => reduxData.userReducers);
  const handleRestoreItemSubmit = async () => {
    // Ensure this check happens after the async operation completes
    dispatch(restoreUsersDeletedAction(data.user._id))
  };
  
  useEffect(() => {
    if(!pendingRestoreDeletedUser && usersRestoredMessage == "user restored successfully"){
      toast.success("Restore User Successfully!",{containerId:"RestoreUserSuccess"});
      data.setVisible(false);
      dispatch(fetchUsersDeletedAction());
      dispatch(resetRestoreAction());
    }
    if(!pendingRestoreDeletedUser && usersRestoredMessage == "Have an error"){
      toast.error("Restore User Failed!",{containerId:"RestoreUserFailed"});
      data.setVisible(false);
      dispatch(resetRestoreAction());
    }
  }, [dispatch,pendingRestoreDeletedUser,usersRestoredMessage])

  return (
    <>
      <ToastContainer autoClose={1500} hideProgressBar={true} containerId="RestoreUserSuccess" position="top-center" />
      <ToastContainer autoClose={1500} hideProgressBar={true} containerId="RestoreUserFailed" position="top-right" />

      <CModal
        scrollable
        visible={data.visible}
        onClose={() => data.setVisible(false)}
        aria-labelledby="ScrollingLongContentExampleLabel2"
      >
        <CModalHeader>
          <CModalTitle style={{color:"#2196F3", marginLeft:"35%"}} id="ScrollingLongContentExampleLabel2">Restore Item</CModalTitle>
        </CModalHeader>
        <CModalBody>
          Are you sure restoring this item?
        </CModalBody>
        <CModalFooter>
          <CButton color="secondary" onClick={() => data.setVisible(false)}>
            Close
          </CButton>
          <CButton color="primary" onClick={() => handleRestoreItemSubmit()}>Yes</CButton>
        </CModalFooter>
      </CModal>
    </>
  )
}
export default ModalRestore;