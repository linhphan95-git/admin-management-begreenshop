import React, { useEffect, useState }  from "react"
import { CButton, CCol, CForm, CFormCheck, CFormInput, CFormSelect, CModal, CModalBody, CModalFooter, CModalHeader, CModalTitle, CTable, CTableBody, CTableDataCell, CTableHead, CTableHeaderCell, CTableRow } from "@coreui/react"
import {userRoles} from "../../../data"
import { useDispatch, useSelector } from "react-redux"
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { Box, Checkbox, FormControl, FormControlLabel, FormHelperText, IconButton, InputAdornment, InputLabel, OutlinedInput } from "@mui/material";
import Visibility from '@mui/icons-material/Visibility';
import VisibilityOff from '@mui/icons-material/VisibilityOff';
import { changePasswordAction, changeUserInfoAction, fetchUsersAction, resetEditUserMessage } from "../../../actions/user.actions";

const ModalEditForm = (data) => {
  const {users, user, limit, currentPage, searchContent, totalPage,
    isAdmin, isModerator, isUser
   } = useSelector((reduxData) => reduxData.userReducers);
  const dispatch = useDispatch();
  const {pendingUserInfo, userInfoMessage, password} = useSelector((reduxData) => reduxData.userReducers);
  const [rolesNow, setRolesNow] = useState([]);
  // const [password, setPassword] = useState("");
  const [isValid, setIsValid] = useState(true);

  // Sử dụng useEffect để khởi tạo rolesNow khi data.user.roles đã có giá trị
  useEffect(() => {
    if (data.user && data.user.roles) {
      setRolesNow(data.user.roles.map(role => role._id));
    }
  }, [data.user]);
  //hàm xử lý lưu role đã chọn vào state
  const handleRoleChange = (roleId, isChecked) => {
    // Tạo một bản sao của mảng rolesNow để tránh thay đổi trực tiếp state
    let updatedRoles = [...rolesNow];
    
    if (isChecked) {
      // Nếu checkbox được chọn, thêm roleId vào updatedRoles nếu chưa có
      if (!updatedRoles.includes(roleId)) {
        updatedRoles.push(roleId);
      }
    } else {
      // Nếu checkbox bị bỏ chọn, xóa roleId khỏi updatedRoles
      updatedRoles = updatedRoles.filter(id => id !== roleId);
    }
    // Cập nhật state với mảng updatedRoles mới
    setRolesNow(updatedRoles);
  };
  //hàm xử lý lưu role đã chọn vào db
  const handleEditUserInfoClick = () =>{
    if(rolesNow.length >0 ){
      dispatch(changeUserInfoAction(data.user._id, rolesNow ));
    }else{
      toast.error('Need to choose at least 1 role', {containerId:"RoleFailed"});
    }
  }
  
  //edit
  useEffect(()=>{
    if(!pendingUserInfo && userInfoMessage == "User Role updated successfully"){
      toast.success('User role updated successfully', {containerId:"UserSuccess"});
      // tải lại bảng order sau khi cập nhật status
      dispatch(fetchUsersAction(limit, currentPage,searchContent, true, true, true));
      //đóng modal edit
      data.setVisible(false);
      //reset message state
      dispatch(resetEditUserMessage());
    }
    if(!pendingUserInfo && userInfoMessage == "Have an error"){
      toast.error('User Update Failed!', {containerId:"UserFailed"});
      //đóng modal edit
      data.setVisible(false)
    }
  },[dispatch, pendingUserInfo, userInfoMessage, limit, currentPage, searchContent])

  return (
    <>
      <ToastContainer containerId="UserSuccess" autoClose={2000} hideProgressBar={true} position="top-center" />
      <ToastContainer containerId="UserFailed" autoClose={2000} hideProgressBar={true}  position="top-right" />
      <ToastContainer containerId="RoleFailed" autoClose={2000} hideProgressBar={true} position="top-center" />
      <CModal
        visible={data.visible}
        onClose={() => data.setVisible(false)}
        aria-labelledby="LiveDemoExampleLabel"
      >
        <CModalHeader>
          <CModalTitle id="LiveDemoExampleLabel">Edit User Info</CModalTitle>
        </CModalHeader>
        <CModalBody>
          {userRoles.map((role, index) => {
            var isChecked = false;
            if(data.user?.roles?.length>0 ){
              // isChecked = data.user.roles.some(dataRole => dataRole._id === role.value);
              isChecked = rolesNow.includes(role.value) ? true : false;
            }
            return <FormControlLabel key={index} 
                      control={<Checkbox color="success" checked={isChecked} />}  
                      label={role.title} 
                      onChange={(event) => handleRoleChange(role.value,event.target.checked)} 
                    />
          })}
            {/* <Box>
              <h6>Change New Password</h6>
              <FormControl sx={{ m: 1, width: '95%' }} variant="outlined">
                <InputLabel htmlFor="outlined-adornment-password">Password</InputLabel>
                <OutlinedInput
                  id="outlined-adornment-password"
                  type={showPassword ? 'text' : 'password'}
                  endAdornment={
                    <InputAdornment position="end">
                      <IconButton
                        aria-label="toggle password visibility"
                        onClick={handleClickShowPassword}
                        onMouseDown={handleMouseDownPassword}
                        edge="end"
                      >
                        {showPassword ? <VisibilityOff /> : <Visibility />}
                      </IconButton>
                    </InputAdornment>
                  }
                  label="Password"
                  onChange={handleChangePassword}
                  defaultValue={password}
                  inputProps={{
                    pattern: "^(?=.*[A-Za-z])(?=.*\\d)[A-Za-z\\d]{8,}$"  // Ví dụ pattern: ít nhất 8 ký tự, bao gồm chữ cái và số
                  }}
                />
                {!isValid && <FormHelperText style={{color:"red"}}>Invalid password format. Password must be at least 8 characters long and contain at least one letter and one number.</FormHelperText>}
              </FormControl>
            </Box> */}
            
        </CModalBody>
        <CModalFooter>
          <CButton color="secondary" onClick={() => data.setVisible(false)}>
            Close
          </CButton>
          <CButton
            onClick={handleEditUserInfoClick}
            color="primary"
            variant="outline"
            disabled={!isValid} // Sử dụng thuộc tính disabled dựa trên isValid
          >
            Save changes
          </CButton>
        </CModalFooter>
      </CModal>
    </>
  )
}
export default ModalEditForm;