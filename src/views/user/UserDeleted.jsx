
import React, { useEffect, useState } from 'react';
import { Box, TextField, Tooltip } from "@mui/material"
import { useDispatch, useSelector } from 'react-redux';
import { DataGrid } from '@mui/x-data-grid';
import { fetchUserDetailAction, fetchUsersDeletedAction } from '../../actions/user.actions';
import SettingsBackupRestoreIcon from '@mui/icons-material/SettingsBackupRestore';
import ModalRestore from './modal/ModalRestore';


const UserDeleted = () => {
  // const [visibleDeleteModal, setVisibleDeleteModal]  = useState(false);
  const dispatch = useDispatch();
  const {usersDeleted, user} = useSelector((reduxData) => reduxData.userReducers);
  useEffect( function fetchUser() {
    // Gọi API để lấy dữ liệu order đổ ra bảng
    dispatch(fetchUsersDeletedAction());
  }, [dispatch]);
  
  const columns = [
    { 
      field: 'orderNumeric',
      headerName: '#', 
      width: 30 
    },
    { 
      field: '_id',
      headerName: 'User ID', 
      width: 150 
    },
    { 
      field: 'username', 
      headerName: 'Username', 
      width: 150,
    },
    { 
      field: 'email',
      headerName: 'Email',
      width: 150,
    },
    {
      field: 'customer',
      headerName: 'Is Customer',
      width: 150,
      renderCell: (params) => {
        return (
          <div>
            {params.row.customer != "" ?
              <p>{params.row.customer}</p>
              :
              <p>No</p>
            }
          </div>
        );
      }
    },
    {
      field: 'roles',
      headerName: 'Roles',
      sortable: false,
      width: 150,
      renderCell: (params) => {
        return (
          <div style={{ marginTop:"17px", display: 'flex', flexDirection: 'column', lineHeight:"5px" }}>
            {params.row.roles.map((role, index) => (
              <p key={index}>{role.name}</p>
            ))}
          </div>
        );
      }
    },
    {
      field: 'action',
      headerName: 'Action',
      type: 'number',
      width: 90,
      sortable: false,
      filterable: false,
      renderCell: (params) => (
        <Box>
          <Tooltip title="Restore Product">
            <SettingsBackupRestoreIcon
              onClick={() => handleRestoreIconClick(params.row.username)}
              style={{ color: 'purple', cursor: 'pointer' }}
            />
          </Tooltip>
        </Box>
        
      ),
    },
  ];
  const rows = usersDeleted.map((user, index) => ({ ...user, orderNumeric: index + 1 }));;
  //restore user action
  const [visibleRestoreModal, setVisibleRestoreModal] = useState(false);
  const handleRestoreIconClick = (username) => {
    setVisibleRestoreModal(true);
    dispatch(fetchUserDetailAction(username));
  }
  return(
    <>
      <div style={{ height: 600, width: '100%' }}>
        <h1 style={{ textAlign:"center" }}>User Deleted Management</h1>
        <DataGrid
          className='grid-pagination'
          rows={rows}
          columns={columns}
          getRowHeight={(params) => {
            return 70; // set chiều cao cho các hàng khác
          }}
          initialState={{
            pagination: {
              paginationModel: { page: 0, pageSize: 10 },
            },
          }}
          pageSizeOptions={[5, 10]}
          // checkboxSelection
          getRowId={(row) => row._id}
        />
      </div>
      <ModalRestore visible={visibleRestoreModal} setVisible={setVisibleRestoreModal} user={user[0]}/> 
    </>
  )
}

export default UserDeleted;