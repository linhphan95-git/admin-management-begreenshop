import React, { useEffect } from 'react'
import { Box, Checkbox, FormControlLabel } from "@mui/material"
import SearchIcon from '@mui/icons-material/Search';
import { useDispatch, useSelector } from 'react-redux';
import { onChangeAdminRoleAction, onChangeFirstPage, onChangeModeratorRoleAction, onChangeSearchUserAction, onChangeUserRoleAction } from '../../actions/user.actions';
const FilterBar = () => {
  const dispatch = useDispatch();
  const {searchContent, isAdmin, isModerator, isUser} = useSelector((reduxData)=> reduxData.userReducers);

  const handleAdminChange = () => {
    dispatch(onChangeFirstPage());
    dispatch(onChangeAdminRoleAction(!isAdmin))
  }
  const handleModeratorChange = () => {
    dispatch(onChangeFirstPage())
    dispatch(onChangeModeratorRoleAction(!isModerator))
  }
  const handleUserChange = () => {
    dispatch(onChangeFirstPage())
    dispatch(onChangeUserRoleAction(!isUser))
  }
  const handleOnChangeSearchUser = (event) =>{
    dispatch(onChangeFirstPage())
    dispatch(onChangeSearchUserAction(event.target.value))
  }
  return(
    <>
      {/* by name */}
      <Box style={{position:"relative", margin:"10px 0px"}}>
        <SearchIcon style={{color:"#7AB730", position:"absolute", top:"7px", left:"170px"}}/>
        <input value={searchContent} onChange={handleOnChangeSearchUser} style={{backgroundColor:"transparent",color:"#7AB730", 
        border:"2px solid #7AB730", borderRadius:"5px", padding:"5px" }} type="text" placeholder='Search...' />
      </Box>
      {/* by Role */}
      <Box  style={{marginTop:"10px"}}>
        <h5 style={{color:"#F65005"}}>Filter By Role</h5>
        <FormControlLabel style={{color:"#7AB730", width:"100%"}} 
          onChange={handleAdminChange}
          checked={isAdmin}
          control={<Checkbox color="success"  
          sx={{
              color: "#7AB730",
              '&.Mui-checked': {
                color: "#7AB730",
              },
            }}
          />} 
          label="Admin" />
        <FormControlLabel style={{color:"#7AB730", width:"100%"}} 
          onChange={handleModeratorChange}
          control={<Checkbox color="success"  
          checked={isModerator}
          sx={{
              color: "#7AB730",
              '&.Mui-checked': {
                color: "#7AB730",
              },
            }}
          />} 
          label="Moderator" />
        <FormControlLabel style={{color:"#7AB730", width:"100%"}} 
        onChange={handleUserChange}
        control={<Checkbox color="success"  
        checked={isUser}
        sx={{
            color: "#7AB730",
            '&.Mui-checked': {
              color: "#7AB730",
            },
          }}
        />} 
        label="User" />
      </Box>
    </>
  )
}

export default FilterBar;