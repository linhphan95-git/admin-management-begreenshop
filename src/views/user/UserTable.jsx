
import React, { useEffect, useState } from 'react';
import EditIcon from '@mui/icons-material/Edit';
import DeleteIcon from '@mui/icons-material/Delete';
import { useDispatch, useSelector } from 'react-redux';
import { exportExcelUserFile, fetchUserDetailAction, fetchUsersAction, onChangePaginationUserAction } from '../../actions/user.actions';
import ModalEditForm from './modal/ModalEditForm';
import ModalDelete from './modal/ModalDelete';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCheck, faUserSecret, faUserShield, faUsers,  } from '@fortawesome/free-solid-svg-icons';
import LongMenuFilter from './LongMenuFilter';
import { Grid, Pagination, Paper, Table, TableBody, TableCell, TableContainer, TableHead, TableRow, Tooltip, Typography } from "@mui/material"
import DownloadIcon from '@mui/icons-material/Download';
import { CButton } from '@coreui/react';

const UserTable = () => {
  const dispatch = useDispatch();
  const {users, user, limit, currentPage, searchContent, totalPage,
    isAdmin, isModerator, isUser
   } = useSelector((reduxData) => reduxData.userReducers);
  useEffect( function fetchUser() {
    // Gọi API để lấy dữ liệu toàn bộ users đổ ra bảng
    dispatch(fetchUsersAction(limit, currentPage, searchContent, isAdmin, isModerator, isUser));
  }, [dispatch, limit, currentPage, searchContent, isAdmin, isModerator, isUser]);
  const resultLoginInfo = JSON.parse(localStorage.getItem('adminLoginInfo')) || [] // Lấy mảng roles từ localStorage và parse nó thành object
  const hasAdmin = resultLoginInfo[0]?.roles?.some(role=> role.name ==="admin");
  // edit user roles
  const [visibleEditModal, setVisibleEditModal] = useState(false);
  const handleEditIconClick = (username) => {
    if(hasAdmin){
      setVisibleEditModal(true);
      dispatch(fetchUserDetailAction(username));
    }else{
      toast.error("You don't have permission to edit!", {
        position: "top-center",
      });
    }      
  }
  //delete user
  const [visibleDeleteModal, setVisibleDeleteModal] = useState(false);
  const handleDeleteIconClick = async (username) => {
    if(hasAdmin){
      setVisibleDeleteModal(true);
      await dispatch(fetchUserDetailAction(username));
    }else{
      toast.error("You don't have permission to delete!", {
        position: "top-center",
      });
    }   
  }
  const onChangePaginationUser = (event, value) => {
    dispatch(onChangePaginationUserAction(value))
  }

  //xử lý sự kiện click nút tải file excel
  const handleOnClickExportExcelButton = () =>{
    dispatch(exportExcelUserFile())
  }

  return(
    <>
      {/* <LongMenuFilter/>
      <button onClick={handleOnClickExportExcelButton} style={{color:"white", backgroundColor:"#21a727",
        border:"none", padding:"5px 3px", borderRadius:"5px", marginBottom:"10px"}}>
        <DownloadIcon/>EXCEL FILE
      </button>  */}
      <div style={{display:"flex", marginBottom:"20px"}}>
        {/* filter bar menu */}
        <LongMenuFilter/>
        <CButton 
          onClick={handleOnClickExportExcelButton} 
          color="success" shape="rounded-pill"
          style={{color:"white", marginRight:"10px"}}
        >
          <DownloadIcon/>Excel file
        </CButton>
      </div>
      <TableContainer component={Paper}>
        <Table sx={{ minWidth: 650 }} aria-label="simple table">
            <TableHead>
            <TableRow>
                <TableCell sx={{ fontWeight:"bolder" }}>#</TableCell>
                <TableCell sx={{ fontWeight:"bolder" }}>User ID</TableCell>
                <TableCell sx={{ fontWeight:"bolder" }}>Username</TableCell>
                <TableCell sx={{ fontWeight:"bolder" }}>Email</TableCell>
                <TableCell sx={{ fontWeight:"bolder", width:"50px" }}>Is Customer</TableCell>
                <TableCell sx={{ fontWeight:"bolder" }}>Role</TableCell>
                <TableCell sx={{ fontWeight:"bolder" }}>Action</TableCell>
            </TableRow>
            </TableHead>
            <TableBody>
            {
              users.length > 0 &&
              users.map((element, index) => {
                return (
                  <TableRow 
                    sx={{
                      '&:last-child td, &:last-child th': { border: 0 },
                      '&:hover': {
                        backgroundColor: '#f5f5f5',
                      },
                      backgroundColor: index % 2 === 0 ? '#f9f9f9' : '#ffffff',
                    }}
                    key={index}>
                      <TableCell>{(currentPage-1)*limit + (index+1)}</TableCell>
                      <TableCell>{element._id}</TableCell>
                      <TableCell>{element.username}</TableCell>
                      <TableCell>{element.email}</TableCell>
                      <TableCell>{element.customer ? 
                        <FontAwesomeIcon style={{color:"green"}} icon={faCheck}/>
                        : 
                        "no"
                        }
                      </TableCell>
                      <TableCell>
                        <div style={{ marginTop: "5px", display: 'flex', flexDirection: 'column', lineHeight: "5px" }}>
                          {element.roles.map((role, index) => {
                            if (role.name === "user") {
                              return (
                                <Typography key={index} variant="body1">
                                  <FontAwesomeIcon title='User' style={{color:"#f44336", cursor: 'pointer'}} icon={faUsers} /> {role.name}
                                </Typography>
                              );
                            } else if (role.name === "admin") {
                              return (
                                <Typography key={index} variant="body1">
                                  <FontAwesomeIcon title='Admin' style={{marginRight:"5px",color:"green" , cursor: 'pointer'}} icon={faUserSecret} />  {role.name} 
                                </Typography>
                              );
                            } else {
                              return (
                                <Typography key={index} variant="body1">
                                  <FontAwesomeIcon title='Moderator' style={{color:"#673ab7", cursor: 'pointer'}} icon={faUserShield} /> {role.name}
                                </Typography>
                              );
                            }
                          })}
                        </div>
                      </TableCell>
                      <TableCell>
                      <Tooltip title="Edit">
                        <EditIcon
                        onClick={() => handleEditIconClick(element.username)}
                          style={{ color: '#3f51b5', cursor: 'pointer' }}
                        />
                      </Tooltip>
                      <Tooltip title="Delete">
                        <DeleteIcon 
                          onClick={()=> handleDeleteIconClick(element.username)}
                          style={{color:"red", cursor:"pointer"}}
                        />
                      </Tooltip>
                      </TableCell>
                  </TableRow>
                ) 
              })
            }
            </TableBody>
        </Table>
      </TableContainer>
      <Grid item lg={12} md={12} sm={12} xs={12} mt={5} mb={2}>
          <Pagination count={totalPage} page={currentPage}
           variant="outlined" color="secondary"
           onChange={onChangePaginationUser}
           />
      </Grid>
      <ModalDelete visible={visibleDeleteModal} setVisible={setVisibleDeleteModal} user={user[0]}></ModalDelete>
      <ModalEditForm visible={visibleEditModal} setVisible={setVisibleEditModal} user={user[0]}/>
      <ToastContainer/>
    </>
  )
}

export default UserTable;