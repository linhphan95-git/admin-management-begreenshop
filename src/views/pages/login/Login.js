import React from 'react'
import { Link } from 'react-router-dom'
import {
  CButton,
  CCard,
  CCardBody,
  CCardGroup,
  CCol,
  CContainer,
  CForm,
  CFormInput,
  CInputGroup,
  CInputGroupText,
  CRow,
} from '@coreui/react'
import CIcon from '@coreui/icons-react'
import { cilLockLocked, cilUser } from '@coreui/icons'
import { useDispatch, useSelector } from "react-redux";
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { changeLoginValueFormAdminAction, loginAdminAction, logoutAction } from '../../../actions/user.actions'

const Login = () => {
  const dispatch = useDispatch();
  const { loginInfoInput, pendingLogin, resultLoginInfo } = useSelector((reduxData) => reduxData.userReducers);

  const handleInputLoginChange = (event) =>{
    const {id, value} = event.target;
    // console.log(id);
    // console.log(value);
    dispatch(changeLoginValueFormAdminAction({
      ...loginInfoInput,
      [id]:value
    }))
  }
  // console.log(loginInfoInput)
  const handleLoginSubmit = () => {
    if(!loginInfoInput.username){
      toast.error("Username is required", {
        position: "top-center", hideProgressBar:true,
      })
      return false;
    }
    if(!loginInfoInput.password){
      toast.error("Password is required", {
        position: "top-center", hideProgressBar:true,
      })
      return false;
    }
    dispatch(loginAdminAction(loginInfoInput))
  }
  // console.log(resultLoginInfo);
  React.useEffect(() => {
    // Kiểm tra accessToken, nếu có hiển thị thông báo
    if (!pendingLogin && resultLoginInfo.accessToken) {
      // sessionStorage.setItem('accessToken', resultLoginInfo.accessToken);
      // sessionStorage.setItem('refreshToken', resultLoginInfo.refreshToken);
      // alert("Đăng nhập thành công");
      const hasAdmin = resultLoginInfo.roles.some(role=> role.name ==="admin");
      const hasModerator = resultLoginInfo.roles.some(role=> role.name ==="moderator");

      if(hasAdmin || hasModerator){
        toast.success("Login Successfully!", {
          position: "top-center",
        })
        // Redirect to login page if user is not logged in
        setTimeout(() => {
          window.location.href = "/";
        }, 2000); // 1000ms tương đương 1 giây
      }else{
        toast.error("You don't have permission to access!", {
          position: "top-center",
        });
        //ko lưu thông tin đăng nhập vào storage 
        dispatch(logoutAction());
      }
      
    }
    if (!pendingLogin && resultLoginInfo.message) {
      toast.error("Login Failed! Please Check Your Account And Password", {
        position: "top-center", hideProgressBar:true,
      })
    }
  }, [pendingLogin,resultLoginInfo ]);
  return (
  <>
    <div className="bg-body-tertiary min-vh-100 d-flex flex-row align-items-center">
      <CContainer>
        <CRow className="justify-content-center">
          <CCol md={8}>
            <CCardGroup>
              <CCard className="p-4">
                <CCardBody>
                  <CForm>
                    <h1>Login</h1>
                    <p className="text-body-secondary">Sign In to your account</p>
                    <CInputGroup className="mb-3">
                      <CInputGroupText>
                        <CIcon icon={cilUser} />
                      </CInputGroupText>
                      <CFormInput 
                        onChange={handleInputLoginChange} 
                        id='username'
                        placeholder="Username"
                        autoComplete="username" />
                    </CInputGroup>
                    <CInputGroup className="mb-4">
                      <CInputGroupText>
                        <CIcon icon={cilLockLocked} />
                      </CInputGroupText>
                      <CFormInput
                        onChange={handleInputLoginChange}
                        type="password"
                        id='password'
                        placeholder="Password"
                        autoComplete="current-password"
                      />
                    </CInputGroup>
                    <CRow>
                      <CCol xs={6}>
                        <CButton onClick={handleLoginSubmit} color="primary" className="px-4">
                          Login
                        </CButton>
                      </CCol>
                      <CCol xs={6} className="text-right">
                        {/* <CButton color="link" className="px-0">
                          Forgot password?
                        </CButton> */}
                      </CCol>
                    </CRow>
                  </CForm>
                </CCardBody>
              </CCard>
              <CCard className="text-white bg-primary py-5" style={{ width: '44%' }}>
                <CCardBody className="text-center">
                  <div>
                    <h2>Sign up</h2>
                    <p>
                      Welcome to admin page of BeGreen Shop
                    </p>
                    <Link to="http://localhost:3006/signup">
                      <CButton color="primary" className="mt-3" active tabIndex={-1}>
                        Register Now!
                      </CButton>
                    </Link>
                  </div>
                </CCardBody>
              </CCard>
            </CCardGroup>
          </CCol>
        </CRow>
      </CContainer>
    </div>
    <ToastContainer/>
  </>

  )
}

export default Login
