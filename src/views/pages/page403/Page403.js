import React from 'react'
import {
  CButton,
  CCol,
  CContainer,
  CFormInput,
  CInputGroup,
  CInputGroupText,
  CRow,
} from '@coreui/react'
import CIcon from '@coreui/icons-react'
import { cilMagnifyingGlass } from '@coreui/icons'
import { Button } from 'bootstrap'

const Page403 = () => {
  return (
    <div className="bg-body-tertiary min-vh-100 d-flex flex-row align-items-center">
      <CContainer>
        <CRow className="justify-content-center">
          <CCol md={6}>
            <div className="clearfix">
              <img width={"500px"} src='https://www.psdstamps.com/wp-content/uploads/2022/04/round-access-denied-stamp-png-768x512.png'></img>
              <h1 className="float-start display-3 me-4">403</h1>
              <h4 className="pt-3">Forbidden.</h4>
              <p className="text-body-secondary float-start">
                We are sorry but you do not have access to this page or resource!
              </p>
            </div>
            <a href='/'>
              <button 
                style={{border:"none", color:"white", backgroundColor:"green", padding:"5px", borderRadius:"5px"}}>
                Go Back Home
              </button>
            </a>
           
          </CCol>
        </CRow>
      </CContainer>
    </div>
  )
}

export default Page403
