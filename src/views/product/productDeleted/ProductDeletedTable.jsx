
import React, { useEffect, useState } from 'react';
import { DataGrid} from '@mui/x-data-grid';
import { fetchProductDetailAction, fetchProductsDeletedAction, restoreProductDeletedAction } from '../../../actions/admin.actions';
import { useDispatch, useSelector } from 'react-redux';
import { Box, Tooltip } from '@mui/material';
import SettingsBackupRestoreIcon from '@mui/icons-material/SettingsBackupRestore';
import ModalRestore from '../modal/ModalRestore';

const ProductDeletedTable = () => {
  const dispatch = useDispatch();
  const {pendingFetchProductDeleted, productsDeleted, productDetail} = useSelector((reduxData) => reduxData.adminReducers);
  useEffect( function fetchProducts() {
    // Gọi API để lấy dữ liệu order đổ ra bảng
    dispatch(fetchProductsDeletedAction());
  }, [dispatch]);

  const columns = [
    { 
      field: 'order',
      headerName: '#', 
      width: 50,
      sortable: false,
      filterable: false,
    },
    { 
      field: 'name',
      headerName: 'Product Name', 
      width: 150 
    },
    { 
      field: 'imageUrl', 
      headerName: 'Image', 
      width: 100,
      sortable: false,
      filterable: false,
      renderCell: (params) => (
        <div>
          <img
            width="50px"
            height="50px"
            src={`http://localhost:8080/images/${params.row.imageUrl[0]}`}
            alt="Product"
          />
        </div>
      ),
    },
    { 
      field: 'buyPrice',
      headerName: 'Price',
      width: 100,
    },
    {
      field: 'promotionPrice',
      headerName: 'Promotion',
      width: 100,
    },
    {
      field: 'amount',
      headerName: 'Stock',
      width: 100,
    },
    {
      field: 'origin',
      headerName: 'Origin',
      width: 100,
    },
    {
      field: 'weight',
      headerName: 'Weight (kg)',
      width: 100,
    },
    {
      field: 'action',
      headerName: 'Action',
      type: 'number',
      width: 90,
      sortable: false,
      filterable: false,
      renderCell: (params) => (
        <Box>
          <Tooltip title="Restore Product">
            <SettingsBackupRestoreIcon
              onClick={() => handleRestoreIconClick(params.row._id)}
              style={{ color: 'purple', cursor: 'pointer' }}
            />
          </Tooltip>
        </Box>
        
      ),
    },
  ];
  //dùng map sau đó tạo thêm field order để lấy số thứ tự sp
  const rows = productsDeleted.map((product, index) => ({ ...product, order: index + 1 }));
  const [visibleRestoreModal, setVisibleRestoreModal] = useState(false);
  const handleRestoreIconClick =  (productId) =>{
    dispatch(fetchProductDetailAction(productId));
    setVisibleRestoreModal(true);
  }
  return(
     <>
      <ModalRestore 
        visible={visibleRestoreModal}
        setVisible={setVisibleRestoreModal}
        productId = {productDetail._id}
      />
      <div style={{ height: 600, width: '100%' }}>
        <DataGrid
          className='grid-pagination'
          rows={rows}
          columns={columns}
          getRowHeight={(params) => {
            return 100; // set chiều cao cho các hàng khác
          }}
          initialState={{
            pagination: {
              paginationModel: { page: 0, pageSize: 10 },
            },
          }}
          pageSizeOptions={[5, 10]}
          // checkboxSelection
          getRowId={(row) => row._id}
        />
      </div>
     </>
  )
}

export default ProductDeletedTable;