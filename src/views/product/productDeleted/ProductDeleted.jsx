import React, { useEffect } from 'react'
import { Grid } from "@mui/material"
import {
  CCol,
  CRow,
} from '@coreui/react'
import ProductDeletedTable from './ProductDeletedTable';
const ProductDeleted = () => {
  return (
    <>
      <CRow>
        <CCol xs>
          <h1 style={{textAlign:"center", marginBottom:"30px"}}>Products Deleted Management</h1>
          {/* table products Deleted */}
          <Grid item lg={12} md={12} sm={12} xs={12}>
            <ProductDeletedTable/>
          </Grid>
        </CCol>
      </CRow>
      
    </>
  )
}

export default ProductDeleted
