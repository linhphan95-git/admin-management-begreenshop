import React, { useEffect, useState } from 'react'
import { Button, Grid, Pagination} from "@mui/material"
import {
  CButton,
  CCol,
  CRow,
} from '@coreui/react'
import { useDispatch, useSelector } from 'react-redux'
import { exportExcelProductFile, fetchProductTypesAction, fetchProductsAdminAction,
   onChangePaginationAction, resetAfterCreateAction, 
   resetEditMessageAction} from '../../actions/admin.actions';
import TableProduct from './TableProduct'
import AddIcon from '@mui/icons-material/Add';
import DownloadIcon from '@mui/icons-material/Download';
import LongMenuFilter from './LongMenuFilter'
import ModalForm from './modal/Modal';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';


const Product = () => {
  const dispatch = useDispatch();
  const { 
    products, limitProductsPage,
    totalPage, currentPage, minPrice, maxPrice, productName, productEdited, pendingEdit,
    inStock, outStock, allStock, typeId, pendingNewProduct, message, editMessage
    } = useSelector((reduxData) => reduxData.adminReducers);

  useEffect( function fetchProduct() {
      // Gọi API để lấy dữ liệu
      dispatch(fetchProductsAdminAction(limitProductsPage,currentPage,
        minPrice, maxPrice, productName,inStock, outStock, allStock,typeId));
  }, [dispatch, limitProductsPage, currentPage,minPrice, maxPrice, productName,
     inStock, outStock, allStock, typeId]);

  //sự kiện chuyển trang sản phẩm
  const onChangePagination = (event, value) => {
    dispatch(onChangePaginationAction(value))
  }
  //open/close modal add new
  const [visible, setVisible] = useState(false);
  //open/close modal edit 
  const [visibleEditForm, setVisibleEditForm]  = useState(false);

  const handleOnClickAddNewModalButton = () => {
    //mở modal lên
    setVisible(!visible);
    //lấy types để đổ vào select option
    dispatch(fetchProductTypesAction())
    //reset lại form modal add new sau khi đóng
    dispatch(resetAfterCreateAction());
  }
  //xử lý sự kiện click nút tải file excel
  const handleOnClickExportExcelButton = () =>{
    dispatch(exportExcelProductFile())
  }

  //hiển thị thông báo sau khi click nút tạo mới
  useEffect( function createFunction () {
    //thành công
    if(!pendingNewProduct && message  === "Create Successfully" ){
      // Hiển thị thông báo tạo thành công
      toast.success('😁 Create Successfully!', {containerId: 'D'});
      //cập nhật lại dữ liệu sản phẩm sau khi tạo mới
      dispatch(fetchProductsAdminAction(limitProductsPage,currentPage,minPrice, maxPrice, productName,inStock, outStock, allStock,typeId));
      //đóng lại modal khi tạo thành công
      setVisible(false);
      dispatch(resetEditMessageAction());
    }
    //tạo mới thất bại
    if(!pendingNewProduct && message.includes("Have an error") && message.includes("duplicate key")){
      // Hiển thị nếu tên sp bị trùng với sp khác
      toast.error("Create Failed! The product name is duplicated", {containerId: 'B'});
      dispatch(resetEditMessageAction());
    }
    if(!pendingNewProduct && message.includes("Have an error") && !message.includes("duplicate key")){
      // Hiển thị thông báo các trường hợp lỗi khác
      toast.error("Create Failed! Have an error", {containerId: 'B'});
      dispatch(resetEditMessageAction());
    }
  }, [dispatch, pendingNewProduct, message, setVisible ])

  // hiển thị thông báo sau khi click submit nút edit
  useEffect(()=>{
    //EDIT
    if(!pendingEdit && editMessage  === "Edit Successfully" ){
      // Hiển thị thông báo
      toast.success('😁 Edit Successfully!', {containerId: 'C'});
      //cập nhật lại dữ liệu sản phẩm sau khi tạo mới
      dispatch(fetchProductsAdminAction(limitProductsPage,currentPage,minPrice, maxPrice, productName,inStock, outStock, allStock,typeId));
      //đóng modal sau khi edit thành công
      setVisibleEditForm(false);
      dispatch(resetEditMessageAction());
    }
    if(!pendingEdit && editMessage.includes("Have an error") && editMessage.includes("duplicate key")){
      // Hiển thị thông báo nếu tên sp bị trùng sp khác
      toast.error("Edit Failed! The product name is duplicated", {containerId: 'A'});
      dispatch(resetEditMessageAction())
    }
    if(!pendingEdit && editMessage.includes("Have an error") && !editMessage.includes("duplicate key")){
      // Hiển thị thông báo nếu edit fail do các case khác
      toast.error("Edit Failed! Have an error", {containerId: 'A'});
      dispatch(resetEditMessageAction())
    }
  }, [dispatch, setVisibleEditForm, pendingEdit, editMessage ])

  return (
    <>
    {/* đặt id cho mỗi toast nếu có nhiều toast để tránh lỗi */}
    <ToastContainer autoClose={1500} hideProgressBar={true} containerId="A" position="top-right" />
    <ToastContainer autoClose={1500} hideProgressBar={true} containerId="B" position="top-right"/>
    <ToastContainer autoClose={1500} hideProgressBar={true} containerId="C" position="top-center" />
    <ToastContainer autoClose={1500} hideProgressBar={true} containerId="D" position="top-center" />

      <CRow>
        <CCol xs>
          <h1 style={{textAlign:"center"}}>Product Management</h1>
          <div style={{display:"flex", marginBottom:"20px"}}>
            <CButton 
              onClick={handleOnClickAddNewModalButton} 
              color="info" shape="rounded-pill"
              style={{color:"white", marginRight:"10px"}}
            >
              <AddIcon/>Add New
            </CButton>

            {/* filter bar menu */}
            <LongMenuFilter/>
            <CButton 
              onClick={handleOnClickExportExcelButton} 
              color="success" shape="rounded-pill"
              style={{color:"white", marginRight:"10px"}}
            >
              <DownloadIcon/>Excel file
            </CButton>
          </div>
          
          {/* table products */}
          <Grid item lg={12} md={12} sm={12} xs={12}>
              <TableProduct products={products} visibleEditForm={visibleEditForm}
               setVisibleEditForm={setVisibleEditForm} />
          </Grid>
          <Grid item lg={12} md={12} sm={12} xs={12} mt={5}>
              <Pagination count={totalPage} page={currentPage} onChange={onChangePagination} />
          </Grid>
        </CCol>
      </CRow>
      <ModalForm visible={visible} setVisible={setVisible} />
    </>
  )
}

export default Product
