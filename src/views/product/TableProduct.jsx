
import React, { useState } from 'react';
import { Paper, Table, TableBody, TableCell, TableContainer, TableHead, TableRow, Tooltip } from "@mui/material"
import EditIcon from '@mui/icons-material/Edit';
import DeleteIcon from '@mui/icons-material/Delete';
import { useDispatch, useSelector } from 'react-redux';
import { checkProductInOrderDetailAction, fetchProductDetailAction, fetchProductTypesAction, 
  resetEditFormAction, 
  resetProductInOrderDetail} from '../../actions/admin.actions';
import ModalEditForm from './modal/ModalEditForm';
import 'react-toastify/dist/ReactToastify.css';
import ModalDelete from './modal/ModalDelete';
const TableProduct = (data) => {
  const [visibleDeleteModal, setVisibleDeleteModal]  = useState(false);
  const dispatch = useDispatch();
  const {loginSuccessInfo, currentPage, limitProductsPage, productDetail, pendingEdit, editMessage,minPrice, maxPrice, productName,inStock, outStock, allStock,typeId} = useSelector((reduxData) => reduxData.adminReducers);
  var products = data.products;
  //xử lý khi click nút edit
  const handleEditIconClick = (productId) => {
    //reset form edit before Edit
    dispatch(resetEditFormAction());
    // open edit modal
    data.setVisibleEditForm(true)
    //lấy thông tin sp theo id
    dispatch(fetchProductDetailAction(productId));
    //lấy toàn bộ type product để đưa vào ô select
    dispatch(fetchProductTypesAction());
  }
  //xử lý khi click xóa
  const handleDeleteIconClick = async (productId) => {
     //open Delete Modal
    setVisibleDeleteModal(!visibleDeleteModal);
    //get product by id
    dispatch(fetchProductDetailAction(productId));
    //reset value productInOrderDetail state
    await dispatch(resetProductInOrderDetail());
    // get productInOrderDetail to check product in some orderDetail or not before delete
    await dispatch(checkProductInOrderDetailAction(productId))
  }
  return(
    <>
    <ModalDelete visible={visibleDeleteModal} setVisible={setVisibleDeleteModal} productId={productDetail._id}></ModalDelete>
    <ModalEditForm visible={data.visibleEditForm} setVisible={data.setVisibleEditForm} productDetail={productDetail} />
    <TableContainer component={Paper}>
        <Table sx={{ minWidth: 650 }} aria-label="simple table">
            <TableHead>
            <TableRow>
                <TableCell sx={{ fontWeight:"bolder" }}>Order</TableCell>
                <TableCell sx={{ fontWeight:"bolder" }}>Name</TableCell>
                <TableCell sx={{ fontWeight:"bolder" }}>Image</TableCell>
                <TableCell sx={{ fontWeight:"bolder" }}>Price</TableCell>
                <TableCell sx={{ fontWeight:"bolder" }}>Promotion</TableCell>
                <TableCell sx={{ fontWeight:"bolder" }}>Stock</TableCell>
                <TableCell sx={{ fontWeight:"bolder" }}>Origin</TableCell>
                <TableCell sx={{ fontWeight:"bolder" }}>Weight(kg)</TableCell>
                <TableCell sx={{ fontWeight:"bolder" }}>Type</TableCell>
                <TableCell sx={{ fontWeight:"bolder" }}>Action</TableCell>
            </TableRow>
            </TableHead>
            <TableBody>
            {
              products.length > 0 &&
              products.map((element, index) => {
                  return (
                      <TableRow 
                        sx={{
                          '&:last-child td, &:last-child th': { border: 0 },
                          '&:hover': {
                            backgroundColor: '#f5f5f5',
                          },
                          backgroundColor: index % 2 === 0 ? '#f9f9f9' : '#ffffff',
                        }}
                        key={index}>
                          <TableCell>{(currentPage-1)*limitProductsPage + (index+1)}</TableCell>
                          <TableCell>{element.name}</TableCell>
                          <TableCell >
                            <img style={{width:"50px", height:"50px"}} src={`http://localhost:8080/images/${element.imageUrl[0]}`} alt="" />
                          </TableCell>
                          <TableCell >${element.buyPrice}</TableCell>
                          <TableCell >${element.promotionPrice}</TableCell>
                          <TableCell>{element.amount}</TableCell>
                          <TableCell>{element.origin}</TableCell>
                          <TableCell>{element.weight}</TableCell>
                          <TableCell>
                            {element.type.deletedAt ? (
                              <strike style={{color:"red"}}>{element.type.name? element.type.name + " - Type Deleted" : "Type Not Exist"}</strike>
                            ) : (
                              element.type.name
                            )}
                          </TableCell>
                          <TableCell>
                            <Tooltip title="Edit">
                              <EditIcon onClick={() => handleEditIconClick(element._id)} style={{width:"20px", color:"blue", cursor:"pointer"}}/>
                            </Tooltip>
                            <Tooltip title="Delete">
                              <DeleteIcon onClick={()=> handleDeleteIconClick(element._id)} style={{color:"red", cursor:"pointer"}}/>
                            </Tooltip>
                          </TableCell>
                      </TableRow>
                  ) 
              })
            }
            </TableBody>
        </Table>
      </TableContainer>
    </>
  )
}

export default TableProduct;