import React, { useEffect } from "react";
import { CButton, CModalTitle, CModalBody, CModalFooter , CModal, CModalHeader } from "@coreui/react"
import { ToastContainer, toast } from 'react-toastify';
import { useDispatch, useSelector } from 'react-redux';
import { fetchProductsDeletedAction, resetRestoreAction, restoreProductDeletedAction } from "../../../actions/admin.actions";
const ModalRestore = (data) =>{
  const dispatch = useDispatch();
  const { pendingRestore, restoreMessage } = useSelector((reduxData) => reduxData.adminReducers);
  //xử lý restore
  const handleRestoreItemSubmit = () => {
    dispatch(restoreProductDeletedAction(data.productId));
  }
  useEffect( function restore() {
    //hiện thông báo sau khi restore
    if(!pendingRestore && restoreMessage === "product restored successfully"){
      toast.success("Restore Successfully",{containerId:"RestoreSuccess"});
      dispatch(fetchProductsDeletedAction())
      data.setVisible(false)
      dispatch(resetRestoreAction());
    }
    if(!pendingRestore && restoreMessage && restoreMessage !== "product restored successfully"){
      toast.error("Restore Failed",{containerId:"RestoreFailed"})
      dispatch(resetRestoreAction());
    }
  }, [dispatch,pendingRestore,restoreMessage]);
  return(
    <>
      <ToastContainer autoClose={1500} hideProgressBar={true} containerId="RestoreSuccess" position="top-center" />
      <ToastContainer autoClose={1500} hideProgressBar={true} containerId="RestoreFailed" position="top-right" />
      <CModal
        scrollable
        visible={data.visible}
        onClose={() => data.setVisible(false)}
        aria-labelledby="ScrollingLongContentExampleLabel2"
      >
        <CModalHeader>
          <CModalTitle style={{color:"#2196F3", marginLeft:"35%"}} id="ScrollingLongContentExampleLabel2">Restore Item</CModalTitle>
        </CModalHeader>
        <CModalBody>
          Are you sure restoring this item?
        </CModalBody>
        <CModalFooter>
          <CButton color="secondary" onClick={() => data.setVisible(false)}>
            Close
          </CButton>
          <CButton color="primary" onClick={() => handleRestoreItemSubmit()}>Yes</CButton>
        </CModalFooter>
      </CModal>
    </>
  )
}

export default ModalRestore