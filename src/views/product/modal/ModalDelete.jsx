import React, { useEffect } from "react";
import { CButton, CModalTitle, CModalBody, CModalFooter , CModal, CModalHeader } from "@coreui/react"
import FormInfo from "../formInfo/FormInfo";
import { useDispatch, useSelector } from "react-redux";
import { fetchProductsAdminAction, 
  hardDeleteAction, resetDeleteAction, softDeleteAction } from "../../../actions/admin.actions";
import { ToastContainer, toast } from 'react-toastify';

const ModalDelete= (data) => {
  
  const dispatch = useDispatch();
  const {productInOrderDetail, hardDeletePending, hardDeleteProduct, pendingCheckProductInOrderDetail
    ,hardDeleteMessage, limitProductsPage,currentPage,minPrice, maxPrice, 
    productName,inStock, outStock, allStock,typeId, softDeletePending,
    softDeleteMessage 
  } = useSelector((reduxData) => reduxData.adminReducers);
  const handleDeleteItemSubmit = async () => {
    // Ensure this check happens after the async operation completes
    if(!pendingCheckProductInOrderDetail){
      // dùng xóa mềm
      dispatch(softDeleteAction(data.productId))
      //nếu sp này có nằm trong đơn đặt hàng thì chỉ xóa mềm
      // if(productInOrderDetail.length>0){
      //   dispatch(softDeleteAction(data.productId))
      // }else{
      //   //nếu ko thì xóa cứng
      //   dispatch(hardDeleteAction(data.productId))
      // }
    }
  };
  
  // useEffect( function hardDelete() {
  //   //HARD DELETE
  //   if(!hardDeletePending && hardDeleteMessage  === "Delete Successfully" ){
  //     // Hiển thị thông báo
  //     toast.success('😁 Delete Successfully!', {containerId: 'DeleteSuccess'});
  //     //cập nhật lại dữ liệu sản phẩm sau khi tạo mới
  //     dispatch(fetchProductsAdminAction(limitProductsPage,currentPage,minPrice, maxPrice, productName,inStock, outStock, allStock,typeId));
  //     // Đóng modal sau khi xóa thành công
  //     data.setVisible(false);
  //     //reset message;
  //     dispatch(resetDeleteAction());
  //   }
  //   if(!hardDeletePending && hardDeleteMessage  === "Have an error" ){
  //     // Hiển thị thông báo
  //     toast.error('Delete Failed!', {containerId: 'DeleteFail'});
  //     dispatch(resetDeleteAction());
  //   }
  // }, [dispatch, hardDeletePending, hardDeleteMessage ]);

  useEffect( function softDelete() {
    //SOFT DELETE
    if(!softDeletePending && softDeleteMessage  === "product soft deleted successfully" ){
      // Hiển thị thông báo
      toast.success('😁 Delete Successfully!', {containerId: 'SoftDeleteSuccess'});
      //cập nhật lại dữ liệu sản phẩm sau khi tạo mới
      dispatch(fetchProductsAdminAction(limitProductsPage,currentPage,minPrice, maxPrice, productName,inStock, outStock, allStock,typeId));
      // Đóng modal sau khi xóa thành công
      data.setVisible(false);
      dispatch(resetDeleteAction());
    }
    if(!softDeletePending && softDeleteMessage == "Cannot delete product with active orders"){
      toast.error("Cannot delete product with active orders!",{containerId:"DeleteProdActive"});
      data.setVisible(false);
      dispatch(resetDeleteAction());
    }
    if(!softDeletePending && softDeleteMessage  === "Have an error" ){
      // Hiển thị thông báo
      toast.error('Delete Failed!', {containerId: 'SoftDeleteFail'});
      dispatch(resetDeleteAction());
    }
  }, [dispatch, softDeletePending, softDeleteMessage ])
  return (
    <>
      {/* <CButton color="primary" onClick={data.setVisible}>Launch demo modal</CButton> */}
      <ToastContainer autoClose={1500} hideProgressBar={true} containerId="DeleteSuccess" position="top-center" />
      <ToastContainer autoClose={1500} hideProgressBar={true} containerId="DeleteFail" position="top-center"/>
      <ToastContainer autoClose={1500} hideProgressBar={true} containerId="SoftDeleteSuccess" position="top-center" />
      <ToastContainer autoClose={1500} hideProgressBar={true} containerId="SoftDeleteFail" position="top-center"/>
      <ToastContainer autoClose={1500} hideProgressBar={true} containerId="DeleteProdActive" position="top-center"/>

      <CModal
        scrollable
        visible={data.visible}
        onClose={() => data.setVisible(false)}
        aria-labelledby="ScrollingLongContentExampleLabel2"
      >
        <CModalHeader>
          <CModalTitle style={{color:"#2196F3", marginLeft:"35%"}} id="ScrollingLongContentExampleLabel2">Delete Item</CModalTitle>
        </CModalHeader>
        <CModalBody>
          Are you sure deleting this item?
        </CModalBody>
        <CModalFooter>
          <CButton color="secondary" onClick={() => data.setVisible(false)}>
            Close
          </CButton>
          <CButton color="primary" onClick={() => handleDeleteItemSubmit()}>Yes</CButton>
        </CModalFooter>
      </CModal>
    </>
  )
}
export default ModalDelete;