import React, { useEffect, useState } from "react"
import { CButton, CCol, CForm, CFormCheck, CFormFeedback, CFormInput, CFormLabel, CFormSelect, CFormTextarea, CInputGroup, CInputGroupText, CToast, CToastBody, CToastClose } from "@coreui/react"
import { useDispatch, useSelector } from "react-redux"
import { addNewProductAction, fetchProductTypesAction, onChangeInputAction, uploadImageAction } from "../../../actions/admin.actions";
import {inputLabels, patternProducts, feedbackInvalids, inputGroupTexts} from "../../../data";
import { ToastContainer, toast } from 'react-toastify';

const FormInfo = (data) => {
  const [validated, setValidated] = useState(false)
  const dispatch = useDispatch();
  const {types, productInfos } = useSelector((reduxData)=> reduxData.adminReducers);
  //lưu thông tin trong form vào state
  const handleOnChangeInput = (event) => {
    const { id, files, value } = event.target;
  
    if (id === "imageUrl" && files) {
        //tải nhiều ảnh, lưu vào thành 1 mảng tối đa 3 ảnh
        if(files.length>3){
          toast.error("Maximum 3 images", {containerId: 'Image'});
        }
        const limitedFiles = Array.from(files).slice(0, 3);
        const fileUrls = limitedFiles.map(file => URL.createObjectURL(file));
        // Lưu URL tạm thời vào state
        dispatch(onChangeInputAction({
          ...productInfos,
          [id]: { urls: fileUrls, files: limitedFiles },
        }));
      
    } else {
      // Xử lý các input khác
      dispatch(onChangeInputAction({
        ...productInfos,
        [id]: value
      }));
    }
  };
  //xử lý thêm mới sp
  const handleSubmit = async (event) => {
    //kiểm tra form validate ok chưa 
    const form = event.currentTarget
    if(productInfos.description.length < 500){
      toast.error("Description has minimum 500 words", {containerId: 'Description'});
      event.preventDefault();
      event.stopPropagation();
    }else if(productInfos.imageUrl && productInfos.imageUrl.files.length<3){
      toast.error("Need to 3 images", {containerId: 'ImageMiss'});
      event.preventDefault();
      event.stopPropagation();
    }//nếu form chưa ok vi phạm điều kiện các pattern thì ko thực hiện submit
    else if (form.checkValidity() === false) {
      event.preventDefault()
      event.stopPropagation()
    }//nếu ok thì ko thực hiện submit gọi api thêm mới 
    else{
      // Tạo một FormData object
      const formData = new FormData();
      
      // Thêm các tệp hình ảnh vào FormData
      if (productInfos.imageUrl && productInfos.imageUrl.files) {
        productInfos.imageUrl.files.forEach(file => {
          //append các file ảnh vào imageUrl key
          formData.append('files', file);
        });
      }
      const oldFiles = []; // Đây là mảng rỗng nếu không có ảnh cũ
      formData.append('oldFiles', JSON.stringify(oldFiles));
      // Upload images first
      const uploadedImages = await dispatch(uploadImageAction(formData));
      if (uploadedImages.error) {
        console.error('Error uploading images:', uploadedImages.error);
        return;
      }

      // Create product with the returned image filenames
      const imageFilenames = uploadedImages.data.map(file => file.filename);
      const productData = {
        ...productInfos,
        imageUrl: imageFilenames,
      };
      dispatch(addNewProductAction(productData)); 
      // dispatch(addNewProductAction(productInfos, formData));
      // dispatch(uploadImageAction(formData))
    }
    //hiển thị thông báo validate input nếu điền thiếu hoặc ko đúng
    setValidated(true)
  }

  return (
    <>
      <ToastContainer autoClose={1500} hideProgressBar={true} containerId="Image" position="top-right" />
      <ToastContainer autoClose={1500} hideProgressBar={true} containerId="Description" position="top-right"/>
      <ToastContainer autoClose={1500} hideProgressBar={true} containerId="ImageMiss" position="top-right"/>

      <CForm
        className="row g-3 needs-validation"
        noValidate
        validated={validated}
        onSubmit={handleSubmit}
      > 
        {/* query object ra và lấy thông tin các key, value */}
        {Object.entries(productInfos).map(([key, value]) => {
          const patternAttribute = patternProducts[key] ? { pattern: patternProducts[key] } : {};
          if (inputLabels[key] !== "Product Type" && inputLabels[key] !== "Product Image" && inputLabels[key] !== "Product Description") {
            return (
              <CCol md={12} key={key}>
                <CFormLabel style={{fontWeight: "bolder", color: "#9C27B0"}} htmlFor="validationTooltipUsername">
                  {inputLabels[key]}
                </CFormLabel>
                <CInputGroup className="has-validation">
                  {inputGroupTexts[key] && (
                    <CInputGroupText id="inputGroupPrepend">
                      {inputGroupTexts[key]}
                    </CInputGroupText>
                  )}
                  <CFormInput
                    type="text"
                    onChange={handleOnChangeInput}
                    value={value}
                    id={key}
                    required
                    {...patternAttribute}
                    feedbackInvalid={feedbackInvalids[key]}
                  />
                </CInputGroup>
              </CCol>
            );
          } else if (inputLabels[key] === "Product Type") {
            return (
              <CCol md={12} key={key}>
                <CFormLabel style={{fontWeight: "bolder", color: "#9C27B0"}} htmlFor="validationTooltipUsername">
                  {inputLabels[key]}
                </CFormLabel>
                <CFormSelect
                  aria-describedby="validationCustom04Feedback"
                  feedbackInvalid="Please select a valid state."
                  id="type"
                  required
                  onChange={handleOnChangeInput}
                  defaultValue={value} // Sử dụng defaultValue thay vì value
                >
                  <option value={""}>Choose Product Type</option>
                  {types.map((type, index) => (
                    <option key={index} value={type._id}>
                      {type.name}
                    </option>        
                  ))}
                </CFormSelect>
              </CCol>
            );
          } else if (inputLabels[key] === "Product Image") {
            return (
              <CCol md={12} key={key}>
                <CFormLabel style={{fontWeight: "bolder", color: "#9C27B0"}} htmlFor="validationTooltipUsername">
                  {inputLabels[key]}
                </CFormLabel>
                <CFormInput
                  type="file"
                  id={key}
                  onChange={handleOnChangeInput}
                  {...patternAttribute}
                  feedbackInvalid={feedbackInvalids[key]}
                  aria-label="file example"
                  required
                  multiple 
                  accept="image/*" // Chỉ chấp nhận file hình ảnh
                />
                {/* image upload preview */}
                {value.urls && value.urls.map((url, index) => (
                  <img
                    key={index}
                    src={url}
                    alt={`Uploaded preview ${index + 1}`}
                    style={{ marginTop: "10px", width: "100px", height: "100px" }}
                  />
                ))}
                
              </CCol>
            );
          }else if (inputLabels[key] === "Product Description") {
            return (
              <CCol md={12} key={key}>
                <CFormLabel style={{fontWeight: "bolder", color: "#9C27B0"}} htmlFor="validationTooltipUsername">
                  {inputLabels[key]}
                </CFormLabel>
                <CInputGroup className="has-validation">
                  {inputGroupTexts[key] && (
                    <CInputGroupText id="inputGroupPrepend">
                      {inputGroupTexts[key]}
                    </CInputGroupText>
                  )}
                  <CFormTextarea
                    type="text"
                    onChange={handleOnChangeInput}
                    value={value}
                    id={key}
                    required
                    {...patternAttribute}
                    feedbackInvalid={feedbackInvalids[key]}
                  />
                </CInputGroup>
              </CCol>
            );
          }
          return null; // Trường hợp không khớp điều kiện nào
        })}
      </CForm>
    </>
  );
  
}
export default FormInfo;