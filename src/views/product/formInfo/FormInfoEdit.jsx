import React, { useEffect, useState } from "react"
import { CButton, CCol, CForm, CFormCheck, CFormFeedback, CFormInput, CFormLabel, CFormSelect, CFormTextarea, CInputGroup, CInputGroupText, CToast, CToastBody, CToastClose } from "@coreui/react"
import { useDispatch, useSelector } from "react-redux"
import { editProductDetailAction, addNewProductAction, fetchProductTypesAction, onChangeInputEditAction, uploadImageAction } from "../../../actions/admin.actions";
import {inputLabels, patternProducts, feedbackInvalids, inputGroupTexts} from "../../../data";
import { ToastContainer, toast } from 'react-toastify';

const FormInfoEdit = (data) => {
  const [validated, setValidated] = useState(false)
  const dispatch = useDispatch();
  const {types, productDetailEdit, productDetail, productEdited } = useSelector((reduxData)=> reduxData.adminReducers);
  //lưu thông tin trong ô input vào state
  const handleOnChangeInput = (event)=>{
    const {id, files, value} = event.target;  
    if (id === "imageUrl" && files) {
      if(files.length>3){
        toast.error("Maximum 3 images");
      }
      //files ảnh để lưu vào form data
      const limitedFiles = Array.from(files).slice(0, 3);
      // hình ảnh để hiển thị ảnh upload preview
      const fileUrls = limitedFiles.map(file => URL.createObjectURL(file));

      // Lưu URL tạm thời vào state
      dispatch(onChangeInputEditAction({
        ...productDetailEdit,
        [id]: { urls: fileUrls, files: limitedFiles },
      }));
    } else {  
        dispatch(onChangeInputEditAction({
          ...productDetailEdit,
          [id]: value
        }))
      }
  }
  //thực hiện chỉnh sửa
  const handleSubmit = async (event) => {
    //kiểm tra form validate ok chưa 
    const form = event.currentTarget
    //nếu giá sửa đổi mà promo ko đổi và giá mới lớn hơn promo cũ 
    if(productDetailEdit.buyPrice && !productDetailEdit.promotionPrice 
      && productDetailEdit.buyPrice> productDetail.promotionPrice && productDetail.promotionPrice != 0 ){
        toast.error("Price must less than promotion ", {containerId: 'price'});
        event.preventDefault();
        event.stopPropagation();
    //nếu promo sửa đổi mà giá ko đổi và promo mới bé lớn hơn giá cũ 
    }else if(!productDetailEdit.buyPrice && productDetailEdit.promotionPrice 
      && productDetail.buyPrice>= productDetailEdit.promotionPrice && productDetailEdit.promotionPrice != 0  ){
        toast.error("Promotion must greater than price or equal 0 ", {containerId: 'price'});
        event.preventDefault();
        event.stopPropagation();
      //nếu promo và giá đều sửa đổi mà giá mới lớn hơn và promo mới
    }else if (productDetailEdit.buyPrice && productDetailEdit.promotionPrice 
      && productDetailEdit.buyPrice>= productDetailEdit.promotionPrice && productDetailEdit.promotionPrice != 0 ){
        toast.error("Promotion must greater than price or equal 0 ", {containerId: 'price'});
        event.preventDefault();
        event.stopPropagation();
    }//nếu form chưa ok vi phạm điều kiện các pattern thì ko thực hiện submit
    else if (form.checkValidity() === false) {
      event.preventDefault()
      event.stopPropagation()
    }//nếu ok thì ko thực hiện submit gọi api thêm mới 
    else{
      // Thêm các tệp hình ảnh vào FormData nếu có hình ảnh được edit
      if (productDetailEdit.imageUrl && productDetailEdit.imageUrl.files) {
          // Tạo một FormData object
          const formData = new FormData();
          productDetailEdit.imageUrl.files.forEach(file => {
            //append các file ảnh vào imageUrl key
            formData.append('files', file);
          });
          //append các file ảnh cũ vào để xóa
          formData.append('oldFiles', JSON.stringify(productDetail.imageUrl));
          // Upload images first
          const uploadedImages = await dispatch(uploadImageAction(formData));
          if (uploadedImages.error) {
            console.error('Error uploading images:', uploadedImages.error);
            return;
          }

          // Create product with the returned image filenames
          const imageFilenames = uploadedImages.data.map(file => file.filename);
          const productData = {
            ...productDetailEdit,
            imageUrl: imageFilenames,
          };          
          dispatch(editProductDetailAction(productDetail._id, productData))
        }else{
          
          //nếu ko có hình ảnh được cập nhật thì cập nhật các thông tin khác bình thường
          dispatch(editProductDetailAction(productDetail._id, productDetailEdit))
        }
      
    }
    //hiển thị thông báo validate input nếu điền thiếu hoặc ko đúng
    setValidated(true)
  }
  return (
    <>
      <ToastContainer />
      <ToastContainer autoClose={1500} hideProgressBar={true} containerId="price" position="top-right" />

      <CForm
        className="row g-3 needs-validation"
        noValidate
        validated={validated}
        onSubmit={handleSubmit}
      >
        {/* query object ra và lấy thông tin các key, value */}
        {Object.entries(inputLabels).map(([key, value]) => {
          const patternAttribute = patternProducts[key]
            ? { pattern: patternProducts[key] }
            : {};  
          if (inputLabels[key] !== "Product Type" && inputLabels[key] !== "Product Image" && inputLabels[key] !== "Product Description") {
            return (
              <CCol md={12} key={key}>
                <CFormLabel
                  style={{ fontWeight: "bolder", color: "#9C27B0" }}
                  htmlFor="validationTooltipUsername"
                >
                  {inputLabels[key]}
                </CFormLabel>
                <CInputGroup className="has-validation">
                  {inputGroupTexts[key] && (
                    <CInputGroupText id="inputGroupPrepend">{inputGroupTexts[key]}</CInputGroupText>
                  )}
                  <CFormInput
                    type="text"
                    onChange={handleOnChangeInput}
                    defaultValue={productDetail[key]}
                    id={key}
                    required
                    {...patternAttribute}
                    feedbackInvalid={feedbackInvalids[key]}
                  />
                </CInputGroup>
              </CCol>
            );
          } else if (inputLabels[key] === "Product Type") {
            return (
              <CCol md={12} key={key}>
                <CFormLabel
                  style={{ fontWeight: "bolder", color: "#9C27B0" }}
                  htmlFor="validationTooltipUsername"
                >
                  {inputLabels[key]}
                </CFormLabel>
                <CFormSelect
                  aria-describedby="validationCustom04Feedback"
                  feedbackInvalid="Please select a valid state."
                  id="type"
                  required
                  onChange={handleOnChangeInput}
                  value={productDetailEdit['type']? productDetailEdit['type'] : productDetail[key]} // với input Sử dụng value thay vì defaultValue
                >
                  {/* <option value="">Choose Product Type</option> */}
                  {types.map((type, index) => (
                    <option key={index} value={type._id}>
                      {type.name}
                    </option>
                  ))}
                </CFormSelect>
              </CCol>
            );
          } else if (inputLabels[key] === "Product Image") {
            return (
              <CCol md={12} key={key}>
                <CFormLabel
                  style={{ fontWeight: "bolder", color: "#9C27B0" }}
                  htmlFor="validationTooltipUsername"
                >
                  {inputLabels[key]}
                </CFormLabel>
                <CFormInput
                  type="file"
                  accept="image/png, image/jpeg, image/jpg, image/gif"
                  id={key}
                  onChange={handleOnChangeInput}
                  // {...patternAttribute}
                  // feedbackInvalid={feedbackInvalids[key]}
                  aria-label="file example"
                  // required
                  multiple
                />
          
                {productDetailEdit.imageUrl ? 
                  (productDetailEdit.imageUrl.urls.map((url, index) => (
                    <img
                      key={index}
                      src={url}
                      alt={`${index + 1}`}
                      style={{ margin: "10px", width: "100px", height: "100px", border:"solid 1px black" }}
                    />
                  )))
                  :
                  (productDetail.imageUrl && productDetail.imageUrl.map((url, index) => (
                    <img
                      key={index}
                      src={`http://localhost:8080/images/${url}`}
                      alt={`Uploaded preview ${index + 1}`}
                      style={{ margin: "10px", width: "100px", height: "100px", border:"solid 1px black" }}
                    />
                  )))
                }
              </CCol>
            );
          }else if (inputLabels[key] === "Product Description") {
            return (
              <CCol md={12} key={key}>
                <CFormLabel
                  style={{ fontWeight: "bolder", color: "#9C27B0" }}
                  htmlFor="validationTooltipUsername"
                >
                  {inputLabels[key]}
                </CFormLabel>
                <CInputGroup className="has-validation">
                  {inputGroupTexts[key] && (
                    <CInputGroupText id="inputGroupPrepend">{inputGroupTexts[key]}</CInputGroupText>
                  )}
                  <CFormTextarea
                    type="text"
                    
                    onChange={handleOnChangeInput}
                    defaultValue={productDetail[key]}
                    id={key}
                    required
                    {...patternAttribute}
                    feedbackInvalid={feedbackInvalids[key]}
                  />
                </CInputGroup>
              </CCol>
            );
          }
          return null; // Trường hợp không khớp điều kiện nào
        })}
      </CForm>
    </>
  );
  
}
export default FormInfoEdit;