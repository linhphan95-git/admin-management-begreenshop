import React, { useEffect } from 'react'
import { Box, Checkbox, FormControlLabel } from "@mui/material"
import SearchIcon from '@mui/icons-material/Search';
import { changeMaxPriceAction, changeMinPriceAction, changeToFirstPageAction, 
  checkAllStockAction, checkInStockAction, checkOutStockAction, 
  checkTypeProductAction, searchProductNameAction } from '../../actions/admin.actions';
import { useDispatch, useSelector } from 'react-redux';
const FilterBar = (data) => {
  const dispatch = useDispatch();
  const {productName, inStock, outStock, allStock, typeId, minPrice, maxPrice} = useSelector((reduxData)=> reduxData.adminReducers);
  //lưu giá filter min vào state
  const handleOnChangeMinPrice = (event) =>{
    dispatch(changeToFirstPageAction())
    dispatch(changeMinPriceAction(event.target.value))
  }
  //lưu giá filter max vào state
  const handleOnChangeMaxPrice = (event) =>{
    dispatch(changeToFirstPageAction())
    dispatch(changeMaxPriceAction(event.target.value))
  }
  //lưu instock vào state
  const handleInStockChange = () => {
    dispatch(changeToFirstPageAction())
    dispatch(checkInStockAction(!inStock))
  }
  //lưu outstock vào state
  const handleOutOfStockChange = () => {
    dispatch(changeToFirstPageAction())
    dispatch(checkOutStockAction(!outStock))
  }
  //lưu allstock vào state
  const handleAllProductsChange = () => {
    dispatch(changeToFirstPageAction())
    dispatch(checkAllStockAction(!allStock))
  }
  //lưu search name vào state
  const handleOnChangeSearchName = (event) =>{
    dispatch(changeToFirstPageAction())
    dispatch(searchProductNameAction(event.target.value))
  }
  //lưu type product vào state
  const onCheckBoxType = (typeId) =>{
    dispatch(changeToFirstPageAction())
    dispatch(checkTypeProductAction(typeId))
  }
  const types = data.types;
  return(
    <>
      {/* by name */}
      <Box style={{position:"relative", margin:"10px 0px"}}>
        <SearchIcon style={{color:"#7AB730", position:"absolute", top:"7px", left:"170px"}}/>
        <input value={productName} onChange={handleOnChangeSearchName} style={{backgroundColor:"transparent",color:"#7AB730", 
        border:"2px solid #7AB730", borderRadius:"5px", padding:"5px" }} type="text" placeholder='Search Name...' />
      </Box>
      {/* by all stock */}
      <Box style={{marginTop:"10px"}}>
        <h5 style={{color:"#F65005"}}>By All Product</h5>
        <FormControlLabel style={{color:"#7AB730"}} 
          onChange={handleAllProductsChange}
          checked={allStock}
          control={<Checkbox color="success"  
          sx={{
              color: "#7AB730",
              '&.Mui-checked': {
                color: "#7AB730",
              },
            }}
          />} 
          label="All Product" />
      </Box >
       {/* by price */}
      <Box style={{marginTop:"10px"}}>
        <h5 style={{color:"#F65005"}}>By Price</h5>
        <Box style={{marginTop:"10px", display:"flex"}}>
          <Box>
            <label style={{color:"#7AB730", marginRight:"10px"}} htmlFor="">From </label>
            <input type="number" placeholder='' 
              style={{width:"50px", backgroundColor:"transparent",
              border:"2px solid #7AB730", borderRadius:"2px", color:"#7AB730"}}
              onChange={handleOnChangeMinPrice}
              value={minPrice}
            />
          </Box>
          <Box>
            <label style={{color:"#7AB730", marginRight:"10px", marginLeft:"10px"}} htmlFor="">To </label>
            <input type="number" placeholder='' 
              style={{width:"50px", backgroundColor:"transparent",
              border:"2px solid #7AB730", borderRadius:"2px", color:"#7AB730"}}
              onChange={handleOnChangeMaxPrice}
              value={maxPrice}
              />
          </Box>
        </Box>
      </Box >
      {/* by stock */}
      <Box  style={{marginTop:"10px"}}>
        <h5 style={{color:"#F65005"}}>By Stock</h5>
        <FormControlLabel style={{color:"#7AB730"}} 
          onChange={handleInStockChange}
          checked={inStock}
          control={<Checkbox color="success"  
          sx={{
              color: "#7AB730",
              '&.Mui-checked': {
                color: "#7AB730",
              },
            }}
          />} 
          label="In stock" />
        <FormControlLabel style={{color:"#7AB730"}} 
          onChange={handleOutOfStockChange}
          control={<Checkbox color="success"  
          checked={outStock}
          sx={{
              color: "#7AB730",
              '&.Mui-checked': {
                color: "#7AB730",
              },
            }}
          />} 
          label="Out of stock" />
      </Box>
      {/* by type */}
      <Box>
        <h5 style={{color:"#F65005"}}>By Type</h5>
        {types.map((value, index) => {
          return <FormControlLabel key={index} 
          onChange={() => onCheckBoxType(value._id)}
          style={{color:"#7AB730"}} 
          control={<Checkbox color="success"  
          checked={typeId.findIndex(type => type === value._id) !== -1 ? true: false}
          sx={{
              color: "#7AB730",
              '&.Mui-checked': {
                color: "#7AB730",
              },
            }}
          />} 
          label={value.name} />
        })
      }
      </Box>
    </>
  )
}

export default FilterBar;