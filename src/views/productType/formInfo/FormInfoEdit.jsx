import React, { useEffect, useState } from "react"
import { CButton, CCol, CForm, CFormCheck, CFormFeedback, CFormInput, CFormLabel, CFormSelect, CFormTextarea, CInputGroup, CInputGroupText, CToast, CToastBody, CToastClose } from "@coreui/react"
import { useDispatch, useSelector } from "react-redux"
import { editProductDetailAction, addNewProductAction, fetchProductTypesAction, onChangeInputEditAction, uploadImageAction } from "../../../actions/admin.actions";
import {inputLabels, patternProducts, feedbackInvalids, inputGroupTexts} from "../../../data";
import { ToastContainer, toast } from 'react-toastify';
import { editProductTypeDetailAction, onChangeProductTypeEditInfo, uploadTypeImageAction } from "../../../actions/productType.actions";

const FormInfoEdit = (data) => {
  const [validated, setValidated] = useState(false)
  const dispatch = useDispatch();
  const {typeDetail, typeEditInfos
  } = useSelector((reduxData)=> reduxData.productTypeReducers);
  const handleOnChangeInput = (event) => {
    const { id, files, value } = event.target;
    if (id === "imageUrl" && files) {
      //tải 1 ảnh
      const selectedIconImageFile = files[0];
      const fileUrl = URL.createObjectURL(selectedIconImageFile);
      // Lưu URL tạm thời vào state
      dispatch(onChangeProductTypeEditInfo({
        ...typeEditInfos,
        [id]: { url: fileUrl, file: selectedIconImageFile },
      }));
    } else if (id === "imageUrlBackground" && files){
        //tải 1 ảnh
        const selectedBackgroundImageFile = files[0];
        const fileUrl = URL.createObjectURL(selectedBackgroundImageFile);
        // Lưu URL tạm thời vào state
        dispatch(onChangeProductTypeEditInfo({
          ...typeEditInfos,
          [id]: { url: fileUrl, file: selectedBackgroundImageFile },
        }));
    }else {
      // Xử lý các input khác
      dispatch(onChangeProductTypeEditInfo({
        ...typeEditInfos,
        [id]: value
      }));
    }
  };
   
  const handleSubmit = async (event) => {
    //kiểm tra form validate ok chưa 
    const form = event.currentTarget
    if (form.checkValidity() === false) {
      event.preventDefault()
      event.stopPropagation()
    }//nếu ok thì ko thực hiện submit gọi api thêm mới 
    else if(typeEditInfos.name && typeEditInfos.name.length > 16){
        toast.error("Name is not longer than 16 words", {containerId: 'name'});
        event.preventDefault();
        event.stopPropagation();
    }//nếu ok thì ko thực hiện submit gọi api thêm mới 
    else{
      const formData = new FormData();
      // Thêm các tệp hình ảnh vào FormData nếu có hình ảnh được edit
      if (typeEditInfos.imageUrl && typeEditInfos.imageUrl.file) {
        //append các file ảnh vào imageUrl key
        const file = typeEditInfos.imageUrl.file;
        formData.append('imageUrl', file);
      }
      if (typeEditInfos.imageUrlBackground && typeEditInfos.imageUrlBackground.file) {
        //append các file ảnh vào imageUrlBackground key
        const file = typeEditInfos.imageUrlBackground.file;
        formData.append('imageUrlBackground', file);
      }

      const oldFiles = []; // Đây là mảng rỗng nếu không có ảnh cũ
      formData.append('oldFiles', JSON.stringify(oldFiles));
      // Upload images first
      const uploadedImages = await dispatch(uploadTypeImageAction(formData));
      if (uploadedImages.error) {
        console.error('Error uploading images:', uploadedImages.error);
        return;
      }

      // Create type product with the returned image filenames
      const imageFilenames = uploadedImages.data; // Assuming the response contains image names in the data
      const productTypeData = {
        ...typeEditInfos,
        imageUrl: imageFilenames.imageUrl,
        imageUrlBackground: imageFilenames.imageUrlBackground,
      };
      dispatch(editProductTypeDetailAction(typeDetail._id,productTypeData)); 
    }
    //hiển thị thông báo validate input nếu điền thiếu hoặc ko đúng
    setValidated(true)
  }
  return (
    <>
      <ToastContainer />
      <ToastContainer autoClose={1500} hideProgressBar={true} containerId="name" position="top-right" />
      <CForm
        className="row g-3 needs-validation"
        noValidate
        validated={validated}
        onSubmit={handleSubmit}
      > 
         <CCol md={12}>
          <CFormLabel style={{fontWeight: "bolder", color: "#9C27B0"}} htmlFor="validationTooltipUsername">
            Product Type Name
          </CFormLabel>
          <CFormInput
            defaultValue={typeEditInfos.name ? typeEditInfos.name : typeDetail.name }
            type="text"
            feedbackInvalid="Product type name is required!"
            id="name"
            required
            onChange={handleOnChangeInput}
          />
        </CCol>
        <CCol md={12}>
          <CFormLabel style={{fontWeight: "bolder", color: "#9C27B0"}} htmlFor="validationTooltipUsername">
            Icon Image
          </CFormLabel>
          <CFormInput
            type="file"
            id="imageUrl"
            onChange={handleOnChangeInput}
          />
           {/* nếu ko có ảnh edit được up lên thì hiển thị ảnh sẵn có của type sp */}
            <img
              key={"imageUrl"}
              src={typeEditInfos.imageUrl ? typeEditInfos.imageUrl.url : `http://localhost:8080/images/types/`+typeDetail.imageUrl}
              alt={`Uploaded preview imageUrl`}
              style={{ marginTop: "10px", width: "100px", height: "100px" }}
            />
        </CCol>
        <CCol md={12}>
          <CFormLabel style={{fontWeight: "bolder", color: "#9C27B0"}} htmlFor="validationTooltipUsername">
            Background Image
          </CFormLabel>
          <CFormInput
            type="file"
            id="imageUrlBackground"
            onChange={handleOnChangeInput}
          />
          {/* old image and image upload preview */}
          <img
              key={"imageUrl"}
              src={typeEditInfos.imageUrlBackground ? typeEditInfos.imageUrlBackground.url : `http://localhost:8080/images/types/`+ typeDetail.imageUrlBackground}
              alt={`Uploaded preview imageUrl`}
              style={{ marginTop: "10px", width: "100px", height: "100px" }}
            />
        </CCol>
      </CForm>
    </>
  );
  
}
export default FormInfoEdit;