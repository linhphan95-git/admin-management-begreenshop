import React, { useState } from "react"
import { CCol, CForm, CFormInput, CFormLabel} from "@coreui/react"
import { useDispatch, useSelector } from "react-redux"

import { ToastContainer, toast } from 'react-toastify';
import { addProductTypeAction, onChangeProductTypeInfo, uploadTypeImageAction } from "../../../actions/productType.actions";

const FormInfo = () => {
  const [validated, setValidated] = useState(false)
  const dispatch = useDispatch();
  const {productTypeInfos} = useSelector((reduxData)=> reduxData.productTypeReducers);
  //xử lý lưu thông tin vào state khi user nhập thông tin tạo mới
  const handleOnChangeInput = (event) => {
    const { id, files, value } = event.target;
    if (id === "imageUrl" && files) {
      //tải 1 ảnh
      const selectedIconImageFile = files[0];
      const fileUrl = URL.createObjectURL(selectedIconImageFile);
      // Lưu URL tạm thời vào state
      dispatch(onChangeProductTypeInfo({
        ...productTypeInfos,
        [id]: { url: fileUrl, file: selectedIconImageFile },
      }));
    } else if (id === "imageUrlBackground" && files){
        //tải 1 ảnh
        const selectedBackgroundImageFile = files[0];
        const fileUrl = URL.createObjectURL(selectedBackgroundImageFile);
        // Lưu URL tạm thời vào state
        dispatch(onChangeProductTypeInfo({
          ...productTypeInfos,
          [id]: { url: fileUrl, file: selectedBackgroundImageFile },
        }));
    }else {
      // Xử lý các input khác
      dispatch(onChangeProductTypeInfo({
        ...productTypeInfos,
        [id]: value
      }));
    }
  };
  //xử lý khi click nút submit add type mới
  const handleSubmit = async (event) => {
    //kiểm tra form validate ok chưa 
    const form = event.currentTarget
    if (form.checkValidity() === false) {
      event.preventDefault()
      event.stopPropagation()
    }//nếu ok thì ko thực hiện submit gọi api thêm mới 
    else{
      // Tạo một FormData object
      const formData = new FormData();

      // Thêm các tệp hình ảnh vào FormData
      if (productTypeInfos.imageUrl && productTypeInfos.imageUrl.file) {
        //append các file ảnh vào imageUrl key
        const file = productTypeInfos.imageUrl.file;
        formData.append('imageUrl', file);
      }
      if (productTypeInfos.imageUrlBackground && productTypeInfos.imageUrlBackground.file) {
        //append các file ảnh vào imageUrlBackground key
        const file = productTypeInfos.imageUrlBackground.file;
        formData.append('imageUrlBackground', file);
      }

      const oldFiles = []; // Đây là mảng rỗng nếu không có ảnh cũ
      formData.append('oldFiles', JSON.stringify(oldFiles));
      // Upload images first
      const uploadedImages = await dispatch(uploadTypeImageAction(formData));
      if (uploadedImages.error) {
        console.error('Error uploading images:', uploadedImages.error);
        return;
      }

      // Create product with the returned image filenames
      const imageFilenames = uploadedImages.data; // Assuming the response contains image names in the data
      const productTypeData = {
        ...productTypeInfos,
        imageUrl: imageFilenames.imageUrl,
        imageUrlBackground: imageFilenames.imageUrlBackground,
      };
      dispatch(addProductTypeAction(productTypeData)); 
    }
    //hiển thị thông báo validate input nếu điền thiếu hoặc ko đúng
    setValidated(true)
  }

  return (
    <>
      <ToastContainer autoClose={1500} hideProgressBar={true} containerId="Image" position="top-right" />
      <ToastContainer autoClose={1500} hideProgressBar={true} containerId="Description" position="top-right"/>
      <CForm
        className="row g-3 needs-validation"
        noValidate
        validated={validated}
        onSubmit={handleSubmit}
      > 
         <CCol md={12}>
          <CFormLabel style={{fontWeight: "bolder", color: "#9C27B0"}} htmlFor="validationTooltipUsername">
            Product Type Name
          </CFormLabel>
          <CFormInput
            value={productTypeInfos.name}
            type="text"
            feedbackInvalid="Product type name is required!"
            id="name"
            required
            onChange={handleOnChangeInput}
          />
        </CCol>
        <CCol md={12}>
          <CFormLabel style={{fontWeight: "bolder", color: "#9C27B0"}} htmlFor="validationTooltipUsername">
            Icon Image
          </CFormLabel>
          <CFormInput
            type="file"
            id="imageUrl"
            onChange={handleOnChangeInput}
            feedbackInvalid="Image icon is required!"
            required 
          />
          {/* image upload preview */}
            {productTypeInfos.imageUrl.url ? 
              <img
              key={"imageUrl"}
              src={productTypeInfos.imageUrl.url}
              alt={`Uploaded preview imageUrl`}
              style={{ marginTop: "10px", width: "100px", height: "100px" }}
            />
            :
            null   
            }
                   
        </CCol>
        <CCol md={12}>
          <CFormLabel style={{fontWeight: "bolder", color: "#9C27B0"}} htmlFor="validationTooltipUsername">
            Background Image
          </CFormLabel>
          <CFormInput
            type="file"
            id="imageUrlBackground"
            onChange={handleOnChangeInput}
            feedbackInvalid="Image background is required!"
            required 
          />
          {/* image upload preview */}
          {productTypeInfos.imageUrlBackground.url ? 
              <img
              key={"imageUrlBackground"}
              src={productTypeInfos.imageUrlBackground.url}
              alt={`Uploaded preview image Background`}
              style={{ marginTop: "10px", width: "100px", height: "100px" }}
            />
            :
            null   
            }
        </CCol>
      </CForm>
    </>
  );
  
}
export default FormInfo;