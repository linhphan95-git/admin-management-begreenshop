
import React, { useState } from 'react';
import { Paper, Table, TableBody, TableCell, TableContainer, TableHead, TableRow, Tooltip } from "@mui/material"
import EditIcon from '@mui/icons-material/Edit';
import DeleteIcon from '@mui/icons-material/Delete';
import { useDispatch, useSelector } from 'react-redux';
import { checkProductInOrderDetailAction, fetchProductDetailAction,
  resetProductInOrderDetail} from '../../actions/admin.actions';
import ModalEditForm from './modal/ModalEditForm';
import 'react-toastify/dist/ReactToastify.css';
import { fetchProductTypeDetailAction, resetEditTypeFormAction,
   resetMessageAction} from '../../actions/productType.actions';
import ModalDelete from './modal/ModalDelete';

const TableProductType = (data) => {
  const [visibleDeleteModal, setVisibleDeleteModal]  = useState(false);
  const dispatch = useDispatch();
  const {types, currentPage, limit} = useSelector((reduxData) => reduxData.productTypeReducers);

  //xử lý sự kiện click vào edit icon
  const handleEditIconClick = (typeId) => {
    //reset form edit before Edit
    dispatch(resetEditTypeFormAction());
    // open edit modal
    data.setVisibleEditForm(true);
    //lấy thông tin type sp theo id
    dispatch(fetchProductTypeDetailAction(typeId));
    //reset message
    dispatch(resetMessageAction());
  }
  //su kien xoa type
  const handleDeleteIconClick = (typeId) => {
    //open Delete Modal
    setVisibleDeleteModal(!visibleDeleteModal);
    //get product by id
    dispatch(fetchProductTypeDetailAction(typeId));
    
  }
  return(
    <>
    <ModalDelete 
      visible={visibleDeleteModal} 
      setVisible={setVisibleDeleteModal} 
      >
    </ModalDelete>
    <ModalEditForm visible={data.visibleEditForm} setVisible={data.setVisibleEditForm} />
    <TableContainer component={Paper} sx={{marginTop:"20px" }}>
        <Table sx={{ minWidth: 650}} aria-label="simple table">
            <TableHead>
            <TableRow>
                <TableCell sx={{ fontWeight:"bolder" }}>Order</TableCell>
                <TableCell sx={{ fontWeight:"bolder" }}>Type Name</TableCell>
                <TableCell sx={{ fontWeight:"bolder" }}>Image Icon</TableCell>
                <TableCell sx={{ fontWeight:"bolder" }}>Image Background</TableCell>
                <TableCell sx={{ fontWeight:"bolder" }}>Action</TableCell>
            </TableRow>
            </TableHead>
            <TableBody>
            {
              types.length > 0 &&
              types.map((element, index) => {
                  return (
                      <TableRow 
                        sx={{
                          '&:last-child td, &:last-child th': { border: 0 },
                          '&:hover': {
                            backgroundColor: '#f5f5f5',
                          },
                          backgroundColor: index % 2 === 0 ? '#f9f9f9' : '#ffffff',
                        }}
                        key={index}>
                          <TableCell>{(currentPage-1)*limit + (index+1)}</TableCell>
                          <TableCell>{element.name}</TableCell>
                          <TableCell >
                            <img style={{width:"50px", height:"50px"}} src={`http://localhost:8080/images/types/`+element.imageUrl} alt="" />
                          </TableCell>
                          <TableCell>
                            <img style={{width:"50px", height:"50px"}} src={`http://localhost:8080/images/types/`+ element.imageUrlBackground} alt="" />
                          </TableCell>
                          <TableCell>
                            <Tooltip title="Edit">
                              <EditIcon onClick={() => handleEditIconClick(element._id)} style={{color:"blue", cursor:"pointer"}}/>
                            </Tooltip>
                            <Tooltip title="Delete">
                              <DeleteIcon onClick={()=> handleDeleteIconClick(element._id)} style={{color:"red", cursor:"pointer"}}/>
                            </Tooltip>
                          </TableCell>
                      </TableRow>
                  ) 
              })
            }
            </TableBody>
        </Table>
      </TableContainer>
    </>
  )
}

export default TableProductType;