import React, { useEffect } from "react";
import { CButton, CModalTitle, CModalBody, CModalFooter , CModal, CModalHeader } from "@coreui/react"
import { useDispatch, useSelector } from "react-redux";
import { ToastContainer, toast } from 'react-toastify';
import { fetchProductTypesTableAction, resetMessageAction, softDeleteTypeAction } from "../../../actions/productType.actions";

const ModalDelete= (data) => {
  const dispatch = useDispatch();
  const {typeDetail, limit, currentPage, typeName, softDeletePending, softDeleteMessage} = useSelector((reduxData) => reduxData.productTypeReducers);
  //xử lý sự kiện click nút submit
  const handleDeleteItemSubmit = () => {
      // dùng xóa mềm
      dispatch(softDeleteTypeAction(typeDetail._id))
  };
  //thông báo sau khi xóa thành công
  useEffect( () =>{
    //SOFT DELETE
    if(!softDeletePending && softDeleteMessage  === "Type Deleted Successfully" ){
      // Hiển thị thông báo
      toast.success('😁 Delete Successfully!', {containerId: 'SoftDeleteSuccess'});
      //cập nhật lại dữ liệu sản phẩm sau khi tạo mới
      dispatch(fetchProductTypesTableAction(limit, currentPage, typeName));
      // Đóng modal sau khi xóa thành công
      data.setVisible(false);
      dispatch(resetMessageAction());
    }
    if(!softDeletePending && softDeleteMessage  === "have an error" ){
      // Hiển thị thông báo
      toast.error('Delete Failed!', {containerId: 'SoftDeleteFail'});
      dispatch(resetMessageAction());
    }
  }, [dispatch, softDeletePending, softDeleteMessage ]);
  
  return (
    <>
      {/* <CButton color="primary" onClick={data.setVisible}>Launch demo modal</CButton> */}
      <ToastContainer autoClose={1500} hideProgressBar={true} containerId="DeleteSuccess" position="top-center" />
      <ToastContainer autoClose={1500} hideProgressBar={true} containerId="DeleteFail" position="top-center"/>
      <ToastContainer autoClose={1500} hideProgressBar={true} containerId="SoftDeleteSuccess" position="top-center" />
      <ToastContainer autoClose={1500} hideProgressBar={true} containerId="SoftDeleteFail" position="top-center"/>
      <ToastContainer autoClose={1500} hideProgressBar={true} containerId="DeleteProdActive" position="top-center"/>

      <CModal
        scrollable
        visible={data.visible}
        onClose={() => data.setVisible(false)}
        aria-labelledby="ScrollingLongContentExampleLabel2"
      >
        <CModalHeader>
          <CModalTitle style={{color:"#2196F3", marginLeft:"35%"}} id="ScrollingLongContentExampleLabel2">Delete Item</CModalTitle>
        </CModalHeader>
        <CModalBody>
          Are you sure deleting this type product?
        </CModalBody>
        <CModalFooter>
          <CButton color="secondary" onClick={() => data.setVisible(false)}>
            Close
          </CButton>
          <CButton color="primary" onClick={() => handleDeleteItemSubmit()}>Yes</CButton>
        </CModalFooter>
      </CModal>
    </>
  )
}
export default ModalDelete;