import React from "react";
import { CButton, CModalTitle, CModalBody, CModalFooter , CModal, CModalHeader } from "@coreui/react"
import FormInfo from "../formInfo/FormInfo";
//form dien thong tin them loai sp moi
const ModalForm = (data) => {
  return (
    <>
      {/* <CButton color="primary" onClick={data.setVisible}>Launch demo modal</CButton> */}
      <CModal
        scrollable
        visible={data.visible}
        onClose={() => data.setVisible(false)}
        aria-labelledby="ScrollingLongContentExampleLabel2"
      >
        <CModalHeader>
          <CModalTitle style={{color:"#2196F3", marginLeft:"25%"}} id="ScrollingLongContentExampleLabel2">Add New Product Type</CModalTitle>
        </CModalHeader>
        <CModalBody>
          {/* form info */}
          <FormInfo/>
        </CModalBody>
        <CModalFooter>
          <CButton color="secondary" onClick={() => data.setVisible(false)}>
            Close
          </CButton>
          <CButton color="primary" onClick={() => document.querySelector('form').dispatchEvent(new Event('submit', { cancelable: true, bubbles: true }))}>Add New</CButton>
        </CModalFooter>
      </CModal>
    </>
  )
}
export default ModalForm;