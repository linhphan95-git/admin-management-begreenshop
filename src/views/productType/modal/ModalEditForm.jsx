import React from "react";
import { CButton, CModalTitle, CModalBody, CModalFooter , CModal, CModalHeader } from "@coreui/react"
import FormInfoEdit from "../formInfo/FormInfoEdit";

const ModalEditForm = (data) => {
  return (
    <>
      {/* <CButton color="primary" onClick={data.setVisible}>Launch demo modal</CButton> */}
      <CModal
        scrollable
        visible={data.visible}
        onClose={() => data.setVisible(false)}
        aria-labelledby="ScrollingLongContentExampleLabel2"
      >
        <CModalHeader>
          <CModalTitle 
            style={{color:"#2196F3", 
            marginLeft:"25%"}} 
            id="ScrollingLongContentExampleLabel2">
            Edit Type Information
          </CModalTitle>
        </CModalHeader>
        <CModalBody>
          {/* form info */}
          <FormInfoEdit/>
        </CModalBody>
        <CModalFooter>
          <CButton color="secondary" onClick={() => data.setVisible(false)}>
            Close
          </CButton>
          <CButton color="primary" onClick={() => document.querySelector('form').dispatchEvent(new Event('submit', { cancelable: true, bubbles: true }))}>Edit Item</CButton>
        </CModalFooter>
      </CModal>
    </>
  )
}
export default ModalEditForm;