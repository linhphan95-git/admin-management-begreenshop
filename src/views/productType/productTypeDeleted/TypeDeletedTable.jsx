
import React, { useEffect, useState } from 'react';
import { DataGrid} from '@mui/x-data-grid';
import { useDispatch, useSelector } from 'react-redux';
import ModalRestore from '../modal/ModalRestore';
import { fetchProductTypeDetailAction, fetchTypesDeletedAction } from '../../../actions/productType.actions';
import { Box, Tooltip } from '@mui/material';
import SettingsBackupRestoreIcon from '@mui/icons-material/SettingsBackupRestore';

const TypeDeletedTable = () => {
  const dispatch = useDispatch();
  const {typesDeleted} = useSelector((reduxData) => reduxData.productTypeReducers);
  useEffect(()=>{
    dispatch(fetchTypesDeletedAction());
  }, [dispatch]);
  const columns = [
    { 
      field: 'order',
      headerName: '#', 
      width: 50,
      sortable: false,
      filterable: false,
    },
    { 
      field: 'name',
      headerName: 'Type Name', 
      width: 250 
    },
    { 
      field: 'imageUrl', 
      headerName: 'Image', 
      width: 200,
      sortable: false,
      filterable: false,
      renderCell: (params) => (
        <div>
          <img
            width="50px"
            height="50px"
            src={`http://localhost:8080/images/types/${params.row.imageUrl}`}
            alt="Product"
          />
        </div>
      ),
    },
    { 
      field: 'imageUrlBackground', 
      headerName: 'Image Background', 
      width: 200,
      sortable: false,
      filterable: false,
      renderCell: (params) => (
        <div>
          <img
            width="50px"
            height="50px"
            src={`http://localhost:8080/images/types/${params.row.imageUrlBackground}`}
            alt="Product"
          />
        </div>
      ),
    },
    {
      field: 'action',
      headerName: 'Action',
      type: 'number',
      width: 90,
      sortable: false,
      filterable: false,
      renderCell: (params) => (
        <Box>
          <Tooltip title="Restore Product">
            <SettingsBackupRestoreIcon
              onClick={() => handleRestoreIconClick(params.row._id)}
              style={{ color: 'purple', cursor: 'pointer' }}
            />
          </Tooltip>
        </Box>
      ),
    },
  ];
  //dùng map sau đó tạo thêm field order để lấy số thứ tự sp
  const rows = typesDeleted.map((type, index) => ({ ...type, order: index + 1 }));
  const [visibleRestoreModal, setVisibleRestoreModal] = useState(false);
  const handleRestoreIconClick =  (typeId) =>{
    dispatch(fetchProductTypeDetailAction(typeId));
    setVisibleRestoreModal(true);
  }
  return(
     <>
      <ModalRestore 
        visible={visibleRestoreModal}
        setVisible={setVisibleRestoreModal}
      />
      <div style={{ height: 600, width: '100%' }}>
        <DataGrid
          className='grid-pagination'
          rows={rows}
          columns={columns}
          getRowHeight={(params) => {
            return 100; // set chiều cao cho các hàng khác
          }}
          initialState={{
            pagination: {
              paginationModel: { page: 0, pageSize: 10 },
            },
          }}
          pageSizeOptions={[5, 10]}
          // checkboxSelection
          getRowId={(row) => row._id}
        />
      </div>
     </>
  )
}

export default TypeDeletedTable;