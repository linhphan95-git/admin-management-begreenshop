import React from 'react'
import { Grid } from "@mui/material"
import {
  CCol,
  CRow,
} from '@coreui/react'
import TypeDeletedTable from './TypeDeletedTable';
const TypeDeleted = () => {
  return (
    <>
      <CRow>
        <CCol xs>
          <h1 style={{textAlign:"center", marginBottom:"30px"}}>Types Deleted Management</h1>
          {/* table products Deleted */}
          <Grid item lg={12} md={12} sm={12} xs={12}>
            <TypeDeletedTable/>
          </Grid>
        </CCol>
      </CRow>
    </>
  )
}

export default TypeDeleted
