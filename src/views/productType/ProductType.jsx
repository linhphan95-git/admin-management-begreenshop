import React, { useEffect, useState } from 'react'
import { Button, Grid, Pagination} from "@mui/material"
import {
  CButton,
  CCol,
  CRow,
} from '@coreui/react'
import { useDispatch, useSelector } from 'react-redux'
import { exportExcelProductFile, fetchProductsAdminAction,
   onChangePaginationAction, resetAfterCreateAction, 
   resetEditMessageAction} from '../../actions/admin.actions';
// import TableProduct from './TableProduct'
import AddIcon from '@mui/icons-material/Add';
import DownloadIcon from '@mui/icons-material/Download';
// import LongMenuFilter from './LongMenuFilter'
import ModalForm from './modal/Modal';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import TableProductType from './TableProductType';
import { fetchProductTypesTableAction, onChangePaginationTypeTable, resetFormAddTypeAction, resetMessageAction } from '../../actions/productType.actions';
import LongMenuFilterType from './LongMenuFilterType';


const ProductType = () => {
  const dispatch = useDispatch();
  const {  pendingAddType, newProductTypeMessage, pendingEditType,
     editProductTypeMessage, limit, currentPage, typeName, totalPage
    } = useSelector((reduxData) => reduxData.productTypeReducers);
  useEffect( function fetchProduct() {
      // Gọi API để lấy dữ liệu
      dispatch(fetchProductTypesTableAction(limit, currentPage, typeName));
  }, [dispatch, limit, currentPage, typeName]);
  
  //sự kiện chuyển trang sản phẩm
  const onChangePagination = (event, value) => {
    dispatch(onChangePaginationTypeTable(value))
  }
  //open/close modal add new
  const [visible, setVisible] = useState(false);
  //open/close modal edit 
  const [visibleEditForm, setVisibleEditForm]  = useState(false);
  
  //xử lý sự kiện click nút tạo mới type product
  const handleOnClickAddNewModalButton = () => {
    //mở modal lên
    setVisible(!visible);
    //reset lại form modal add new sau khi đóng
    dispatch(resetFormAddTypeAction());
  }

  //xử lý sự kiện click nút tải file excel
  const handleOnClickExportExcelButton = () =>{
    dispatch(exportExcelProductFile())
  }

  //hiển thị thông báo sau khi click nút tạo mới
  useEffect(() =>{
    //thành công
    if(!pendingAddType && newProductTypeMessage  === "Create successfully" ){
      // Hiển thị thông báo tạo thành công
      toast.success('😁Create Successfully!', {containerId: 'D'});
      //đóng lại modal khi tạo thành công
      setVisible(false);
      //reset message sau khi tạo thành công
      dispatch(resetMessageAction());
      //cập nhật lại type product trong bảng
      dispatch(fetchProductTypesTableAction(limit, currentPage, typeName));
    }
    // //tạo mới thất bại
    if(!pendingAddType && newProductTypeMessage === "Name is duplicate"){
      // Hiển thị nếu tên sp bị trùng với sp khác
      toast.error("Create Failed! Type name is duplicated", {containerId: 'B'});
      //reset form và message sau khi tạo thành công
      dispatch(resetMessageAction());
    }
    if(!pendingAddType && newProductTypeMessage === "Have an error"){
      // Hiển thị thông báo các trường hợp lỗi khác
      toast.error("Create Failed! Have an error", {containerId: 'B'});
      //reset form và message sau khi tạo thành công
      dispatch(resetMessageAction());
    }
    },[dispatch,pendingAddType,newProductTypeMessage])
      
    // hiển thị thông báo sau khi click submit nút edit
    useEffect(()=>{
      //EDIT
      if(!pendingEditType && editProductTypeMessage  === "Update Successfully" ){
        // Hiển thị thông báo
        toast.success('😁 Edit Successfully!', {containerId: 'C'});
        //cập nhật lại dữ liệu sản phẩm sau khi tạo mới
        dispatch(fetchProductTypesTableAction(limit, currentPage, typeName));
        //đóng modal sau khi edit thành công
        setVisibleEditForm(false);
        dispatch(resetMessageAction());
      }
      if(!pendingEditType && editProductTypeMessage === "Name is duplicate"){
        // Hiển thị thông báo nếu tên sp bị trùng sp khác
        toast.error("Edit Failed! Type name is duplicated", {containerId: 'A'});
        dispatch(resetMessageAction())
      }
      if(!pendingEditType && editProductTypeMessage === "Have an error"){
        // Hiển thị thông báo nếu edit fail do các case khác
        toast.error("Edit Failed! Have an error", {containerId: 'A'});
        dispatch(resetMessageAction())
      }
  }, [dispatch, pendingEditType, editProductTypeMessage])
  return (
    <>
    {/* đặt id cho mỗi toast nếu có nhiều toast để tránh lỗi */}
    <ToastContainer autoClose={1500} hideProgressBar={true} containerId="A" position="top-right" />
    <ToastContainer autoClose={1500} hideProgressBar={true} containerId="B" position="top-right"/>
    <ToastContainer autoClose={1500} hideProgressBar={true} containerId="C" position="top-center" />
    <ToastContainer autoClose={1500} hideProgressBar={true} containerId="D" position="top-center" />

      <CRow>
        <CCol xs>
          <h1 style={{textAlign:"center"}}>Product Type Management</h1>
          <div style={{display:"flex", marginBottom:"20px"}}>
            <CButton 
              onClick={handleOnClickAddNewModalButton} 
              color="info" shape="rounded-pill"
              style={{color:"white", marginRight:"10px"}}
            >
              <AddIcon/>Add New
            </CButton>
            {/* filter bar menu */}
            <LongMenuFilterType/>
            {/* <CButton 
              onClick={handleOnClickExportExcelButton} 
              color="success" shape="rounded-pill"
              style={{color:"white"}}
            >
              <DownloadIcon/>Excel File
            </CButton> */}
          </div>
          {/* table products */}
          <Grid item lg={12} md={12} sm={12} xs={12}>
              <TableProductType visibleEditForm={visibleEditForm}
               setVisibleEditForm={setVisibleEditForm}/>
          </Grid>
          <Grid item lg={12} md={12} sm={12} xs={12} mt={5}>
              <Pagination count={totalPage} page={currentPage} onChange={onChangePagination} />
          </Grid>
        </CCol>
      </CRow>
      {/* form thêm mới */}
      <ModalForm visible={visible} setVisible={setVisible} />
    </>
  )
}

export default ProductType
