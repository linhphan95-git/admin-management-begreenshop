import React, { useEffect } from 'react'
import { Box, Checkbox, FormControlLabel } from "@mui/material"
import SearchIcon from '@mui/icons-material/Search';
import { useDispatch, useSelector } from 'react-redux';
import { onChangeSearchTypeName } from '../../actions/productType.actions';
const FilterBarType = (data) => {
  const dispatch = useDispatch();
  const {typeName} = useSelector((reduxData)=> reduxData.productTypeReducers);
  
  const handleOnChangeSearchTypeName = (event) =>{
    // dispatch(changeToFirstPageAction())
    dispatch(onChangeSearchTypeName(event.target.value))
  }
  return(
    <>
      {/* by name */}
      <Box style={{position:"relative", margin:"10px 0px"}}>
        <SearchIcon style={{color:"#7AB730", position:"absolute", top:"7px", left:"270px"}}/>
        <input 
          value={typeName} 
          onChange={handleOnChangeSearchTypeName} 
          style={{
            backgroundColor:"transparent",
            color:"#7AB730", 
            border:"2px solid #7AB730", 
            borderRadius:"5px", 
            padding:"5px",
            width: "300px"
          }} 
          type="text" 
          placeholder='Search Name...' />
      </Box>
    </>
  )
}

export default FilterBarType;