import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { fetchUserDetailAction, resetAdminInfoEditFormAction } from "../../actions/user.actions";
import EditUserForm from "./ModalEditForm";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faUserSecret, faUserShield, faUsers,  } from '@fortawesome/free-solid-svg-icons';

const Admin = () => {
  const dispatch = useDispatch(); // Correctly call useDispatch
  const { userInfos, user } = useSelector((reduxData) => reduxData.userReducers);

  useEffect(() => {
    // Gọi API để lấy dữ liệu user đổ ra bảng
    dispatch(fetchUserDetailAction(userInfos[0].username));
  }, [dispatch, userInfos]);

  const [modal, setModal] = useState(false);
  const toggle = () => setModal(!modal);
  //xử lý event click vào nút thay đổi thông tin admin
  const handleChangeUserProfile = () => {
     toggle(true);
     // reset form edit before open
     dispatch(resetAdminInfoEditFormAction());
  }
 
  return (
    <>
      <EditUserForm modal={modal} toggle={toggle} user={user}/>
     <section style={{ backgroundColor: "#eee" }}>
        <div className="container py-5">
          <div className="row">
            <div className="col-lg-4">
              <div className="card mb-4">
                <div className="card-body text-center">
                  <img
                    src={user[0]?.avatar ? `http://localhost:8080/images/`+ user[0]?.avatar : "https://www.pngall.com/wp-content/uploads/5/Profile-PNG-File.png"}
                    alt="avatar"
                    className="rounded-circle img-fluid"
                    style={{ width: 130, height:"132px" }}
                  />
                  <h4 className="my-3">{userInfos[0].username}</h4>
                  <div className="d-flex justify-content-center align-items-center text-muted list-none">{userInfos[0].roles.map((role, index)=>{
                    return <h6 className="me-2" key={index}> {role.name} </h6>
                  })}</div>
                  {/* <p className="text-muted mb-4">Bay Area, San Francisco, CA</p> */}
                  <div className="d-flex justify-content-center mb-2">
                    <button
                      onClick={handleChangeUserProfile}
                      type="button"
                      data-mdb-button-init=""
                      data-mdb-ripple-init=""
                      className="btn btn-primary mt-5"
                    >
                      Change Info
                    </button>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-lg-8">
              <div className="card mb-4">
                <div className="card-body">
                  <div className="row">
                    <div className="col-sm-3">
                      <p className="mb-0">Username</p>
                    </div>
                    <div className="col-sm-9">
                      <p className="text-muted mb-0">{userInfos[0].username}</p>
                    </div>
                  </div>
                  <hr />
                  <div className="row">
                    <div className="col-sm-3">
                      <p className="mb-0">Email</p>
                    </div>
                    <div className="col-sm-9">
                      <p className="text-muted mb-0">{userInfos[0].email}</p>
                    </div>
                  </div>
                  <hr />
                  <div className="row">
                    <div className="col-sm-3">
                      <p className="mb-0">Roles</p>
                    </div>
                    <div className="col-sm-9" style={{display:"flex"}}>
                      {user[0]?.roles.map((role, index)=>{
                        return <p className="text-muted mb-0" key={index}> {role.name} / </p>
                      })}
                    </div>
                  </div>
                  <hr />
                    <div className="row">
                      <div className="col-sm-3">
                        <p className="mb-0">Roles Description</p>
                      </div>
                      <div className="col-sm-9">
                        <p className="text-muted mb-0">
                          <strong><FontAwesomeIcon title='Admin' style={{color:"green" , cursor: 'pointer'}} icon={faUserSecret} /> Admin:</strong> permission to access and CRUD all tables and pages <br/> 
                          <strong><FontAwesomeIcon title='Moderator' style={{color:"#673ab7", cursor: 'pointer'}} icon={faUserShield} /> Moderator:</strong> permission to access, view, edit some pages in admin page <br/>
                          <strong><FontAwesomeIcon title='User' style={{color:"#f44336", cursor: 'pointer'}} icon={faUsers} /> User:</strong> Can not access to admin page, just access to home page<br/>
                        </p>
                      </div>
                    </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </>
      
  );
}

export default Admin;
