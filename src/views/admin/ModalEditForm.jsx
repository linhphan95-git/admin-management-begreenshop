import React, { useEffect, useState } from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import {FormControl,
     OutlinedInput, InputAdornment,
     IconButton,
     FormHelperText
    } from '@mui/material';
import Visibility from '@mui/icons-material/Visibility';
import VisibilityOff from '@mui/icons-material/VisibilityOff';   
import { useDispatch, useSelector } from "react-redux";
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { fetchUserDetailAction, onChangeAdminInfoAction, uploadAdminInfoAction } from '../../actions/user.actions';
import { uploadImageAction } from '../../actions/admin.actions';

function EditUserForm(data) {
  const dispatch = useDispatch();
  const {userInfos, adminInfoEdited, pendingUploadUserInfo, uploadUserMessage} = useSelector((reduxData)=> reduxData.userReducers);
  const [showPassword, setShowPassword] = React.useState(false);

  const handleClickShowPassword = () => setShowPassword((show) => !show);

  const handleMouseDownPassword = () => {
    // event.preventDefault();
  };
  const [checkPassword, setCheckPassword] = useState(false);
  const pattern = /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/;
  //xử lý lưu thay đổi thông tin admin
  const handleChangeUserInfo = (event) => {
    const { id, files, value } = event.target;
    if(id === "password" && !pattern.test(value)){
      setCheckPassword(true);
    }else{
      setCheckPassword(false);
    }
    if (id === "avatar" && files && files.length > 0 && files[0]) {
      const file = files[0];
      if (file) {
        dispatch(onChangeAdminInfoAction({
          ...adminInfoEdited,
          [id]: { url: URL.createObjectURL(file), file: file },
        }));
      }
    } else {
      dispatch(onChangeAdminInfoAction({
        ...adminInfoEdited,
        [id]: value,
      }));
    }
  };
  //xử lý lưu thay đổi thông tin admin sau khi click submit
  const handleSaveChangeInfo = async () =>{
    if(!adminInfoEdited.password && !adminInfoEdited.avatar){
      toast.error('Do not have info to change', {
        position: "top-center",
        autoClose: 1000,
        hideProgressBar: true,
        closeOnClick: true, containerId:"A" 
        });
      return false;
    }else{
        //password not correct
        if(adminInfoEdited.password && !pattern.test(adminInfoEdited.password)){
          toast.error('Password is not good', {
            position: "top-center",
            autoClose: 1000,
            hideProgressBar: true,
            closeOnClick: true, containerId:"A" 
            });
          return false;
        }else if(adminInfoEdited.avatar){
          //has avatar
          // Tạo một FormData object
          const formData = new FormData();
          formData.append('files', adminInfoEdited.avatar.file);
          if(data.user[0].avatar){
            formData.append('oldFiles', JSON.stringify([data.user[0].avatar]) );
          }else{
            formData.append('oldFiles', JSON.stringify([]) );
          }
          // gọi dispatch Upload images first
          const uploadedImage = await dispatch(uploadImageAction(formData));
          if (uploadedImage.error) {
            toast.error('Error uploading images:', uploadedImage.error, {containerId:"ImageError"});
            return;
          }
          //sau khi gọi dispatch phía trên và upload hình ảnh lên nodejs thành công thì sẽ lấy tên của nó ra và lưu vào imageUrl
          // Create product with the returned image filenames
          const imageFilename = uploadedImage.data[0].filename;
          const userInfoData = {
            ...adminInfoEdited,
            avatar: imageFilename,
          }//gọi dispatch để tạo sp mới
          await dispatch(uploadAdminInfoAction(userInfoData, data.user[0]._id));
        }else{
          dispatch(uploadAdminInfoAction(adminInfoEdited, data.user[0]._id));
        }
      }    
  }
  
  //edit
  useEffect(()=>{
    if(!pendingUploadUserInfo && uploadUserMessage === "User updated successfully"){
      toast.success('User Update Successfully', {containerId:"UserSuccess"});
      //đóng modal edit
      data.toggle(false)
      // reload user info after update
      dispatch(fetchUserDetailAction(userInfos[0].username));
    }
    if(!pendingUploadUserInfo && uploadUserMessage === "Have an error"){
      toast.error('User Update Failed!', {containerId:"UserFailed"});
      //đóng modal edit
      data.toggle(false)
    }
  // eslint-disable-next-line react-hooks/exhaustive-deps
  },[dispatch, pendingUploadUserInfo, uploadUserMessage])
  return (
    <div>
      <ToastContainer containerId="ImageError"/>
      <ToastContainer containerId="UserSuccess"/>
      <ToastContainer containerId="UserFailed" />
      <ToastContainer containerId="A" />

      <Modal isOpen={data.modal} toggle={data.toggle}>
        <ModalHeader toggle={data.toggle}>Change User Info</ModalHeader>
        <ModalBody>
        <FormControl sx={{ m: 1, width: '100%' }} variant="outlined">
          {/* <InputLabel htmlFor="outlined-adornment-password">Password</InputLabel> */}
          <label className="mr-[10px] text-indigo-600 font-bold"> <strong>New Password</strong></label>
          <OutlinedInput 
            size='small'
            type={showPassword ? 'text' : 'password'}
            endAdornment={
              <InputAdornment position="end">
                <IconButton
                  aria-label="toggle password visibility"
                  onClick={handleClickShowPassword}
                  onMouseDown={handleMouseDownPassword}
                  edge="end"
                >
                  {showPassword ? <VisibilityOff /> : <Visibility />}
                </IconButton>
              </InputAdornment>
            }
            label="Password"
            id="password"
            onChange={handleChangeUserInfo}
          />
          {checkPassword ? 
            <FormHelperText error id="standard-weight-helper-text">
              Password require minimum 8 characters containing numbers and words
            </FormHelperText>
            :
            null
          }
        </FormControl>
        <div className="ml-[30px]">
          <label className="mr-[10px] text-indigo-600 font-bold"> <strong>New Avatar: </strong></label>
          <br />
          <input 
            id="avatar"
            type="file" 
            accept="image/png, image/jpeg, image/jpg, image/gif"
            onChange={handleChangeUserInfo}
          />
        </div>
        {adminInfoEdited?.avatar?.url ?
          <img width="70px" height="70px" alt='' src={adminInfoEdited.avatar.url} />
          :
          null
        }
        </ModalBody>
        <ModalFooter>
          <Button color="primary" onClick={handleSaveChangeInfo}>
            Save Change
          </Button>{' '}
          <Button color="secondary" onClick={data.toggle}>
            Cancel
          </Button>
        </ModalFooter>
      </Modal>
    </div>
  );
}

export default EditUserForm;