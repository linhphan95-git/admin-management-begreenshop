import React from 'react'
import { AppContent, AppSidebar, AppFooter, AppHeader } from '../components/index'
import { useSelector } from 'react-redux'

const DefaultLayout = () => {
  // const {userInfos} = useSelector((reduxData)=> reduxData.userReducers);
  const userInfos = JSON.parse(localStorage.getItem('adminLoginInfo')) || [] // Lấy mảng roles từ localStorage và parse nó thành object
 // Trích xuất trường "name" từ mỗi phần tử trong mảng userInfos và lưu vào một mảng mới
  const userRoles = userInfos.length > 0 ? userInfos[0].roles.map(info => info.name) : [];
  return (

    <div>
      <AppSidebar />
      <div className="wrapper d-flex flex-column min-vh-100">
        <AppHeader />
        <div className="body flex-grow-1">
          <AppContent userRoles={userRoles} />
        </div>
        <AppFooter />
      </div>
    </div>
  )
}

export default DefaultLayout
